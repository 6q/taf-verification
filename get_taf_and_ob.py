import get_obs
import get_taf
from time import gmtime
from other_functions.time_based_functions import get_taf_time_frame, move_time


def get_ob_and_taf_json(icao, issue_time=None):
    """
    Download and creates the JSON for the TAF user requested issue time or if issue time is None the last completed TAF.
    Then goes and finds the time frame of the TAF and downloads the OBs matching that time frame +- 3 hours
    :param icao: string. The ICAO of the station you want to download the OBs and TAf for. Ex.
    :param issue_time: string. Ex. '122345' Means the 2345Z TAF on the 12th. If not specified it will get the last completed one
    :return: nothing. Just downloads the ob and taf_test aand saves them to seprates json files
    """
    time_struct = gmtime()  # prevents time racing

    # download the TAF form ADDS
    # get_taf.download_and_process_taf(icao, issue_time=taf_issue_time)
    if issue_time is not None:  # download the TAF with the user specified issue_time
        taf = get_taf.download_adds_taf_as_xml(icao=icao, taf_issue_time=issue_time)
    else:  # user did not specify, just download the last completed taf_test
        taf = get_taf.download_most_recent_completed_taf(icao)

    # processes the xml taf_test into a list
    taf = get_taf.get_raw_taf_from_xml(taf)

    # fix taf_test times
    get_taf.fix_taf_times(taf)

    # convert the taf_test into what will be saved into the JSON file
    taf_for_json = get_taf.taf_to_json_friendly_format(taf)

    # saves the TAF json and saves the path of the saved json into a var
    taf_path = get_taf.taf_to_json(taf_for_json)


    # get time frame of the TAF which will be used get the matching OBs for the same time frame
    taf_start_and_end_time = get_taf_time_frame(taf, time_struct)
    taf_start = taf_start_and_end_time['start']
    taf_start = move_time(taf_start, {'time_hours': -3, 'time_minutes': 0, 'day': 0, 'month': 0, 'year': 0})  # get obs 3 hour before TAF start
    taf_end = taf_start_and_end_time['end']
    taf_end = move_time(taf_end, {'time_hours': 3, 'time_minutes': 0, 'day': 0, 'month': 0, 'year': 0})  # get obs 3 hours after TAF ends


    # downloads the OBs from ADDS
    obs = get_obs.download_adds_obs_as_xml(icao, hours_before=None, time_frame_start=taf_start, time_frame_end=taf_end)

    # convert the xml OBs into a list
    obs = get_obs.get_raw_obs_from_xml(obs, reverse_order=False)

    # convert the OBs into the JSON format
    obs = get_obs.raw_obs_to_json_friendly_format(obs)

    # save to JSON
    ob_path = get_obs.obs_to_json(obs)  # save to JSON and get the path it was saved to

    return {'ob_path': ob_path, 'taf_path': taf_path}


if __name__ == '__main__':
    paths = get_ob_and_taf_json('KDFW')
    print(paths['ob_path'])
    print(paths['taf_path'])
