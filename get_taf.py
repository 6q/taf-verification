import urllib.request
import xml.etree.ElementTree as etree
import time
import json
import re
from other_functions.misc import pad_number, pretty_number, is_prob_line, is_fm_line
from other_functions.time_based_functions import move_time, get_current_time, which_date_came_first, \
    get_taf_time_length, \
    fix_taf_times, adjust_date_higher_taf, get_taf_issue_or_start_time_and_date, get_taf_end_time_and_date, \
    get_line_time_range, adjust_date_lower
from other_functions.common_metar_taf_elements import get_wind, convert_visibility, get_visibility, get_weather, \
    get_clouds


def convert_coded_height(coded_height, return_format='int'):
    """
    Cloud height and LLWS height are encoded 020, where 020 -> 2,000ft, 001 -> 100ft. Converts the coded height to the
    full actual height.
    :param coded_height: str or int. The height in ft in 3 digits form. Ex '020', '001', '250'
    :param return_format: string. Either 'str' or 'int'. Determines if 2044 or '2,044' is returned
    :return: str or int. The converted height in ft.
    """
    converted_height = int(coded_height) * 100  # 020 -> 2,000
    if return_format == 'int':
        return converted_height
    elif return_format == 'str':
        return pretty_number(converted_height)


def download_adds_taf_as_xml(icao, taf_issue_time, file_name=None):
    """
    Downloads a selected TAF from ADDS with the desired input time
    :param file_name: string. The name of the xml file. must include the extension .xml
    :param icao: input_str. The ICAO of the station
    :param taf_issue_time: input_str. The issue time of the TAF to be downloaded. Format is DDHHMM
    :return: xml_root. The usable portion of the XML containing the TAF
    """
    # get the file name if it was not provided
    if file_name is None:
        file_name = f'temp/TAF_{icao.upper()}_{taf_issue_time}.xml'  # TAF_KBAD_y2019_m08_d24_t0200Z would be ideal

    gm_time = time.gmtime()  # get the current full date and time.
    time_zulu = taf_issue_time[2:]  # get '1800' from '041800'
    day = taf_issue_time[:2]  # get '04' from '041800'
    month = gm_time[1]  # current month, might be adjusted 1 lower
    year = gm_time[0]  # current year, might be adjusted 1 lower
    adjusted_time = adjust_date_lower(time_zulu, day, month, year)  # adjust the month and year lower if need be
    month = adjusted_time['month']  # get the updated month
    year = adjusted_time['year']  # get the updated year
    current_time_dict = {'time': time_zulu, 'day': day, 'month': month, 'year': year}

    # lower and upper bound are created because ADDS only lets you get the TAF(taf_as_one_str) for a specific time range
    # if we want a 1800z TAF we should request TAFs between 1730 and 1830
    time_lower_bound = move_time(current_time_dict, {'time_hours': 0, 'time_minutes': -5, 'day': 0, 'month': 0, 'year': 0})
    time_upper_bound = move_time(current_time_dict, {'time_hours': 0, 'time_minutes': 5, 'day': 0, 'month': 0, 'year': 0})

    # seperate the bounds into the individual elements
    time_lower_bound_minutes = time_lower_bound['time'][2:]
    time_lower_bound_hours = time_lower_bound['time'][:2]
    time_lower_bound_day = pad_number(time_lower_bound['day'], 2)
    time_lower_bound_month = pad_number(time_lower_bound['month'], 2)
    time_lower_bound_year = time_lower_bound['year']

    time_upper_bound_minutes = time_upper_bound['time'][2:]
    time_upper_bound_hours = time_upper_bound['time'][:2]
    time_upper_bound_day = pad_number(time_upper_bound['day'], 2)
    time_upper_bound_month = pad_number(time_upper_bound['month'], 2)
    time_upper_bound_year = time_upper_bound['year']

    # ADDS desired time format 2019-08-04T17:00:00Z
    adds_taf_time_format = '{year}-{month}-{day}T{hour}:{minutes}:00Z'

    # Create the string for the lower bound ex 2019-08-04T17:00:00Z
    time_lower_bound_formatted = adds_taf_time_format.format(year=time_lower_bound_year, month=time_lower_bound_month,
                                                             day=time_lower_bound_day, hour=time_lower_bound_hours,
                                                             minutes=time_lower_bound_minutes)

    # Create the string for the upper bound ex 2019-08-04T17:00:00Z
    time_upper_bound_formatted = adds_taf_time_format.format(year=time_upper_bound_year, month=time_upper_bound_month,
                                                             day=time_upper_bound_day, hour=time_upper_bound_hours,
                                                             minutes=time_upper_bound_minutes)

    # the URL to down load from
    taf_url = 'https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&startTime={time_lower_bound}&endTime={time_upper_bound}&timeType=issue&stationString={icao}'.format(
        time_lower_bound=time_lower_bound_formatted, time_upper_bound=time_upper_bound_formatted, icao=icao)

    urllib.request.urlretrieve(taf_url, file_name)  # download the XML file

    tree = etree.parse(file_name)  # save the file
    root = tree.getroot()  # make the XML file usable

    # check if the downloaded file even contain TAFs, if not raise an error
    if int(root[6].attrib.get("num_results")) == 0:
        raise Exception(f'No TAFs were found for\nicao: {icao}\nissue_time: {taf_issue_time}')

    return root


def split_taf(taf_as_one_str):
    """
    Splits a TAF that is a single string into a list of strings. Every string is it'taf_as_one_str own index.
    :param taf_as_one_str: input_str. The entire TAF as a single string
    :return: list. A list of strings. Every string it it'taf_as_one_str own index in the TAF.
    turns: 'blabla BECMG blabla BECMG tatabla TEMPO blabla tatablabla BECMG tatabla'
    into: ['blabla', 'BECMG blabla', 'BECMG tatabla', 'TEMPO blabla tatablabla', 'BECMG tatabla']
    """
    taf_line_by_line = []
    taf_line_seperators = ['BECMG', 'TEMPO', 'FM', 'PROB']  # what to seperate the TAF lines by
    index_start = 0  # used later as the start of the index
    taf_len = len(taf_as_one_str)  # used to add the last index

    for index, char in enumerate(taf_as_one_str):
        # finds the start to the next index and uses it as the end of the current index
        if taf_as_one_str[index:index + 5] in taf_line_seperators or taf_as_one_str[index:index + 4] in taf_line_seperators or taf_as_one_str[index:index + 2] in taf_line_seperators:
            # add individual TAF index to the list of lines for the TAF
            taf_line_by_line.append(taf_as_one_str[index_start:index])
            index_start = index  # update the index at which the next index of the TAF starts

        # add the last index
        if index == taf_len - 1:
            taf_line_by_line.append(taf_as_one_str[index_start:])

    return taf_line_by_line


def get_raw_taf_from_xml(xml_root):
    """
    Gets the raw TAF from the xml from ADDS
    :param xml_root: xml root object. What is returned by download_adds_taf_as_xml
    :return: list. The entire TAF as a list of strings. Every string is one line of tyhe TAF.
    """
    taf_str = xml_root[6][0].find('raw_text').text  # gets the entire TAF as a single index input_str.
    return split_taf(taf_str)


def get_minimum_alstg(taf_line):
    """
    Get the mimimum forecast pressure if the TAF has it. Looks like QNH2992INS.
    :param taf_line: str. Any line of the TAF.
    :return: int. The minimum forecast pressure. Ex 2992, 2987, 3002
    """
    regex = r'(?<=QNH)(\d{4})(?=INS)'
    try:
        min_alstg = re.search(regex, taf_line).group()
    except AttributeError:  # if there is no min alstg encode din the line. Happens with TEMPO and FM lines.
        return None
    return int(min_alstg)


def get_max_min_temp(taf, time_struct):
    """
    Get the max and min forecasted temperature from the TAF
    :param taf: list. The entire TAF as list of strings where each line is it's own input_str in the list.
    :param time_struct: time.struct_time.The result of running time.gmtime() needed to avoid time racing when
    :return: dict. A diction of the max and min temp and the time at when the each occured
    """
    regex = r'TX(?P<max_temp_sign>M?)(?P<max_temp>\d{2})/(?P<max_temp_day>\d{2})(?P<max_temp_hour>\d{2})Z TN(?P<min_temp_sign>M?)(?P<min_temp>\d{2})/(?P<min_temp_day>\d{2})(?P<min_temp_hour>\d{2})Z'
    try:
        max_min_temp_group = re.search(regex, taf[-1])  # search the last line for the max and min temps
        # get the individual elements from the max and min temp groups
        max_temp_sign = max_min_temp_group.group('max_temp_sign')
        max_temp = max_min_temp_group.group('max_temp')
        max_temp_day = int(max_min_temp_group.group('max_temp_day'))
        max_temp_time = '{}00'.format(max_min_temp_group.group('max_temp_hour'))
        min_temp_sign = max_min_temp_group.group('min_temp_sign')
        min_temp = max_min_temp_group.group('min_temp')
        min_temp_day = int(max_min_temp_group.group('min_temp_day'))
        min_temp_time = '{}00'.format(max_min_temp_group.group('min_temp_hour'))

        # make the max temp negative if need be
        if max_temp_sign == 'M':  # max temp is negative
            max_temp = pad_number((int(max_temp) * - 1), 2)
        else:  # convert to int even if not negative
            max_temp = int(max_temp)

        # make the max temp negative if need be
        if min_temp_sign == 'M':  # min temp is negative
            min_temp = pad_number((int(min_temp) * - 1), 2)
        else:  # convert to int even if not negative
            min_temp = int(min_temp)

        # get the date and time the taf_test was used in
        issue_time_and_date = get_taf_issue_or_start_time_and_date(taf[0], time_struct)
        issue_day = issue_time_and_date['day']
        issue_month = issue_time_and_date['month']
        issue_year = issue_time_and_date['year']

        # fix the max temp month and year if need be
        max_temp_fixed_month_and_year = adjust_date_higher_taf(max_temp_day, issue_day, issue_month, issue_year)
        max_temp_month = max_temp_fixed_month_and_year['month']
        max_temp_year = max_temp_fixed_month_and_year['year']

        # fix the min temp month and year if need be
        min_temp_fixed_month_and_year = adjust_date_higher_taf(min_temp_day, issue_day, issue_month, issue_year)
        min_temp_month = min_temp_fixed_month_and_year['month']
        min_temp_year = min_temp_fixed_month_and_year['year']

    except AttributeError:  # if max and min temp are not encoded into the TAF, just set it all to None
        max_temp = None
        max_temp_day = None
        max_temp_time = None
        max_temp_month = None
        max_temp_year = None
        min_temp = None
        min_temp_day = None
        min_temp_time = None
        min_temp_month = None
        min_temp_year = None

    return {'max_temp_group':
                {'max_temp': max_temp,
                 'max_temp_time': max_temp_time,
                 'max_temp_day': max_temp_day,
                 'max_temp_month': max_temp_month,
                 'max_temp_year': max_temp_year},
            'min_temp_group':
                {'min_temp': min_temp,
                 'min_temp_time': min_temp_time,
                 'min_temp_day': min_temp_day,
                 'min_temp_month': min_temp_month,
                 'min_temp_year': min_temp_year}
            }


def get_wind_shear(taf_line):
    """
    Gets the low level wind shear fomr the line of th TAF. LLWS is encoded as WS020/15040KT.
    :param taf_line: str. Any line of the TAF.
    :return: dict. A dictionary of the wind shear elements.
    """
    regex = r'WS(?P<height>\d{3})/(?P<direction>\d{3})(?P<speed>\d{2})KT'
    wind_shear = re.search(regex, taf_line)
    try:
        height = convert_coded_height(wind_shear.group('height'))
        direction = wind_shear.group('direction')
        speed = wind_shear.group('speed')
    except AttributeError:  # there is no LLWS in the line of the TAF
        return None

    return {'height': height,
            'wind_direction': direction,
            'wind_speed': speed}


def get_prob_chance(taf_line):
    """
    Get the amount of probility form a PROP line. 'PROB30' -> 30
    :param taf_line: str. Any line of the TAF.
    :return: int or Nonetype.
    """
    try:
        # get the prob number convert to percentage, PROB30 -> 30
        percent_chance = (re.search(r'PROB(?P<percent_chance>\d{2}) ', taf_line).group('percent_chance'))
        return int(percent_chance)
    except AttributeError:  # if it is not a PROB line
        return None


def is_taf_completed(taf, time_struct):
    """
    A TAF is considered complete when the current time is after the end time of the TAF. Works by comparing the TAF end
    time to the current time
    :param taf: list. The entire TAF as list of strings where each line is it's own input_str in the list.
    :param time_struct: time.struct_time.The result of running time.gmtime() needed to avoid time racing when
    :return: boolean. True if the TAF is completed, False if it is not.
    """
    taf_end_time = get_taf_end_time_and_date(taf, time_struct)
    current_time = get_current_time(time_struct)

    return which_date_came_first(taf_end_time, current_time) == 1  # if the taf_test ends before the current time it is complete. 1 because that's how the function works


def get_line_type(taf_line):
    """
    Determines if a line is predominant or temporary line.
    :param taf_line: str. Any line of the TAF.
    :return: str. The type of of line it is 'predominant' or 'temporary'.
    """
    is_first_line = bool(re.search(r'(\d{6}Z \d{4}/\d{4})', taf_line))  # first lines have 150600Z 1506/1606
    is_fm_line_bool = is_fm_line(taf_line)
    is_becmg_line = 'BECMG' in taf_line

    if is_first_line or is_becmg_line or is_fm_line_bool:
        return 'predominant'
    elif 'TEMPO' in taf_line or is_prob_line(taf_line):
        return 'temporary'
    else:
        raise Exception(f'Could not determine if the line is predominant or temporary\n{taf_line}')


def get_list_of_completed_tafs(icao, time_struct=None):
    """
    A TAF is considered complete when the current time is after the end time of the TAF.
    :param icao: string. The ICAO of the station. EX. 'KBAD, 'KSFO'
    :return: list. The issue times of the latest completed TAFs. The newest TAFs are the beginning of the list.
    """
    if time_struct is None:  # if no time_sctruct is given get it at run time
        time_struct = time.gmtime()

    file_name = f'temp/TAF_{icao}_last_500hr.xml'

    completed_tafs = []  # a list of the issuetimes of TAFs in the format 051800. THis format b/c they are downloaded from ADDs in this format

    # first download all the TAFS within the last 400 hours
    taf_url = f'https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=tafs&requestType=retrieve&format=xml&stationString={icao}&hoursBeforeNow=500.0&timeType=issue'

    urllib.request.urlretrieve(taf_url, file_name)  # download the XML file
    tree = etree.parse(file_name)  # save the file
    xml_root = tree.getroot()
    number_of_taf = int(xml_root[6].attrib.get("num_results"))  # how many TAFs there are in total

    current_date_time = get_current_time(time_struct)  # ge tthe current date and time, neede b/c TAFs do not encode month or year

    for index in range(number_of_taf):  # for taf_test in the xml of all the taf_test get the end time
        taf = xml_root[6][index].find("raw_text").text  # get a single complete TAF from the XML
        taf = split_taf(taf)
        taf = fix_taf_times(taf, current_date_time['month'], current_date_time['year'])

        if is_taf_completed(taf, time_struct):  # if completed
            taf_issue_date_time = get_taf_issue_or_start_time_and_date(taf[0], time_struct, issue_or_start='issue')  # ad the issue time of the taf_test to the list
            taf_issue_time_hours_minutes = taf_issue_date_time['time']  # Ex. '1405'
            taf_issue_day = pad_number(taf_issue_date_time['day'], 2)  # Ex. '03'
            taf_issue_date_time = f'{taf_issue_day}{taf_issue_time_hours_minutes}'  # Ex. '031405'
            completed_tafs.append(taf_issue_date_time)
        else:  # taf_test is still valid now
            pass

    return completed_tafs  # ['120600', '300500']


def download_most_recent_completed_taf(icao, time_struct=None):
    """
    :param icao: string. The ICAO of the station. EX. 'KBAD, 'KSFO'
    :param time_struct: time.struct_time.The result of running time.gmtime() needed to avoid time racing when
    :return: xml_root. The usable portion of the XML containing the TAF
    """
    most_recent_taf_issue_day_time = get_list_of_completed_tafs(icao, time_struct=None)[0]
    root = download_adds_taf_as_xml(icao, most_recent_taf_issue_day_time)
    return root


def get_taf_from_txt_file(txt_file):
    """
    Get the TAF from a text file or really any other file. TAF should have every line of the TAF on a new line in the
    text file too.
    :param txt_file:
    :return: list. List of strings
    """
    line_1 = 'None'
    # determine if the TAF is all on one line
    if 'BECMG' in line_1 or 'TEMPO' in line_1 or is_prob_line(line_1):
        pass

    # open the file
    with open(txt_file) as taf_in:
        taf_in = taf_in.readlines()
        line_first = taf_in[0]

        # determine if the TAF is all on one line
        if 'BECMG' in line_first or 'TEMPO' in line_first or is_prob_line(line_first) or is_fm_line(line_first):
            return split_taf(taf_in[0])  # return list to str

        else:  # not a single line TAF, split up taf_test into list of str
            taf = []  # list of str where every str is it's own line

            for line in taf_in:
                taf.append(line.lstrip().rstrip())  # add the line without the leading whitespace that is common when copy & pasting from ADDS. Also remove \n at end of line
            return taf


def taf_to_json_friendly_format(taf):
    """
    Converts a TAF to a disction that can later be encoded into a json file
    :param taf: lst. A list of strings.
    :return: dict. A dict of the TAF where each index is a new entry
    """
    taf_for_json = {}  # a dict of the entire taf_test index by index
    taf_for_json['meta'] = {}  # set up the empty meta dict
    first_line = taf[0]
    regex_icao = r'(?P<icao>.{,4})(?= \d{6}Z)'  # find the up to 4 char before the date time group [KBAD] 151800Z
    taf_for_json['meta']['icao'] = re.search(regex_icao, first_line).group()  # save the icao of the TAF to start of the dict

    time_struct = time.gmtime()  # prevent time racing. Otherwise time would have to be called once per loop

    # add the full date and time the taf_test was issued on
    taf_issue_date_time = get_taf_issue_or_start_time_and_date(first_line, time_struct, issue_or_start='issue')
    taf_for_json['meta']['issue_year'] = taf_issue_date_time['year']
    taf_for_json['meta']['issue_month'] = taf_issue_date_time['month']
    taf_for_json['meta']['issue_day'] = taf_issue_date_time['day']
    taf_for_json['meta']['issue_time'] = taf_issue_date_time['time']

    # get the start time (not issue)
    taf_start_date_time = get_taf_issue_or_start_time_and_date(first_line, time_struct, issue_or_start='start')
    taf_for_json['meta']['start_year'] = taf_start_date_time['year']
    taf_for_json['meta']['start_month'] = taf_start_date_time['month']
    taf_for_json['meta']['start_day'] = taf_start_date_time['day']
    taf_for_json['meta']['start_time'] = taf_start_date_time['time']

    # get taf_test end time and save it to the beginning
    taf_end_time = get_taf_end_time_and_date(taf, time_struct)
    taf_for_json['meta']['expire_year'] = taf_end_time['year']
    taf_for_json['meta']['expire_month'] = taf_end_time['month']
    taf_for_json['meta']['expire_day'] = taf_end_time['day']
    taf_for_json['meta']['expire_time'] = taf_end_time['time']

    # get the length of the TAF in hours and store it into the JSON
    taf_for_json['meta']['length_hours'] = get_taf_time_length(taf, time_struct)

    # get the max and min temp of the TAF. Will say none for all attributes if it is not in the TAF.
    taf_for_json['temperature_extremes'] = get_max_min_temp(taf, time_struct)

    taf_for_json['data'] = {}  # setup the empty base dict for all the obs
    for index, line in enumerate(taf):  # for every index of the TAF

        taf_line_temporary = {}  # set up a temporary dictionary for each taf_test index

        taf_name = 'taf_line_{}'.format(index + 1)  # the name of the index

        taf_line_temporary['line_type'] = get_line_type(line)

        if is_prob_line(line):
            taf_line_temporary['percent_chance'] = get_prob_chance(line)

        # get the time range the line is active for
        taf_line_temporary['time_frame'] = get_line_time_range(taf, index, time_struct)

        taf_line_temporary['wind'] = get_wind(line)  # get the wind

        taf_line_temporary['visibility'] = convert_visibility(get_visibility(line))  # get visibility

        taf_line_temporary['weather'] = get_weather(line)  # get weather

        taf_line_temporary['clouds'] = get_clouds(line)

        taf_line_temporary['minimum_alstg'] = get_minimum_alstg(line)

        taf_line_temporary['low_level_wind_shear'] = get_wind_shear(line)

        taf_for_json['data'][taf_name] = taf_line_temporary

    return taf_for_json


def taf_to_json(taf):
    """
    Takes a dict of lines of an TAF and saves it to a .json file
    :param taf: dict. A dict of the taf_test index by index
    :return: string. The name of the JSON file the TAF was saved to.
    """
    icao = taf['meta']['icao']
    issue_time = taf['meta']['issue_time']
    issue_day = taf['meta']['issue_day']
    issue_month = taf['meta']['issue_month']
    issue_year = taf['meta']['issue_year']
    file_name = f'temp/TAF_{icao}_isy{issue_year}_ism{issue_month}_isd{issue_day}_ist{issue_time}.json'

    with open(file_name, 'w') as outfile:
        json.dump(taf, outfile, indent=4)

    return file_name  # filename is used later to process the correct file


def download_and_process_taf(icao, issue_time=None):
    """
    A wrapper function for downloading the TAF and processing it so it can be saved into a JSON format. Also saves the JSON format
    :param icao: stirng. The ICAO of the station.
    :param issue_time: string. Ex. '122345' Means the 2345Z TAF on the 12th. If not specified it will get the last completed one
    :return: nothing. Just downloads the TAF and saves the JSON version of it.
    """
    # first download the xml and get the xml root
    if issue_time is not None:  # download the TAF with the user specified issue_time
        taf = download_adds_taf_as_xml(icao=icao, taf_issue_time=issue_time)
    else:  # user did not specify, just download the last completed taf_test
        taf = download_most_recent_completed_taf(icao)

    # processes the xml taf_test into a list
    taf = get_raw_taf_from_xml(taf)

    # fix taf_test times
    fix_taf_times(taf)

    # convert the taf_test into what will be saved into the JSON file
    taf = taf_to_json_friendly_format(taf)

    # saves the TAF json into the
    taf_to_json(taf)


if __name__ == '__main__':


    # print(get_taf_end_time_and_date(taf_test, time.gmtime()))
    # taf_test = download_adds_taf_as_xml(icao='KTUL', taf_issue_time='122345')
    # taf_test = download_adds_taf_as_xml(icao='KCLE', taf_issue_time='162334')
    # taf_test = get_raw_taf_from_xml(taf_test)
    # taf_test = ['KICT 312340Z 3100/3124 15012G22KT P6SM FEW200 ', 'FM010500 17012KT P6SM SCT250 ', 'FM010800 18011KT P6SM VCSH SCT250 WS020/21045KT ', 'FM011100 16009KT P6SM SCT250 ', 'FM011500 18012KT P6SM SCT200']
    # print(get_list_of_completed_tafs('KBOS'))

    taf_test = download_most_recent_completed_taf('KMEM')
    # print(get_taf_from_txt_file('taf_test.txt'))
    taf_test = get_raw_taf_from_xml(taf_test)
    taf_test = fix_taf_times(taf_test)
    # print(taf_test)
    taf_test = taf_to_json_friendly_format(taf_test)
    taf_to_json(taf_test)
    # print(taf_test)
