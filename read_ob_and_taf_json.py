"""
This file has can read the .json for the OBs and TAFs and create x and y axis info for the plots.
"""

import json
# import matplotlib.pyplot as plt
# from matplotlib.ticker import StrMethodFormatter
import plotly.graph_objects as go
from plotly.subplots import make_subplots
from other_functions.time_based_functions import which_date_came_first
from other_functions.misc import pad_number
import pandas as pd


def get_ob():
    pass


class WeatherEntry:
    """
    TODO There will have to be another class that contains the entire TAF and all the OBS as a bunch of instances of this class

    A weather entry is created for every new ob and for every new line of a forcast. It is created everytime the obs of
    TAF changes. This will serve as the x-values.

    A Weather_Entry contains the time it happened. Until when it lasted and the weather elements (y-values)

    For the given time it holds the valid observation, valid predominant forecast of the TAF, and if applicable the
    temporary forecast conditions.
    """

    def __init__(self):
        # self.icao = None
        # self.type = None  # if OB or forecast
        # self.forecast_type = None  # Predominant of Temporary
        self.is_ob = False
        self.is_predom = False
        self.is_tempo = False


        # time
        self.year = None  # 2019
        self.month = None  # 8
        self.day = None  # 15
        self.time = None  # '1304'

        # Observation (METAR)

        self.ob_wind_dir = None
        self.ob_wind_spd = None
        self.ob_wind_gust = None

        self.ob_vis = None
        self.ob_wx = None
        self.ob_sky_con = None
        self.ob_temp = None
        self.ob_temp_dew = None
        self.ob_alstg = None

        # pk wnd
        self.ob_is_pk_wnd = None
        self.ob_pk_wnd_dir = None
        self.ob_pk_wnd_spd = None

        # Predominant Forecast (TAF 1st line, BECMG, FM)
        self.taf_predom_wind_dir = None
        self.taf_predom_wind_spd = None
        self.taf_predom_wind_gust = None

        self.taf_predom_vis = None
        self.taf_predom_wx = None
        self.taf_predom_sky_con = None
        self.taf_predom_alstg = None

        # Temporary Forecast (TAF TEMPO)
        self.tempo_end_year = None
        self.tempo_end_month = None
        self.tempo_end_day = None
        self.tempo_end_time = None

        self.taf_tempo_wind_dir = None
        self.taf_tempo_wind_spd = None
        self.taf_tempo_wind_gust = None

        self.taf_tempo_vis = None
        self.taf_tempo_wx = None
        self.taf_tempo_sky_con = None
        # print('done with init')  # DEBUG

    def __str__(self):
        return f"""
        is OB: {self.is_ob}
        is Predominant TAF line: {self.is_predom}
        is Temporary TAF line: {self.is_tempo}
        is Peak Wind: {self.ob_is_pk_wnd}
        
        Year:  {self.year}
        Month: {self.month}
        Day:   {self.day}
        Time:  {self.time}
        
        ----------------------------------------------------
        OB - wind direction: {self.ob_wind_dir}
        OB - wind speed:     {self.ob_wind_spd}
        OB - wind gust:      {self.ob_wind_gust}
        
        OB - visibility: {self.ob_vis}
        
        OB - weather:
        {self.ob_wx}
        
        OB - sky condition (clouds):
        {self.ob_sky_con}
        
        OB - temperature (C): {self.ob_temp}
        OB - dew point (C):   {self.ob_temp_dew}
        OB - altimeter settings: {self.ob_alstg}
        
        ----------------------------------------------------
        PK WND - Wind direction: {self.ob_pk_wnd_dir}
        PK WND - Wind speed: {self.ob_pk_wnd_spd}
        
        ----------------------------------------------------
        TAF Predominant - wind direction: {self.taf_predom_wind_dir}
        TAF Predominant - wind speed:     {self.taf_predom_wind_spd}
        TAF Predominant - wind gust:      {self.taf_predom_wind_gust}
        
        TAF Predominant - visibility: {self.taf_predom_vis}
        
        TAF Predominant - weather:
        {self.taf_predom_wx}
        
        TAF Predominant - sky condition (clouds):
        {self.taf_predom_sky_con}
        
        ----------------------------------------------------
        TAF Temporary - end year: {self.tempo_end_year}
        TAF Temporary - end year: {self.tempo_end_month}
        TAF Temporary - end year: {self.tempo_end_day}
        TAF Temporary - end year: {self.tempo_end_time}
        
        TAF Temporary - wind direction: {self.taf_tempo_wind_dir}
        TAF Temporary - wind speed:     {self.taf_tempo_wind_spd}
        TAF Temporary - wind gust:      {self.taf_tempo_wind_gust}
        
        TAF Temporary - visibility: {self.taf_tempo_vis}
        
        TAF Temporary - weather:
        {self.taf_tempo_wx}
        
        TAF Temporary - sky condition (clouds):
        {self.taf_tempo_sky_con}
        """

    def set_info(self, single_ob_or_taf_line_dict):
        """
        Set the info found in the __init__. Takes as input either a OB or TAF and set the info found in it (time and the
        weather elements)
        :param single_ob_or_taf_line_dict: dictionary . A dictionary of either a single ob or a single forecast.
        :return: nothing
        """

        # first determine if OB or TAF
        try:
            if single_ob_or_taf_line_dict['line_type'] == 'predominant':  # it first line, BECMG line or FM line
                self.is_predom = True
                self.year = single_ob_or_taf_line_dict['time_frame']['hard_start_year']
                self.month = single_ob_or_taf_line_dict['time_frame']['hard_start_month']
                self.day = single_ob_or_taf_line_dict['time_frame']['hard_start_day']
                self.time = single_ob_or_taf_line_dict['time_frame']['hard_start_time']

                self.taf_predom_wind_dir = single_ob_or_taf_line_dict['wind']['wind_dir']
                self.taf_predom_wind_spd = single_ob_or_taf_line_dict['wind']['wind_speed_sustained']
                self.taf_predom_wind_gust = single_ob_or_taf_line_dict['wind']['wind_speed_gust']

                self.taf_predom_vis = single_ob_or_taf_line_dict['visibility']
                self.taf_predom_wx = single_ob_or_taf_line_dict['weather']
                self.taf_predom_sky_con = single_ob_or_taf_line_dict['clouds']
                self.taf_predom_alstg = single_ob_or_taf_line_dict['minimum_alstg']

            elif single_ob_or_taf_line_dict['line_type'] == 'temporary':
                self.is_tempo = True
                # start time
                self.year = single_ob_or_taf_line_dict['time_frame']['hard_start_year']
                self.month = single_ob_or_taf_line_dict['time_frame']['hard_start_month']
                self.day = single_ob_or_taf_line_dict['time_frame']['hard_start_day']
                self.time = single_ob_or_taf_line_dict['time_frame']['hard_start_time']
                # end time
                self.tempo_end_year = single_ob_or_taf_line_dict['time_frame']['hard_end_year']
                self.tempo_end_month = single_ob_or_taf_line_dict['time_frame']['hard_end_month']
                self.tempo_end_day = single_ob_or_taf_line_dict['time_frame']['hard_end_day']
                self.tempo_end_time = single_ob_or_taf_line_dict['time_frame']['hard_end_time']

                self.taf_tempo_wind_dir = single_ob_or_taf_line_dict['wind']['wind_dir']
                self.taf_tempo_wind_spd = single_ob_or_taf_line_dict['wind']['wind_speed_sustained']
                self.taf_tempo_wind_gust = single_ob_or_taf_line_dict['wind']['wind_speed_gust']

                self.taf_tempo_vis = single_ob_or_taf_line_dict['visibility']
                self.taf_tempo_wx = single_ob_or_taf_line_dict['weather']
                self.taf_tempo_sky_con = single_ob_or_taf_line_dict['clouds']

        except KeyError:  # must be an OB. If OB it can be a PK WND ob or regular ob
            # both ob and PK WND have time
            self.year = single_ob_or_taf_line_dict['time']['issue_year']
            self.month = single_ob_or_taf_line_dict['time']['issue_month']
            self.day = single_ob_or_taf_line_dict['time']['issue_day']
            self.time = single_ob_or_taf_line_dict['time']['issue_time']

            try:  # see if it is a PK WND ob
                self.ob_is_pk_wnd = bool(single_ob_or_taf_line_dict['peak_wind'])  # will be true if there is line is PK WND
                self.ob_pk_wnd_dir = single_ob_or_taf_line_dict['peak_wind']['pk_wind_dir']
                self.ob_pk_wnd_spd = single_ob_or_taf_line_dict['peak_wind']['pk_wind_speed']

            except KeyError:  # is regular ob
                self.is_ob = True

                self.ob_wind_dir = single_ob_or_taf_line_dict['wind']['wind_dir']
                self.ob_wind_spd = single_ob_or_taf_line_dict['wind']['wind_speed_sustained']
                self.ob_wind_gust = single_ob_or_taf_line_dict['wind']['wind_speed_gust']

                self.ob_vis = single_ob_or_taf_line_dict['visibility']
                self.ob_wx = single_ob_or_taf_line_dict['weather']
                self.ob_sky_con = single_ob_or_taf_line_dict['clouds']
                self.ob_temp = single_ob_or_taf_line_dict['temperature']
                self.ob_temp_dew = single_ob_or_taf_line_dict['dew_point']
                self.ob_alstg = single_ob_or_taf_line_dict['alstg']
        # print('done setting info')  # DEBUG

    def get_data_types(self):
        """
        Gets which types of data are stored in self. Data is ob, predom, or tempo
        :return: set Ex. {'ob', 'tempo'} or {'predom', 'tempo'} or {'ob'}, etc..
        """
        list_data_types = set()
        if self.is_ob:
            list_data_types.add('ob')
        if self.is_predom:
            list_data_types.add('predom')
        if self.is_tempo:
            list_data_types.add('tempo')

        return list_data_types

    @staticmethod
    def merge_duplicates(*args, elements_to_merge={'ob', 'predom', 'tempo'}):
        """
        If there are 2 entries with the same time, this wil merge them. This will happen when a OB has the same time as
        a TAF line. This will make one weather entry have 2 or more components to it. Uses time from the first element.
        :param elements_to_merge: list. ['ob', 'predom', 'tempo'] be default merges all possible elements. Should only be used in
        populate_empty_required 'ob' to merge the required elements.
        :param args: list of WeatherEntry(). 2 or 3 instances of the class WeatherEntry
        :return: WeatherEntry(). A new instance of WeatherEntry() with the two inputs merged if they happened at the
        same time. Otherwise it does nothing
        """
        entry_ob = entry_predom = entry_tempo = None  # None by default so copying from these doesn't throw NameError

        # print(f'DEBUG pring: *args: {args}')
        # determine which arg is what
        for entry in args:
            if entry.is_ob:
                entry_ob = entry
            elif entry.is_predom:
                entry_predom = entry
            elif entry.is_tempo:
                entry_tempo = entry

        # print(f'one of the args is_ob: {bool(entry_ob)}')
        # print(f'one of the args is_predom: {bool(entry_predom)}')
        # print(f'one of the args is_tempo: {bool(entry_tempo)}')

        # create the new entry
        entry_new = WeatherEntry()
        entry_new.year = args[0].year
        entry_new.month = args[0].month
        entry_new.day = args[0].day
        entry_new.time = args[0].time

        if entry_ob and 'ob' in elements_to_merge:
            entry_new.is_ob = True
            entry_new.ob_wind_dir = entry_ob.ob_wind_dir
            entry_new.ob_wind_spd = entry_ob.ob_wind_spd
            entry_new.ob_wind_gust = entry_ob.ob_wind_gust
            entry_new.ob_vis = entry_ob.ob_vis
            entry_new.ob_wx = entry_ob.ob_wx
            entry_new.ob_sky_con = entry_ob.ob_sky_con
            entry_new.ob_temp = entry_ob.ob_temp
            entry_new.ob_temp_dew = entry_ob.ob_temp_dew
            entry_new.ob_alstg = entry_ob.ob_alstg
            if entry_ob.ob_is_pk_wnd:
                entry_new.ob_is_pk_wnd = True
                entry_new.ob_pk_wnd_dir = entry_ob.ob_pk_wnd_dir
                entry_new.ob_pk_wnd_spd = entry_ob.ob_pk_wnd_spd

        if entry_predom and 'predom' in elements_to_merge:
            entry_new.is_predom = True
            entry_new.taf_predom_wind_dir = entry_predom.taf_predom_wind_dir
            entry_new.taf_predom_wind_spd = entry_predom.taf_predom_wind_spd
            entry_new.taf_predom_wind_gust = entry_predom.taf_predom_wind_gust
            entry_new.taf_predom_vis = entry_predom.taf_predom_vis
            entry_new.taf_predom_wx = entry_predom.taf_predom_wx
            entry_new.taf_predom_sky_con = entry_predom.taf_predom_sky_con
            entry_new.taf_predom_alstg = entry_predom.taf_predom_alstg

        if entry_tempo and 'tempo' in elements_to_merge:
            entry_new.is_tempo = True
            entry_new.tempo_end_year = entry_tempo.tempo_end_year
            entry_new.tempo_end_month = entry_tempo.tempo_end_month
            entry_new.tempo_end_day = entry_tempo.tempo_end_day
            entry_new.tempo_end_time = entry_tempo.tempo_end_time
            entry_new.taf_tempo_wind_dir = entry_tempo.taf_tempo_wind_dir
            entry_new.taf_tempo_wind_spd = entry_tempo.taf_tempo_wind_spd
            entry_new.taf_tempo_wind_gust = entry_tempo.taf_tempo_wind_gust
            entry_new.taf_tempo_vis = entry_tempo.taf_tempo_vis
            entry_new.taf_tempo_wx = entry_tempo.taf_tempo_wx
            entry_new.taf_tempo_sky_con = entry_tempo.taf_tempo_sky_con

        return entry_new

    def __eq__(self, other):
        if isinstance(other, WeatherEntry):
            all_checks = []

            # time
            all_checks.append(self.year == other.year)
            all_checks.append(self.month == other.month)
            all_checks.append(self.day == other.day)
            all_checks.append(self.time == other.time)

            # predom
            all_checks.append(self.is_predom == other.is_predom)
            all_checks.append(self.taf_predom_wind_dir == other.taf_predom_wind_dir)
            all_checks.append(self.taf_predom_wind_spd == other.taf_predom_wind_spd)
            all_checks.append(self.taf_predom_wind_gust == other.taf_predom_wind_gust)
            all_checks.append(self.taf_predom_vis == other.taf_predom_vis)
            all_checks.append(self.taf_predom_wx == other.taf_predom_wx)
            all_checks.append(self.taf_predom_sky_con == other.taf_predom_sky_con)
            all_checks.append(self.taf_predom_alstg == other.taf_predom_alstg)

            # tempo
            all_checks.append(self.is_tempo == other.is_tempo)
            all_checks.append(self.taf_tempo_wind_dir == other.taf_tempo_wind_dir)
            all_checks.append(self.taf_tempo_wind_spd == other.taf_tempo_wind_spd)
            all_checks.append(self.taf_tempo_wind_gust == other.taf_tempo_wind_gust)
            all_checks.append(self.taf_tempo_vis == other.taf_tempo_vis)
            all_checks.append(self.taf_tempo_wx == other.taf_tempo_wx)
            all_checks.append(self.taf_tempo_sky_con == other.taf_tempo_sky_con)
            all_checks.append(self.tempo_end_year == other.tempo_end_year)
            all_checks.append(self.tempo_end_month == other.tempo_end_month)
            all_checks.append(self.tempo_end_day == other.tempo_end_day)
            all_checks.append(self.tempo_end_time == other.tempo_end_time)

            # ob
            all_checks.append(self.is_ob == other.is_ob)
            all_checks.append(self.ob_wind_dir == other.ob_wind_dir)
            all_checks.append(self.ob_wind_spd == other.ob_wind_spd)
            all_checks.append(self.ob_wind_gust == other.ob_wind_gust)
            all_checks.append(self.ob_vis == other.ob_vis)
            all_checks.append(self.ob_wx == other.ob_wx)
            all_checks.append(self.ob_sky_con == other.ob_sky_con)
            all_checks.append(self.ob_temp == other.ob_temp)
            all_checks.append(self.ob_temp_dew == other.ob_temp_dew)
            all_checks.append(self.ob_alstg == other.ob_alstg)
            all_checks.append(self.ob_is_pk_wnd == other.ob_is_pk_wnd)
            all_checks.append(self.ob_pk_wnd_dir == other.ob_pk_wnd_dir)
            all_checks.append(self.ob_pk_wnd_spd == other.ob_pk_wnd_spd)

            return all(all_checks)

    def get_entry_time(self):
        """
        Gets the time of the WeatherEntry
         :return: dict. Ex. {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
        """
        return {'time': self.time, 'day': self.day, 'month': self.month, 'year': self.year}

    def get_tempo_end(self):
        """
        Gets the TEMPO end time.
        :return: dict. Ex. {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
        """
        if self.is_tempo:
            return {'time': self.tempo_end_time, 'day': self.tempo_end_day, 'month': self.tempo_end_month, 'year': self.tempo_end_year}
        else:
            return None  # line is not a tempo line

    def __lt__(self, other):
        """
        If the two entries happened at the same time it will returns False
        :param other: WeatherEntry(). Instance of the class WeatherEntry.
        :return: boolean. True if self is lass than other
        """

        if isinstance(other, WeatherEntry):
            if self.year <= other.year:
                if self.month <= other.month:
                    if self.day <= other.day:
                        if int(self.time) < int(other.time):
                            return True
            return False

    def get_dict_version(self, element_to_get='all'):
        """
        Do not change the weather itself.
        :param element_to_get: string. Which individual element to get, if None is specified than get all of them
        :return: dict. A dictionary version of the weather entry.
        """

        # always create the base dict and add the time (dict will always have time) TODO delete if no longer needed
        return_dict = {'meta': {
                            # date and time
                            'year': self.year,
                            'month': self.month,
                            'day': self.day,
                            'time': self.time},
                        'data':{
                            'is_predom': self.is_predom,
                            'is_tempo': self.is_tempo,
                            'is_ob': self.is_ob,
                        }}

        return {
            'meta': {
                # date and time
                'year': self.year,
                'month': self.month,
                'day': self.day,
                'time': self.time},

            'data': {
                # types of data include
                'is_predom': self.is_predom,
                'is_tempo': self.is_tempo,
                'is_ob': self.is_ob,
                'is_pk_wind': self.ob_is_pk_wnd,

                # predom
                'taf_predom_wind_dir': self.taf_predom_wind_dir,
                'taf_predom_wind_spd': self.taf_predom_wind_spd,
                'taf_predom_wind_gust': self.taf_predom_wind_gust,
                'taf_predom_vis': self.taf_predom_vis,
                'taf_predom_wx': self.taf_predom_wx,
                'taf_predom_sky_con': self.taf_predom_sky_con,
                'taf_predom_alstg': self.taf_predom_alstg,

                # tempo
                'taf_tempo_end_year': self.tempo_end_year,
                'taf_tempo_end_month': self.tempo_end_month,
                'taf_tempo_end_day': self.tempo_end_day,
                'taf_tempo_end_time': self.tempo_end_time,
                'taf_tempo_wind_dir': self.taf_tempo_wind_dir,
                'taf_tempo_wind_spd': self.taf_tempo_wind_spd,
                'taf_tempo_wind_gust': self.taf_tempo_wind_gust,
                'taf_tempo_vis': self.taf_tempo_vis,
                'taf_tempo_wx': self.taf_tempo_wx,
                'taf_tempo_sky_con': self.taf_tempo_sky_con,

                # ob
                'ob_wind_dir': self.ob_wind_dir,
                'ob_wind_spd': self.ob_wind_spd,
                'ob_wind_gust': self.ob_wind_gust,
                'ob_vis': self.ob_vis,
                'ob_wx': self.ob_wx,
                'ob_sky_con': self.ob_sky_con,
                'ob_temp': self.ob_temp,
                'ob_temp_dew': self.ob_temp_dew,
                'ob_alstg': self.ob_alstg,

                # pk wnd
                'pk_wind_dir': self.ob_pk_wnd_dir,
                'pk_wind_speed': self.ob_pk_wnd_spd
            }
        }


class AllWeather:
    """
    This is where all the OBS and where the TAF are stored as a series of WeatherEntry
    """

    def __init__(self):
        self.icao = None  # set later when the either ob or taf json is read
        self.weather_entries = []  # a dict of all the WeatherEntry() instances
        self.entries_counter = 0  # start counting at 0 not 1

        self.taf_expire_year = None
        self.taf_expire_month = None
        self.taf_expire_day = None
        self.taf_expire_time = None

        # plotting lists
        self.x_axis = []
        self.x_axis_pd = []
        self.x_axis_pk_wnd = []
        self.x_axis_pk_wnd_pd = []
        self.y_ob_alstg = []
        self.y_ob_wind_dir = []
        self.y_predom_wind_dir = []
        self.y_tempo_wind_dir = []
        self.y_ob_wind_spd = []
        self.y_pk_wind_dir = []
        self.y_ob_wind_gust_spd = []
        self.y_predom_wind_spd = []
        self.y_predom_wind_gust_spd = []
        self.y_tempo_wind_spd = []
        self.y_tempo_wind_gust_spd = []
        self.y_pk_wind_spd = []
        self.y_alstg = []

    def __str__(self):
        to_return = []
        for count, entry in enumerate(self.weather_entries):
            to_return.append(f'entry number: {count+1}\n{entry.__str__()}\n{"="*60}')

        return '\n'.join(to_return)

    def read_obs_json(self, file):
        """
        Gets a python dict of the ob json file
        :param file: dict. Dictionary of all the obs, includes some meta info which will not be included in the return dict
        :return: dict. The OBs as a dict
        """
        with open(file) as file:
            obs_dict = json.load(file)

        obs_data = obs_dict['data']
        self.icao = obs_dict['meta']['icao']  # set the icao

        for ob_name in obs_data:  # ob = ob_name_#, type str
            ob = obs_data[ob_name]  # get the complete dict of the line of the OB
            tmp = WeatherEntry()
            # print(ob)  DEBUG
            tmp.set_info(ob)
            self.weather_entries.append(tmp)

        self.check_and_merge_all_duplicates()
        # self.sort_entries()  # sort the weather entries

    def read_taf_json(self, file):
        """
        Gets a python dict of the json file.
        :param file: string. The path and file name to the JSON version of the TAF
        :return: dict. The dictionary version of the TAF
        """

        with open(file) as file:
            taf_dict = json.load(file)

        taf_data = taf_dict['data']
        self.icao = taf_dict['meta']['icao']  # set the icao

        for taf_name in taf_data:  # ob = ob_name_#, type str
            taf = taf_data[taf_name]  # get the complete dict of the line of the TAF
            tmp = WeatherEntry()  # create an instance for the taf
            # print(taf)  DEBUG
            tmp.set_info(taf)  # populate the instance with info from the line
            self.weather_entries.append(tmp)

        self.check_and_merge_all_duplicates()
        self.sort_entries()  # sort the weather entries

        # set the taf expire time in self
        self.taf_expire_year = taf_dict['meta']['expire_year']
        self.taf_expire_month = taf_dict['meta']['expire_month']
        self.taf_expire_day = taf_dict['meta']['expire_day']
        self.taf_expire_time = taf_dict['meta']['expire_time']

    def add_entries_for_tempo_end(self):
        """
        Add a separate instance of WeatherEntry for when the Tempo ends.
        :return: nothing. It works on self.weather_entries
        """
        insert_offset = 0  # keep track of how many new items have been added
        for index, entry in enumerate(self.weather_entries[:]):  # [:] to make copy of the list, allowing us to edit to original
            if entry.is_tempo:
                new_entry = WeatherEntry()
                new_entry.year = entry.tempo_end_year
                new_entry.month = entry.tempo_end_month
                new_entry.day = entry.tempo_end_day
                new_entry.time = entry.tempo_end_time
                self.weather_entries.insert(index+1+insert_offset, new_entry)  # +1 to insert in the next one, +insert_offset b/c the list is changing
                insert_offset += 1

        self.sort_entries()  # There might be another ob before the tempo ends


    @staticmethod  # TODO should this go here or in WeatherEntrty() or in global?
    def are_weather_entries_same_time(*args):
        """
        Checks whether the two ore more instances of WeatherEntry have the same time.
        :param args: 2 or more instances of WeatherEntry()
        :return: boolean. True is they all have the same time. False otherwise.
        """
        if len(args) < 2:
            raise Exception('2 or more instances of WeatherEntry() are required to check if they are the same.')

        first = args[0]

        same_year = all([first.year == entry.year for entry in args])
        same_month = all([first.month == entry.month for entry in args])
        same_day = all([first.day == entry.day for entry in args])
        same_time = all([first.time == entry.time for entry in args])

        return same_year and same_month and same_day and same_time

    def sort_entries(self):
        """
        Sort all the entries in self.weather_entries
        :return: nothing.
        """
        self.weather_entries.sort(key=lambda entry: (entry.year, entry.month, entry.day, int(entry.time)))

    def check_and_merge_all_duplicates(self):
        """
        If there are 2 or more entries with the same time, this wil merge them. This will happen when a OB has the same time as
        a TAF line. This will make one weather entry have 2 or more components to it.
        :return: nothing. It processes self.weather_entries
        """
        # print(self.weather_entries[0])
        self.sort_entries()  # sort the entries so that when iterating over the loop we don't have to check ahead for dupes.

        new_weather_entries = []  # a list of the new entries with duplicates merged.

        index_current = 0  # start at 0 because I want to start at the beginning of the list. goes +1 every loop
        while index_current < len(self.weather_entries):  # while there are items in the list this will eval to True. When the list is empty it will evaluate to False
            # print(index_current)
            current_weather_entry = self.weather_entries[index_current]
            duplicates = [current_weather_entry]  # the list of duplicates that will need ot be merged, put the current entry in there and add other too it, reset every loop
            del_list_indicies = []  # the index of where duplicates were found, delete these at the end of every loop
            for index_next, next_weather_entry in enumerate(self.weather_entries[index_current+1:]):
                if self.are_weather_entries_same_time(current_weather_entry, next_weather_entry):  # if they are same time they will need to be merged
                    duplicates.append(next_weather_entry)
                    del_list_indicies.append(index_next+index_current)  # the next index will be to low we need to offset it by where it started from which is index_current

            if len(duplicates) > 1:  # 2 or more items means a duplicate was found and now needs to be merged.
                merged_entry = WeatherEntry.merge_duplicates(*duplicates)  # merge the duplicates, must be tuple be cause the function to merge takes as it's input *args which in python is a tuple
                new_weather_entries.append(merged_entry)  # append the merged weather_entry to the new list
                for index in del_list_indicies:  # delete the duplicate entries
                    del self.weather_entries[index]

            else:  # no duplicates were found just add the current entry to the list
                new_weather_entries.append(current_weather_entry)

            index_current += 1  # after processing the current index move on the next one.

        self.weather_entries = new_weather_entries  # replace the old list with the new list which has duplicates merged

        self.sort_entries()

    def populate_empty_required(self):
        """
        Every instance of WeatherEntry should have something for the OB; most instances should have Predom TAF. Because OBs are +-3 hours
        from the TAF end points there won't have to a be a TAF line for those end time frames.
        :return: nothing. alters self.weather_entries
        """
        # taf end period.
        taf_end_period = {'time': self.taf_expire_time, 'day': self.taf_expire_day, 'month': self.taf_expire_month, 'year': self.taf_expire_year}

        num_entries = len(self.weather_entries)
        for index, entry in enumerate(self.weather_entries[:]):  # ignore the first element b/c there is no previous elements to copy from

            if not entry.is_ob:  # if the current entry is not an ob go back
                for previous_entry in self.weather_entries[index::-1][1:]:  # go back in entries
                    if previous_entry.is_ob:
                        entry.is_ob = True
                        entry.ob_wind_dir = previous_entry.ob_wind_dir
                        entry.ob_wind_spd = previous_entry.ob_wind_spd
                        entry.ob_wind_gust = previous_entry.ob_wind_gust
                        entry.ob_vis = previous_entry.ob_vis
                        entry.ob_wx = previous_entry.ob_wx
                        entry.ob_sky_con = previous_entry.ob_sky_con
                        entry.ob_temp = previous_entry.ob_temp
                        entry.ob_temp_dew = previous_entry.ob_temp_dew
                        entry.ob_alstg = previous_entry.ob_alstg
                        # if previous_entry.ob_is_pk_wnd:
                        #     entry.ob_is_pk_wnd = True
                        #     entry.ob_pk_wnd_dir = previous_entry.ob_pk_wnd_dir
                        #     entry.ob_pk_wnd_spd = previous_entry.ob_pk_wnd_spd
                        self.weather_entries[index] = entry
                        break
            if not entry.is_predom:  # if the current entry does not have a predom in it. OBs before the TAF is valid should not have TAF lines in them and they won't because it can only search before itself, nto after. And there will never be a taf line before b/c weather_entries is sorted when .json files are read
                # make sure the TAF has not already expired in which case don't copy it into the OB
                if which_date_came_first(taf_end_period, entry.get_entry_time()) == 2:  # if TAF not yet expired, add it
                    for previous_entry in self.weather_entries[index::-1][1:]:  # go back in entries
                        if previous_entry.is_predom:
                            entry.is_predom = True
                            entry.taf_predom_wind_dir = previous_entry.taf_predom_wind_dir
                            entry.taf_predom_wind_spd = previous_entry.taf_predom_wind_spd
                            entry.taf_predom_wind_gust = previous_entry.taf_predom_wind_gust
                            entry.taf_predom_vis = previous_entry.taf_predom_vis
                            entry.taf_predom_wx = previous_entry.taf_predom_wx
                            entry.taf_predom_sky_con = previous_entry.taf_predom_sky_con
                            entry.taf_predom_alstg = previous_entry.taf_predom_alstg
                            self.weather_entries[index] = entry
                            break
            if not entry.is_tempo:
                #  tempos are tricky because we can't just go back to the lat one. It has to be within the tempo time frame.
                if which_date_came_first(taf_end_period, entry.get_entry_time()) == 2:  # if the TAF has not yet expired
                    for previous_entry in self.weather_entries[index::-1][1:]:  # search for a tempo line backwards
                        if previous_entry.is_tempo:  # previous entry is a tempo entry
                            # make sure previous tempo entry is still valid
                            current_entry_after_tempo_start = which_date_came_first(entry.get_entry_time(), previous_entry.get_entry_time()) == 2
                            current_entry_before_tempo_end = which_date_came_first(entry.get_entry_time(), previous_entry.get_tempo_end()) == 1
                            is_entry_in_tempo_time_frame = current_entry_after_tempo_start and current_entry_before_tempo_end
                            if is_entry_in_tempo_time_frame:
                                entry.is_tempo = True
                                entry.tempo_end_year = previous_entry.tempo_end_year
                                entry.tempo_end_month = previous_entry.tempo_end_month
                                entry.tempo_end_day = previous_entry.tempo_end_day
                                entry.tempo_end_time = previous_entry.tempo_end_time
                                entry.taf_tempo_wind_dir = previous_entry.taf_tempo_wind_dir
                                entry.taf_tempo_wind_spd = previous_entry.taf_tempo_wind_spd
                                entry.taf_tempo_wind_gust = previous_entry.taf_tempo_wind_gust
                                entry.taf_tempo_vis = previous_entry.taf_tempo_vis
                                entry.taf_tempo_wx = previous_entry.taf_tempo_wx
                                entry.taf_tempo_sky_con = previous_entry.taf_tempo_sky_con
                                self.weather_entries[index] = entry
                                break

    def weather_entries_dict_version(self):
        """
        Does not change self.weather_entries to a dict!
        :return: dict. Get a self.weather entries in dict format
        """
        keys = [i for i in range(len(self.weather_entries))]
        return dict(zip(keys, [entry.get_dict_version() for entry in self.weather_entries]))

    def save_to_json(self, file_name='temp/allweather.json'):
        """
        Saves the current weather_entries as a json dict.
        :return:
        """
        save_dict = {}  # the dict that will be saved tot he file includes some more meta data
        save_dict['meta'] = {'icao': self.icao}
        save_dict['data'] = self.weather_entries_dict_version()

        with open(file_name, 'w') as outfile:
            json.dump(save_dict, outfile, indent=4)

    def get_x_axis(self, return_format='full_date'):
        """
        Get all the x-axis values for the plot. The return of this can and will be used to for plots. Time is the x-axis
        :param return_format: string. Has 3 options. full_date, ints, offset_date
        :return: list. A list of x-values.
        full_date -> ['201905131500', '201905131557', '201905131658']
        offset_date -> ['0', '059']  # doesn't really work well at all yet
        ints -> [0, 1, 3, 4, ...]
        """
        if return_format == 'full_date' or return_format == 'offset_date':
            first_entry_time = int(f'{self.weather_entries[0].year}{pad_number(self.weather_entries[0].month, 2)}{pad_number(self.weather_entries[0].day, 2)}{self.weather_entries[0].time}')
            for entry in self.weather_entries:
                entry_time = int(f'{entry.year}{pad_number(entry.month, 2)}{pad_number(entry.day, 2)}{entry.time}')
                if return_format == 'offset_date':
                    self.x_axis.append(entry_time - first_entry_time)
                else:  # is full date
                    self.x_axis.append(entry_time)
        elif return_format == 'ints':  # [0, 1, 3, 4, ...]
            self.x_axis = [i for i in range(len(self.weather_entries))]
        else:
            raise Exception(f"Invalid return_format specified for get_x_axis()\n{return_format}")

        # create the x list for pandas so plotly can use it.
        self.x_axis_pd = [pd.datetime.strptime(str(dt_int), "%Y%m%d%H%M") for dt_int in self.x_axis]

        # self.x_axis_pd = pd.to_datetime(x_axis, format='%Y%m%d%H%M').strftime('%Y-%d-%m %H:%M:%S')

    def get_all_y_axis_plot(self):
        """
        Gets all the wind speeds, for the ob, predom and tempo lines
        :return: dict. A dictionary of lists.
        """
        for index, entry in enumerate(self.weather_entries):

            # ob wind dir
            if entry.ob_wind_dir is None or entry.ob_wind_dir == "VRB":
                self.y_ob_wind_dir.append(None)
            else:
                self.y_ob_wind_dir.append(int(entry.ob_wind_dir))

            # taf wind dir
            if entry.taf_predom_wind_dir is None or entry.taf_predom_wind_dir == "VRB":  # not all entry have a taf part to them
                self.y_predom_wind_dir.append(None)
            else:
                self.y_predom_wind_dir.append(int(entry.taf_predom_wind_dir))  # convert to int and add

            # tempo wind dir
            if entry.taf_tempo_wind_dir is None or entry.taf_tempo_wind_dir == "VRB":
                self.y_tempo_wind_dir.append(None)
            else:
                self.y_tempo_wind_dir.append(int(entry.taf_tempo_wind_dir))

            # pk wnd dir
            if entry.ob_pk_wnd_dir is not None:  # if there is a pk_wnd_dir
                self.y_pk_wind_dir.append(int(entry.ob_pk_wnd_dir))

                # Also create a new x_list for pk wnd. pk wnd is plotted as dots not lines. Only plot the xs' for which there are ys'. This find those xs'
                self.x_axis_pk_wnd.append(self.x_axis[index])  # add the x value for which the is a pk wnd to a new list

            # ob wind speed
            if entry.ob_wind_spd is None:
                self.y_ob_wind_spd.append(None)
            else:
                self.y_ob_wind_spd.append(int(entry.ob_wind_spd))

            # ob wind gust speed
            if entry.ob_wind_gust is None:
                self.y_ob_wind_gust_spd.append(None)
            else:
                self.y_ob_wind_gust_spd.append(int(entry.ob_wind_gust))

            # predom wind speed
            if entry.taf_predom_wind_spd is None:
                self.y_predom_wind_spd.append(None)
            else:
                self.y_predom_wind_spd.append(int(entry.taf_predom_wind_spd))

            # predom wind gust speed
            if entry.taf_predom_wind_gust is None:
                self.y_predom_wind_gust_spd.append(None)
            else:
                self.y_predom_wind_gust_spd.append(int(entry.taf_predom_wind_gust))

            # tempo wind speed
            if entry.taf_tempo_wind_spd is None:
                self.y_tempo_wind_spd.append(None)
            else:
                self.y_tempo_wind_spd.append(int(entry.taf_tempo_wind_spd))

            # tempo wind gust speed
            if entry.taf_tempo_wind_gust is None:
                self.y_tempo_wind_gust_spd.append(None)
            else:
                self.y_tempo_wind_gust_spd.append(int(entry.taf_tempo_wind_gust))

            # pk wnd speed
            if entry.ob_pk_wnd_spd is not None:
                self.y_pk_wind_spd.append(entry.ob_pk_wnd_spd)

            # ob alstg
            self.y_ob_alstg.append(entry.ob_alstg)

        # create the pandas pk wnd list
        self.x_axis_pk_wnd_pd = [pd.datetime.strptime(str(dt_int), "%Y%m%d%H%M") for dt_int in self.x_axis_pk_wnd]

    @staticmethod
    def get_max_in_list(l):
        """
        Can't use max() because some items are None and not ints. max() Expects a list of only ints
        :param: l. list some list of ints and Nones. Might be all Nones
        :return: int. If list is only None it will return 10
        """
        current_max = 0
        for i in l:  # some i will be ints some will be None
            if isinstance(i, int):  # only need to check if is int
                if i > current_max:
                    current_max = i

        if current_max == 0:  # list must have been all None or not a single int over 0
            current_max = 10

        return current_max

    def get_max_wind_spd_all(self):
        """
        Get the max wind speed found between all the obs and taf lines.
        :return: int
        """
        return max(AllWeather.get_max_in_list(self.y_ob_wind_spd), AllWeather.get_max_in_list(self.y_ob_wind_gust_spd), AllWeather.get_max_in_list(self.y_predom_wind_spd), AllWeather.get_max_in_list(self.y_predom_wind_gust_spd), AllWeather.get_max_in_list(self.y_tempo_wind_spd), AllWeather.get_max_in_list(self.y_tempo_wind_gust_spd), AllWeather.get_max_in_list(self.y_pk_wind_spd))

    def plotly_wind_dir(self):

        # make 2 plots
        fig = make_subplots(rows=2, cols=1, subplot_titles=('Wind Direction', 'Wind Speed'))

        # colors
        ob_wind_col = '#488284'
        predom_wind_col = "#4882fd"
        tempo_wind_col = "#FF5F15"

        # wind dir plot
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_ob_wind_dir, mode='lines+markers', name='OB', hovertext='observation wind direction', hoverinfo='x+y+text', line=dict(color=ob_wind_col)), row=1, col=1)
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_predom_wind_dir, mode='lines+markers', name='PREDOM', hovertext='TAF predominant line wind direction', hoverinfo='x+y+text', line=dict(color=predom_wind_col)), row=1, col=1)
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_tempo_wind_dir, mode='lines+markers', name='TEMPO', hovertext='TEMPO line wind direction', hoverinfo='x+y+text', line=dict(color=tempo_wind_col)), row=1, col=1)
        fig.append_trace(go.Scatter(x=self.x_axis_pk_wnd_pd, y=self.y_pk_wind_dir, mode='markers', name='PK WIND', hovertext='peak wind direction', hoverinfo='x+y+text', line=dict(color=ob_wind_col)), row=1, col=1)
        # fig.update_layout(title='Wind Direction', row=1, col=1)
        fig.update_yaxes(tick0=0, dtick=10)
        fig.update_yaxes(range=[-2, 362], row=1, col=1)  # wind direction should always be plotted from 0 to 360

        # wind speed plot
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_ob_wind_spd, mode='lines+markers', hovertext='observation wind speed', hoverinfo='x+y+text', showlegend=False, line=dict(color=ob_wind_col)), row=2, col=1)  # ob
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_ob_wind_gust_spd, mode='lines+markers', hovertext='observations wind gust speed', hoverinfo='x+y+text', showlegend=False, line=dict(color=ob_wind_col, dash='dash')), row=2, col=1)  # ob gust
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_predom_wind_spd, mode='lines+markers', hovertext='TAF predominant line wind speed', hoverinfo='x+y+text', showlegend=False, line=dict(color=predom_wind_col)), row=2, col=1)  # predom predom-wind
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_predom_wind_gust_spd, mode='lines+markers', hovertext='TAF predominant line wind gust speed', hoverinfo='x+y+text', showlegend=False, line=dict(color=predom_wind_col, dash='dash')), row=2, col=1)  # predom wind gust
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_tempo_wind_spd, mode='lines+markers', hovertext='TAF TEMPO line wind speed', hoverinfo='x+y+text', showlegend=False, line=dict(color=tempo_wind_col, dash='dash', simplify=True)), row=2, col=1)  # tempo wind gust
        fig.append_trace(go.Scatter(x=self.x_axis_pd, y=self.y_tempo_wind_gust_spd, mode='lines+markers', hovertext='TAF TEMPO line wind gust speed', hoverinfo='x+y+text', showlegend=False, line=dict(color=tempo_wind_col, dash='dash', simplify=True)), row=2, col=1)  # tempo wind gust
        fig.append_trace(go.Scatter(x=self.x_axis_pk_wnd_pd, y=self.y_pk_wind_spd, mode='markers', hovertext='peak wind speed', hoverinfo='x+y+text', showlegend=False, line=dict(color=ob_wind_col)), row=2, col=1)  # pk wnd
        # fig.update_layout(title='Wind Speed', row=1, col=1)
        fig.update_yaxes(tick0=0, dtick=5, row=2, col=1)
        fig.update_yaxes(range=[-1, self.get_max_wind_spd_all()+5], row=2, col=1)  # plus 5 to go a little above the max value
        fig.show()


if __name__ == '__main__':
    from get_taf_and_ob import get_ob_and_taf_json
    paths = get_ob_and_taf_json('KRAP')
    ob_path = paths['ob_path']
    # ob_path = 'temp/OBs_KCTB_sy2019_sm9_sd24_st0908__ey2019_em9_ed25_et1356.json'
    taf_path = paths['taf_path']
    # taf_path = 'temp/TAF_KCTB_isy2019_ism9_isd24_ist1127.json'
    a = AllWeather()
    a.read_taf_json(taf_path)
    a.read_obs_json(ob_path)
    a.add_entries_for_tempo_end()
    a.populate_empty_required()
    a.get_x_axis(return_format='full_date')
    # a.get_x_axis(return_format='full_date')
    a.get_all_y_axis_plot()
    a.plotly_wind_dir()
    # print(a.x_axis_pk_wnd)
    print(a.y_tempo_wind_gust_spd)
    print(a.y_predom_wind_gust_spd)
    # print(a.y_pk_wind_dir)
