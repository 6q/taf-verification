test_csv = """ 
valid,value,skyc1,skyc2,skyc3,skyc4
2004-07-21 09:00:00,200,M,,,OVC
2004-07-21 10:00:00,200,,,,
2004-07-21 11:00:00,150,BKN,,,
2004-07-21 12:00:00,140,SCT,,,
2004-07-21 13:00:00,130,SCT,,,
2004-07-21 13:00:00,130,SCT,,,OVC
2004-07-21 13:00:00,130,SCT,,,
2004-07-21 13:00:00,130,SCT,,,
2004-07-21 13:00:00,130,SCT,,,
2004-07-21 13:00:00,130,M,,,
2004-07-21 13:00:00,130,SCT,,,
2004-07-21 14:00:00,080,SCT,,,
2004-07-21 15:00:00,VRB,SCT,,,
2004-07-21 16:00:00,M,,,,
2004-07-21 17:00:00,M,SCT,,,
2004-07-21 17:01:00,VRB,SCT,,,
2004-07-21 19:00:00,VRB,SCT,,,
2004-07-21 20:00:00,280,SCT,,,
2004-07-21 21:00:00,020,SCT,,,
2004-07-21 22:00:00,VRB,SCT,,,
2004-07-21 23:00:00,330,SCT,,,
2004-07-22 00:00:00,VRB,SKC,,,SKC
2004-07-22 01:59:00,VRB,SCT,,,
2004-07-22 02:00:00,VRB,SCT,,,
2004-07-22 03:00:00,VRB,SCT,,,
"""

import pandas as pd
pd.set_option('display.width', 1400)
pd.set_option('display.max_columns', 500)
import numpy as np
from io import StringIO

test_csv = StringIO(test_csv)
df = pd.read_csv(test_csv)

df['valid'] = pd.to_datetime(df['valid'])  # convert 'valid' column to pd.datetime objects
df = df.set_index('valid')  # set the 'valid' as the index of the df

# df.drop_duplicates(subset='valid', keep='last', inplace=True)
df = df.loc[~df.index.duplicated(keep='last')]


def cloud_amount_to_int(*args):
    """
    A helper function for
    Converts cloud amount to an int and saves that int in a seperate columns
    SKC/CLR       --> 0
    FEW           --> 1
    SCT           --> 2
    BKN           --> 3
    OVC           --> 4
    anything else --> np.nan
    :param args: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :return: dataframe. A pandas dataframe with 4 new columns for the number equivalent of the cloud amount
    """
    # col_1, col_2 = tuple_of_colors[0], tuple_of_colors[1]

    cloud_amount_codes = []  # stores the color codes that will be returned

    for cloud_amount in args:
        if cloud_amount == 'CLR':
            cloud_amount_codes.append(0)
        elif cloud_amount == 'FEW':
            cloud_amount_codes.append(1)
        elif cloud_amount == 'SCT':
            cloud_amount_codes.append(2)
        elif cloud_amount == 'BKN':
            cloud_amount_codes.append(3)
        elif cloud_amount == 'OVC':
            cloud_amount_codes.append(4)
        elif cloud_amount == 'VV ':  # NOTE VV is encoded as "VV " in the downloaded csv
            cloud_amount.append(5)
        elif cloud_amount == 'M':  # this elif is DEBUG
            cloud_amount_codes.append(np.nan)  # DEBUG
        else:  # NOT DEBUG
            cloud_amount_codes.append(np.nan)  # NOT DEBUG
            # print(cloud_amount)  # DEBUG
            # raise Exception('The cloud amount is', cloud_amount)  # DEBUG

    return tuple(cloud_amount_codes)


def specify_cloud_amount_coded(df):
    """
    :param df: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :return: dataframe. A pandas dataframe with the cloud amount coded into 4 new columns
    """
    cloud_amount_to_int_vectorized = np.vectorize(cloud_amount_to_int)  # vectorize the function

    try:  # DEBUG
        df['skyc1_code'], df['skyc2_code'], df['skyc3_code'], df['skyc4_code'] = cloud_amount_to_int_vectorized(df['skyc1'], df['skyc2'], df['skyc3'], df['skyc4'])  # apply the vectorized function to the df
    # except ValueError:  # DEBUG
        # print(df['skyc1'], df['skyc2'], df['skyc3'], df['skyc4'])
        # raise ValueError
    except FileNotFoundError:
        pass

    return df


def specify_cloud_amount_coded(df):
    """
    :param df: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :return: dataframe. A pandas dataframe with the cloud amount coded into 4 new columns
    """
    replacement_dict = {
        'VV ': 5,
        'OVC': 4,
        'BKN': 3,
        'SCT': 2,
        'FEW': 1,
        'CLR': 0,
        'SKC': 0,
    }

    # df['skyc1_code'] = df['skyc1'].replace(to_replace=replacement_dict, inplace=False)
    # df['skyc2_code'] = df['skyc2'].replace(to_replace=replacement_dict, inplace=False)
    # df['skyc3_code'] = df['skyc3'].replace(to_replace=replacement_dict, inplace=False)
    # df['skyc4_code'] = df['skyc4'].replace(to_replace=replacement_dict, inplace=False)
    # df['skyc1_code'] = df['skyc1'].replace(to_replace='OVC', inplace=False)  # "OVC" --> 4
    df['skyc1_code'] = df['skyc1'].map(replacement_dict)
    df['skyc2_code'] = df['skyc2'].map(replacement_dict)
    df['skyc3_code'] = df['skyc3'].map(replacement_dict)
    df['skyc4_code'] = df['skyc4'].map(replacement_dict)

    return df

df = specify_cloud_amount_coded(df=df)
print(df)
print(df['skyc1'].dtypes)
print(df['value'].dtypes)














# column_index_valid_column = df.columns.get_loc('valid')  # the column number at which the valid column is. THIS WILL NOT WORK WITH THE VALID COLUMN SET AS INDEX






# for row_index in range(len(df)):
#     # print the index, a pandas datetime object
#     # date_time_this_row = df.iloc[row_index].name
#     date_time_this_row = df.index[row_index]
#     print(date_time_this_row, type(date_time_this_row))
#     pass

# print(df)


