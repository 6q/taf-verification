# from other_functions.time_based_functions import get_current_time
import pandas as pd
import pandas.errors
import os
import shutil
from other_functions.misc import ping, boolean_to_up_or_down, pad_number, convert_temp
from get_obs import obs_from_iastate, download_adds_obs_as_xml, get_pk_wnd_from_ob
import xml.etree.ElementTree as etree
from list_icaos import icaos  # import the ICAOs list
from pathlib import Path
from other_functions.common_metar_taf_elements import get_weather, extract_precip
import numpy as np
from other_functions.time_based_functions import find_row_closest_to_time
import re


def update_obs_csv(csv_file_path, icao=None):
    """
    As time goes on new obs come out the obs database (just a csv file) needs ot be updated with these new obs.
    Goes to the internet and downloads the new obs form iastate.

    Structure:
        make sure adds and iasate are up
        then check that the file exists
        then check and see if it is empty or contains data (at least the keys and on ob)
            if empty
                download it from iastate
        create a new cop of the csv file
        then delete the last week of the obs from the copied csv file
        re-download that last week via iastate
        download the missing parts from ADDS

    :param csv_file_path: string. The path to the csv file.
    :param icao: string. If None then this function will try to find the ICAO in the csv_file argument.
    :return:
    """
    # get icao from path if icao not specified as argument
    if icao is None:  # if None we have to find the icao in the path parameter
        icao = find_icao_in_path(csv_file_path)
    else:
        pass  # icao was specified by user and we don't need to find it

    obs_save_dir = f'../obs/{icao}'  # where the obs should be saved to by default. Goes up on dir into the root project dir and then goes into the obs dir and then the dir for just that icao

    # see if the servers are up, both ADDS and iastate must be up to proceed
    is_iastate_up = ping("mesonet.agron.iastate.edu")
    is_adds_up = ping("aviationweather.gov")
    if not is_adds_up or not is_iastate_up:
        print(f'IASTATE servers are {boolean_to_up_or_down(is_iastate_up)}.\n'
              f'ADDS servers are {boolean_to_up_or_down(is_adds_up)}.')
        return None  # stop running this function if one of the data servers is down
    else:
        print('ADDS and IASTATE server are both up. Proceeding.')

    # check that the file exists
    have_to_download_csv_from_scratch = True  # True for now. If no exceptions are hit it will change to False in the else block
    try:
        csv_df = pd.read_csv(csv_file_path)  # will check if the file exists and is not empty
        csv_df['station'].iat[0]  # just something that verifies there is at least on ob in the csv
    except FileNotFoundError:  # the file does not even exists
        print(f'The file {csv_file_path} does not exist.')
    except pandas.errors.EmptyDataError:  # the file is completely empty, no keys and not a single row
        print(f'The CSV file {csv_file_path} is empty. (But it does exist)')
    except IndexError:  # there is not a single ob in the .csv
        print('The CSV file is missing obs but it has the keys')
    except KeyError:  # If we get key error that means there is no first line of keys, we should download the file again
        print('The CSV file is missing the keys. CSV will be downloaded from scratch.')
    else:
        print(f'The CSV file {csv_file_path} exists. And has at least one OB in it. It is good to use.')
        have_to_download_csv_from_scratch = False
        csv_df = set_valid_date_time_as_index(df=csv_df)  # the csv file exists and has one ob in it, Set the valid column as the dataframes index.

    if have_to_download_csv_from_scratch:
        print(f'The .csv file {csv_file_path} will have to be downloaded from scratch')
    # return None
    # now we can download the obs from IASTATE, this will download all or the partial
    if have_to_download_csv_from_scratch:  # csv is blank get all obs from IASTATE and then from ADDS
        print(f'Downloading the obs for {icao} from scratch. This may take a minute.')
        file_name = f'{icao}.csv'  # the name of the about to be downloaded csv file (only filename, not dir)
        # path = f'../obs/{icao}'  # where the about to be downloaded csv file will be saved (just dir, not filename) # was used in the below two lines in place of obs_save_dir
        obs_from_iastate(icao=icao, download_all=True, file_name=file_name, path=obs_save_dir, print_url=True)
        csv_df = pd.read_csv(obs_save_dir + '/' + file_name).fillna('M')  # converts the file name and path into a unified string
        # csv_df = interpolate_missing_data(df=csv_df)  # interpolate missing data
        # csv_df = specify_weather_components(df=csv_df)  # decode the weather components into the csv
        csv_df = process_obs_df(df=csv_df)
        csv_df_new = csv_df

    # now that we have some csv file we can back it up
    backup_csv_path = backup_file(csv_file_path)  # backup the original csv file and do all operation on the bakcup until the very end
    print(f'Back-up created\n  original {csv_file_path}\n  back-up  {backup_csv_path}')  # let the user know the file was backed-up

    if not have_to_download_csv_from_scratch:  # for when download partial. This block determines which parts of the csv to delete to make room for the partial
        # set_valid_date_time_as_index()

        # first we have to remove the recent obs from the df
        print(f'Because there are obs in {csv_file_path} only the remaining obs will be downloaded (instead of all obs since they first were sent out.)')
        cutoff_index = find_cutoff_point_recentness_index(df=csv_df, how_many_days_back=7, use_current_time=False) + 1  # +1 b/c when we slice [:x], the df will stop 1 before x and not at x, we want to stop at x
        if cutoff_index != -1:  # If there is a cutoff index. For there to be cutoff index there must be obs more recent than the date that is how_many_days_back
            print(f'Older obs at and after index: {cutoff_index} (after {get_datetime_from_row_index(csv_df, cutoff_index)}) will be removed. This ensures that if there have been CORs obs that they are included in the updated .csv\n'
                  f'The last kept ob will be {cutoff_index-1} with a time of {get_datetime_from_row_index(csv_df, cutoff_index-1)}')
            csv_df_new = csv_df[:cutoff_index]  # create a copy of the df in memory but remove the newer obs
        else:  # cutoff_index is -1 which means that no past obs will be deleted # won't trigger unless find_cutoff_point_recentness_index is called with use_current_time=True and the obs in the csv are older than the input how_many_days_back days'.
            print('No past obs from the.csv file will be deleted because the OBs in the .csv file are already older than 7 days. Thus it is assumed they are not missing any CORs')
            csv_df_new = csv_df  # the new csv_df is just the same as the old one

    csv_df_new.to_csv(backup_csv_path, index=True)  # save the a  copy of the df to the backup file. Even if we have_to_download_csv_from_scratch we still need a backup for when we download the ADDS stuff.
    # return None

    if not have_to_download_csv_from_scratch:  # only need to download partial IASTATE if we didn't just download it all from scratch. If we downloaded all form scratch just now then it will already be as new as it can be
        # determine from which date forward to download the obs. Ex if the last ob in the csv_df is on Jun 2nd then we start downloading form June 3rd
        start_date_time = get_datetime_from_row_index(df=csv_df, row_index=cutoff_index, return_as_dt=True)
        start_year = start_date_time.year
        start_month = start_date_time.month
        start_day = start_date_time.day
        print(f'The start date and time for the observation download from IASTATE is {start_year}-{start_month}-{start_day}.')
        # obs_from_iastate(icao, start_date_specified={'year': start_year, 'month': start_month, 'day': start_day, file_name=f'{icao}.csv.part.temp'})  # download the missing obs
        partial_csv_path = obs_from_iastate(icao=icao, start_date_specified={'year': start_year, 'month': start_month, 'day': start_day}, file_name=f'{icao}.csv.part.temp', path=obs_save_dir, print_url=True)  # download the missing obs from IASTATE
        if partial_csv_path:  # if some string was returned. The string is the path of the newly created csv file
            print(f'Downloaded the obs from IASTATE successfully and saved to {partial_csv_path}')
            csv_df_new_part = pd.read_csv(partial_csv_path).fillna('M')  # read the partial IASTATE csv into memory
            print('Decoding weather components for partial IASTATE downloaded obs.')
            csv_df_new_part = process_obs_df(df=csv_df_new_part)
            print('Combining the existing csv file with the recently downloaded missing obs.')

            # csv_df_new = pd.concat([csv_df_new, csv_df_new_part], ignore_index=True, sort=False)  # merge the already df from the csv on file with the df from iastate
            csv_df_new = pd.concat([csv_df_new, csv_df_new_part], sort=False)  # merge the already df from the csv on file with the df from iastate
            csv_df_new.to_csv(backup_csv_path, index=True)  # update the backup (new) csv file too # THIS line is not required for the program to work. Really only need to save the news csv at the very end.
            print(f'  Successfully combined the two csv. \nThe file {backup_csv_path} has been updated to reflect this change.')

    # now download the rest from adds
    # csv_df_new.to_csv('/home/julius/Documents/projects/taf-verification/test_copy.csv')  # DEBUG
    last_ob_time_new_csv = get_last_ob_time_2(df=csv_df_new, return_as_dt=True)  # first determine the last ob in the new csv.

    adds_date_time_start = last_ob_time_new_csv + pd.DateOffset(minutes=1)  # Get the obs 1 minute after the last ob. 1 minute because that is the next possible ob that won't be a duplicate.
    adds_start_time_new_csv_year = adds_date_time_start.year
    adds_start_time_new_csv_month = adds_date_time_start.month
    adds_start_time_new_csv_day = adds_date_time_start.day
    adds_start_time_new_csv_hour = adds_date_time_start.hour
    # print('just hour', adds_start_time_new_csv_hour)
    adds_start_time_new_csv_minute = adds_date_time_start.minute
    # print('just minute', adds_start_time_new_csv_minute)
    adds_start_time_new_csv_formatted_time = f'{pad_number(adds_start_time_new_csv_hour, 2)}{pad_number(adds_start_time_new_csv_minute, 2)}'
    # print('formatted time ==============', adds_start_time_new_csv_formatted_time)
    adds_adds_date_time_start_formatted = {'time': adds_start_time_new_csv_formatted_time, 'day': adds_start_time_new_csv_day, 'month': adds_start_time_new_csv_month, 'year': adds_start_time_new_csv_year}

    print(f'The last ob in the new csv has a date time of:\n  {last_ob_time_new_csv}\nDownloading obs from ADDS that have a valid time at or after\n  {adds_date_time_start}')
    adds_xml_root = download_adds_obs_as_xml(icao=icao, time_frame_start=adds_adds_date_time_start_formatted, del_file_after_getting_xml_root=False, path=obs_save_dir, debug_print=True)  # get the xml root
    print('Converting ADDS .xml to a pandas dataframe.')
    xml_df = xml_to_csv(xml_root=adds_xml_root, empty_cloud_amount='M')  # convert the xml to a pandas df with the same naming convention as the IASTATE csv.
    if xml_df is None:  # there were no obs in the xml so there is no real df for the adds xml.
        print('ADDS xml did not contain any new OBs. It will not be added.')
        if have_to_download_csv_from_scratch:  # this belongs here b/c after download obs from scratch we still want to check if there are obs from ADDS that can be added to the file. Up to this point the full obs IASTATE download has not been saved b/c we still needed to check ADDS.
            print(f'Saving the from scratch downloaded IASTATE obs that did not have any ADDS obs missing to file {csv_file_path}')  # let the user know they downloaded the obs from scratch and did not miss any obs from ADDS
            csv_df.to_csv(csv_file_path, index=True)  # save the df to the csv file
    else:  # there are new obs in the xml
        print('Decoding weather components for ADDS download downloaded obs.')
        xml_df = process_obs_df(df=xml_df)
        print('Combining the IASTATE dataframe with the ADDS dataframe.')
        # csv_df_new = pd.concat([csv_df_new, xml_df], ignore_index=True, sort=False)  # add the ADDS df to the IASTATE df.
        csv_df_new = pd.concat([csv_df_new, xml_df], sort=False)  # add the ADDS df to the IASTATE df.
        print(f'Saving final dataframe to csv {csv_file_path}')
        csv_df_new.to_csv(csv_file_path, index=True)
    print(f'deleting temporary file\n{backup_csv_path}')
    # os.remove(backup_csv_path)  # delete the temporary csv file
    print(f'Finished updating OBs for {icao}.')
    return None


def xml_to_csv(xml_root=None, path=None, empty_cloud_amount='   ', what_to_return_if_not_found='M'):
    """
    Converts a root xml  or xml file into a csv df that can be saved to disk.
    :param xml_root: xml root object. What is returned when an xml is read into the memory by the xml parser
    :param path: string. The path to the xml file
    :param empty_cloud_amount. string. What to represent cloud amount by if ther were not clouds. The IASTATE CSV uses '   '  # TODO maybe when reading in the IASTATE csv we should change it to None?
    :param what_to_return_if_not_found: string. What should the value of a OB attribute be if it is not in the xml. For Example with the XML if there is no clouds then this var will be assigned to the clouds in the dataframe.
    :return: dataframe. A pandas data frame of the observations. Returns None if there are not obs in the xml file.
    """
    if xml_root is None:  # the user provided an xml file. Get the root from the file.
        tree = etree.parse(path)
        xml_root = tree.getroot()

    df = pd.DataFrame()  # set up the empty pandas df to which we will later add obs
    num_obs_in_xml = int(xml_root[6].attrib.get("num_results"))  # how many obs there are total in the xml

    if num_obs_in_xml == 0:  # There are no obs in the xml file. No need to go any further
        return None


    icao = find_attribute_of_xml_row(xml_root=xml_root, row_index=0, attribute='station_id', what_to_return_if_not_found=what_to_return_if_not_found)  # get the ICAO, no need to do this in the loop as they all have the same ICAO
    rows = []  # pandas doesn't like when we add a row one at a time via the .append method. So generate all the rows first and store them in this var. Then add the rows all at once to the pandas df
    for i in range(num_obs_in_xml-1, -1, -1):  # ADDS from newest to oldest, we want oldest to newest.
        metar = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='raw_text', what_to_return_if_not_found=what_to_return_if_not_found)  # Get the RAW metars. Ex. METAR KJFK 011500Z 32003KT 4SM FU 28/16 A//// RMK SLP159 T02830156
        # if not metar:  # if the above function call returned None then there was not metar ('raw_text') attribute of the OB.
        #     metar = 'M'  # if no METAR then asign metar the missing_value

        valid = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='observation_time', what_to_return_if_not_found=what_to_return_if_not_found)  # ob time
        valid = pd.to_datetime(valid).strftime('%Y-%m-%d %H:%M')  # format the adds date time to match the IASTATE one

        tmpf = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='temp_c', what_to_return_if_not_found=what_to_return_if_not_found)  # temperature in F
        if tmpf != what_to_return_if_not_found:  # we found an actual temp. we can convert to F
            tmpf = convert_temp(tmpf, current_unit='c')


        dwpf = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='dewpoint_c', what_to_return_if_not_found=what_to_return_if_not_found)  # dew point in F
        if dwpf != what_to_return_if_not_found:  # we found an actual temp. we can convert to F
            dwpf = convert_temp(dwpf, current_unit='c')

        drct = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='wind_dir_degrees', what_to_return_if_not_found=what_to_return_if_not_found)  # wind direction in degrees
        sknt = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='wind_speed_kt', what_to_return_if_not_found=what_to_return_if_not_found)  # wind speed in KT
        # wind gust
        try:
            gust = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='wind_gust_kt', what_to_return_if_not_found=what_to_return_if_not_found)
        except (AttributeError, TypeError):
            gust = 'M'

        alti = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='altim_in_hg', what_to_return_if_not_found=what_to_return_if_not_found)  # alimeter setting (ALSTG)
        mslp = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='sea_level_pressure_mb', what_to_return_if_not_found=what_to_return_if_not_found)  # sea level pressure (SLP)

        vsby = find_attribute_of_xml_row(xml_root=xml_root, row_index=i, attribute='visibility_statute_mi', what_to_return_if_not_found=what_to_return_if_not_found)   # visibility in SM

        # sky condition. this one is more tricky because the xml uses the same label for all. So first we get the layers as lists of [amount, height]. For example [['BKN', '6500'], ['BKN', '25000']]
        try:
            sky_condition_tmp = []  # a temporary dictionary for storing all the cloud layers of a single ob
            for sky_con_count, sky_con in enumerate(xml_root[6][i].findall("sky_condition")):
                sky_condition_tmp.append([sky_con.attrib['sky_cover'], sky_con.attrib['cloud_base_ft_agl']])
        except (KeyError, IndexError):  # Index Error will be triggered when there are no cloud layers
            sky_condition_tmp.append(['CLR', what_to_return_if_not_found])
            # print('Error when getting cloud layers from XML obs.')


        # convert the list of cloud layers into variables for the csv. The csv stores 4 clouds layers per row regardless of if there were clouds. The xml only includes cloud if there were clouds.
        number_of_cloud_layers = len(sky_condition_tmp)
        if number_of_cloud_layers >= 1:
            skyc1 = sky_condition_tmp[0][0]  # cloud amount
            skyl1 = sky_condition_tmp[0][1]  # cloud height
        else:
            skyc1 = empty_cloud_amount
            skyl1 = what_to_return_if_not_found

        if number_of_cloud_layers >= 2:
            skyc2 = sky_condition_tmp[1][0]  # cloud amount
            skyl2 = sky_condition_tmp[1][1]  # cloud height
        else:
            skyc2 = empty_cloud_amount
            skyl2 = what_to_return_if_not_found

        if number_of_cloud_layers >= 3:
            skyc3 = sky_condition_tmp[2][0]  # cloud amount
            skyl3 = sky_condition_tmp[2][1]  # cloud height
        else:
            skyc3 = empty_cloud_amount
            skyl3 = what_to_return_if_not_found

        if number_of_cloud_layers >= 4:
            skyc4 = sky_condition_tmp[3][0]  # cloud amount
            skyl4 = sky_condition_tmp[3][1]  # cloud height
        else:
            skyc4 = empty_cloud_amount
            skyl4 = what_to_return_if_not_found

        # print(f'cloud layer #1 {skyc1}{skyl1} cloud layer #2 {skyc2}{skyl2} cloud layer #3 {skyc3}{skyl3} cloud layer #4 {skyc4}{skyl4} ')  # DEBUG

        # PK WND
        pk_wnd = get_pk_wnd_from_ob(metar, valid, return_date_time_format='dt')
        if pk_wnd is None:  # there is no PK WND in the ob -> encode 'M' for missing
            peak_wind_time = what_to_return_if_not_found
            peak_wind_drct = what_to_return_if_not_found
            peak_wind_gust = what_to_return_if_not_found
        else:  # there is a PK WND
            peak_wind_time = pk_wnd['date_time']
            peak_wind_drct = pk_wnd['dir']
            peak_wind_gust = pk_wnd['speed']

        # new_row = {'station': icao[1:], 'valid': valid, 'tmpf': tmpf, 'dwpf': dwpf, 'drct': drct, 'sknt': sknt, 'alti': alti, 'mslp': mslp, 'vsby': vsby, 'gust': gust, 'skyc1': skyc1, 'skyl1': skyl1, 'skyc2': skyc2, 'skyl2': skyl2, 'skyc3': skyc3, 'skyl3': skyl3, 'skyc4': skyc4, 'skyl4': skyl4, 'peak_wind_gust': peak_wind_gust, 'pk_wind_drct': peak_wind_drct, 'peak_wind_time': peak_wind_time, 'metar': metar}  # the row to be added to the df
        rows.append({'station': icao[1:], 'valid': valid, 'tmpf': tmpf, 'dwpf': dwpf, 'drct': drct, 'sknt': sknt, 'alti': alti, 'mslp': mslp, 'vsby': vsby, 'gust': gust, 'skyc1': skyc1, 'skyl1': skyl1, 'skyc2': skyc2, 'skyl2': skyl2, 'skyc3': skyc3, 'skyl3': skyl3, 'skyc4': skyc4, 'skyl4': skyl4, 'peak_wind_gust': peak_wind_gust, 'peak_wind_drct': peak_wind_drct, 'peak_wind_time': peak_wind_time, 'metar': metar})
        # df = df.append(new_row, ignore_index=True, sort=False)  # add the row to the df. Pandas does not like it when we use .append it's slower and less efficient

    # more efficient way of getting the dataframe of rows. better than the .append method
    df = pd.concat((pd.DataFrame([row.values()], columns=row.keys()) for row in rows), ignore_index=True, sort=False)  # Shirayuki # Created the df version of the xml the way that pandas likes is (without .append)
    return df


def find_attribute_of_xml_row(xml_root=None, row_index=None, xml_row=None, attribute='station', what_to_return_if_not_found='M'):
    """
    A helper function. Given a row (or index) of the xml file find and returns the user requested attrib. Takes as input either the xml root or the xml row.
    :param xml_root: the root of the xml file
    :param row_index: int. The index of the row of the xml root from which to get the information
    :param xml_row: A row of the xml root
    :param attribute: string. An attribute of an xml row. Ex. "observation_time" or "visibility_statute_mi"
    :param what_to_return_if_not_found: string. What should the value of a OB attribute be if it is not in the xml. For Example with the XML if there is no clouds then this var will be assigned to the clouds in the dataframe.
    :return: string (I think). The value of attribute
    """
    # first we need to get the xml row if it was not specified
    if xml_root:  # a xml root was specified, get the row matching the index
        xml_row = xml_root[6][row_index]
    else:  # do nothing we already got the xml row
        pass

    # try and return the requested value
    try:
        return xml_row.find(attribute).text  # return the value of the attribute
    except AttributeError:  # the ADDS ob doesn't have this attribute
        return what_to_return_if_not_found


def find_icao_in_path(path):
    """
    '/home/julius/Documents/projects/taf-verification/obs/KSFO/KSFO.csv.temp' -> 'KSFO'
    '/home/julius/Documents/projects/taf-verification/obs/KSFO/KSFO.csv' -> 'KSFO'
    Assumes the ICAO is the first four char of the filename!
    :param path: string. The path to the .csv file
    :return: string. The icao
    """
    return path.split('/')[-1].split('.')[0]  # splits up the different directories and uses the last dir (which is the file name). Then splits up file name by . (dots and gets the first bit.


def find_cutoff_point_recentness_index(df, how_many_days_back=7, use_current_time=False):
    """
    This function takes as input a dataframe containing a csv file. It will find which ob in that df is right before
    how_many_days_back. The ob it will find will be slightly more in the past than how_many_days_ago. It returns the index
    of that ob.
    NOTE: If the return value from this function will be used to slice or drop the more recent obs you need to +1 this returned value b/c the slice will stop one short of the index not at the index
    :param df: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :param how_many_days_back: int. How many days back into the past from now to go and find an ob
    :param use_current_time: boolean. Do you want to find point a week ago form today or a week ago from the last ob in the csv?
    :return: int: The index of the ob that is how_many_days_back
    """
    if use_current_time:  # a week (how_many_days_back) ago from today
        start_point_time = pd.datetime.utcnow()
    else:  # use that last time of the ob, # a week (how_many_days_back) ago from the last ob
        start_point_time = get_last_ob_time_2(df=df, return_as_dt=True)

    past_time = start_point_time - pd.DateOffset(days=how_many_days_back)  # find the cutoff point time, (not index)
    # go back in the csv and find the ob that was how_many_days_back. By default finds the ob 7 days in the past.
    found_ob_how_many_days_back = False  # the csv may not have any recent obs in it.
    for row_index in range(len(df) - 1, max((len(df) - 11000), 0), -1):  # 11000 is worst case scenario with an OB/SPECI every minute.
        row = df.iloc[row_index]
        try:  # will work if the valid column is not set as the df index
            row_dt = pd.to_datetime(row['valid'])
        except KeyError:  # if the df does have the valid column set as index
            row_dt = row.name  # if the 'valid' column is set as the index than it can retrieved by the name property (weird, I know)

        if row_dt < past_time:  # we found the ob that was about a week ago
            found_ob_how_many_days_back = True
            break

    if found_ob_how_many_days_back:
        # now that we found the ob that roughly a week ago we need to delete all the obs for that day. IASTATE only allows to download based on day not hour or minute
        day_cutoff_initial = get_datetime_from_row_index(df, row_index, return_as_dt=True).day
        # print(f'The initial cutoff day is {day_cutoff_initial}')
        for row_index_end_prev_day in range(row_index, row_index-1500, -1):  # the high number just in case there are a ton of SPECIs
            row_day = get_datetime_from_row_index(df, row_index_end_prev_day).day
            if day_cutoff_initial != row_day:
                break  # the last ob of the previous day has been found
        # print(f'total iterations of the for loop {count} and the last index is {last_ob_index}, the length of the df is {len(csv_df)}')  DEBUG

        return row_index_end_prev_day  # the cutoff index was found
    return -1  # if all the obs in the csv are older than the how_many_days_back threshold then there is no cutoff index.


def backup_file(file, extension='.temp', path_only=False):
    """
    Backsup a .file file be renaming it to *.extension
    :param file: string. The path to and the name of the file
    :param extension: string. Must include the . (dot) if you want it in the file name. The extenion of the back-up version
    :param path_only. boolean. If True, it won't actually back up the file. It will just return where it would back up to. If False it will back up the file and returns the location.
    :return: string. The input string plus the extension
    """
    assert len(extension) > 0, 'The extension cannot be an empty string "".'  # if there is no extension there will be not backup
    back_up_path = file + extension
    if not path_only:  # back up the file
        shutil.copy2(file, back_up_path)  # saves the file in the same dir with the added extension (keep old extension too though)
    return back_up_path


def get_last_ob_time(file=None, df=None, return_as_dt=True):
    """
    A helper function that gets the last time of the last ob from either the csv file or the dataframe containing the csv file
    :param file: Path to a csv file
    :return: dict. The time of the last ob in the csv file
    """
    if file:  # try to read from csv file
        index_time = get_keys_from_csv_file(file).index('valid')  # find the index of the valid time
        with open(file) as file:
            for line in file:
                pass
            last_line = line.split(',')[index_time]  # 1 is the index in the csv for the time and date

        del file  # free up memory
        if return_as_dt:  # TODO clean up this mess
            return last_line
        else:
            return pd.to_datetime(last_line)
    else:  # must be df
        if return_as_dt:
            return pd.to_datetime(df['valid'].iat[-1])
        else:  # returns as str
            return df['valid'].iat[-1]


def get_last_ob_time_2(file=None, df=None, return_as_dt=True):
    """
    A helper function that gets the last time of the last ob from either the csv file or the  df (dataframe) containing the csv file. If a df is passed this function handles both dfs that have the `valid` column loaded as index and those
    that do not.
    :param file: Path to a csv file
    :return: datetime. A pandas datetime object.
    """
    if file is not None:  # the user wants to get the last ob time from a csv file
        df = pd.read_csv(file)  # create the df based on the file

    try:  # using 'valid' as a key assume that the dataframe does NOT have the 'valid' column loaded as index into the dataframe.
        # last_row_date_time = last_row_date_time['valid']
        last_row_date_time = df['valid'].iat[-1]  # the value for the 'valid' column of the last row
    except KeyError:  # if the df does have the valid column set as the index
        last_row_date_time = df.index[-1]  # the last index of the df

    last_row_date_time = pd.to_datetime(last_row_date_time)  # convert to pd.datetime

    return last_row_date_time


def specify_cloud_amount_coded(df):
    """
    :param df: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :return: dataframe. A pandas dataframe with the cloud amount coded into 4 new columns
    """
    # cloud_amount_to_int_vectorized = np.vectorize(cloud_amount_to_int)  # vectorize the function  # Now longer needed as .map is now used

    replacement_dict = {
        'VV ': 5,
        'OVC': 4,
        'BKN': 3,
        'SCT': 2,
        'FEW': 1,
        'CLR': 0,
        'SKC': 0,
    }

    df['skyc1_code'] = df['skyc1'].map(replacement_dict)
    df['skyc2_code'] = df['skyc2'].map(replacement_dict)
    df['skyc3_code'] = df['skyc3'].map(replacement_dict)
    df['skyc4_code'] = df['skyc4'].map(replacement_dict)

    return df


def cloud_amount_to_int(*args):
    """
    A helper function for
    Converts cloud amount to an int and saves that int in a seperate columns
    SKC/CLR       --> 0
    FEW           --> 1
    SCT           --> 2
    BKN           --> 3
    OVC           --> 4
    anything else --> np.nan
    :param args: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :return: dataframe. A pandas dataframe with 4 new columns for the number equivalent of the cloud amount
    """
    # col_1, col_2 = tuple_of_colors[0], tuple_of_colors[1]

    cloud_amount_codes = []  # stores the color codes that will be returned

    for cloud_amount in args:
        if cloud_amount == 'CLR':
            cloud_amount_codes.append(0)
        elif cloud_amount == 'FEW':
            cloud_amount_codes.append(1)
        elif cloud_amount == 'SCT':
            cloud_amount_codes.append(2)
        elif cloud_amount == 'BKN':
            cloud_amount_codes.append(3)
        elif cloud_amount == 'OVC' or cloud_amount == 'VV ':  # VV (vertical visibility) is just overcast at the surface, which is why it gets code 4 as awell. NOTE VV is encoded as "VV " in the downloaded csv
            cloud_amount_codes.append(4)
        elif cloud_amount == 'M':  # this elif is DEBUG
            cloud_amount_codes.append(np.nan)  # DEBUG
        else:  # NOT DEBUG
            cloud_amount_codes.append(np.nan)  # NOT DEBUG
            print(cloud_amount)  # DEBUG
            raise Exception('The cloud amount is', cloud_amount)  # DEBUG

    return tuple(cloud_amount_codes)


def get_keys_from_csv_file(file_name):
    """
    Gets the keys (first line) of a .csv file
    :param file_name: string. The path to the .csv file
    :return: list. The list of keys as strings in a list.
    """
    with open(file_name) as file:
        keys = file.readline().strip().split(',')  # read the first line and split up by the comma. removes the \n at the end of the line
    del file  # free up memory

    return keys  # looks like ['station', 'valid', 'tmpf', 'dwpf']


def set_valid_date_time_as_index(df):
    """
    Takes as input a pandas df that has a .csv of ob loaded into it and set the 'valid' column to the index. Sets the 'valid' to a column of a pd.datetime objects before setting it as the index.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :return: dataframe. The pandas dataframe with the 'valid' column set as the index.
    """
    df['valid'] = pd.to_datetime(df['valid'])  # convert 'valid' column to pd.datetime objects
    df = df.set_index('valid')  # set the 'valid' as index
    # df.set_index('valid')  # not using this method because it won't work properly when saving the df to csv with index='valid'

    return df


def fix_wind_and_gust_speed(df):
    """
    The predominant wind speed can not be greater than the gust speed. For example "17025G150KT" makes no sense. Obs that have these both wind speed should be deleted.
    Also removes negative wind speeds. They turn into np.NaN
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :return: dataframe. The df without obs that have predominant wind speed greater than gust speed.
    """
    wind_speeds = ['sknt', 'gust']
    # for c in wind_speeds:
    #     print(df[c])
    df[wind_speeds] = df[wind_speeds].where(df[wind_speeds] > 0, np.NaN, inplace=False)  # set predom and gust wind speed to np.NaN if they are negative.

    df = df[~(df['gust'] < df['sknt'])]  # remove bad obs

    return df


def specify_weather_components(path=None, df=None, save_df_to_file=False):
    """
    Without this function the only place to see weather in the csv is in wxcodes (which often is not encoded right) or in the raw metar. This function reads the raw METAR and add 8 more columns for the weather components.
    :param path: string. The path to a csv file.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :param check_if_already_processed_and_skip: boolean. If True this will only specify_weather_components for the lines that do not have it specified. If False is will redo it for all
    :param save_df_to_file. boolean. If True then the df will be saved to the same file. If False it will not be saved
    :return:
    """
    if df is None:  # if df was None and a path was specified, go read the df from file
        df = pd.read_csv(path)

    get_weather_vec = np.vectorize(get_weather)  # do some numpy optimization
    # create and populate the weather columns
    df['wx_intensity'], df['vicinity_or_not'], df['description'], df['precipitation'], df['obscuration'], df['other'], df['rotation'], df['has_liquid_precip'], df['has_solid_precip'] = get_weather_vec(df['metar'], return_type='tuple')
    # df['wx_intensity'], df['vicinity_or_not'], df['description'], df['precipitation'], df['obscuration'], df['other'], df['rotation'], df['precip_liquid_or_solid'], df['is_processed'] = None, None, None, None, None, None, None, None, None

    if save_df_to_file:  # save to disk
        df.to_csv(path)
        # print('debug')

    return df


def get_wind_dir_diff(dir_1: int, dir_2: int) -> int:
    """
    Figures out the shortest way to get from dir_1 to dir_2.
    Positive number go clockwise, negative numbers go counter clockwise.

    .. note::

        The direction 0 and 360 are the same.

    :param dir_1: Direction of the first wind.
    :param dir_2: Direction of the second wind.
    :return: What to add to :code:`dir_1` to get :code:`dir_2`.
    """
    diff = dir_2 - dir_1
    if diff > 180:
        diff -= 360
    elif diff < -180:
        diff += 360
    return diff


get_wind_dir_diff_vec = np.vectorize(get_wind_dir_diff)  # vecotrize the function so it will be faster when applying to a pd dataframe. DON'T make a new column in the df for this, this is a helper function


def remove_rows_with_blank_metar(df):
    """
        Removes every row from the obs dataframe that is not an actual ob. A row is an actual ob when it contains a METAR or SPECI. All METARs or SPECIs always include the ICAO
        in the ob string. The assumption is made that if the row's "metar" value (of type str in this case) contain the ICAO, then the row is an actual ob and it will be kept.
        Rows that have blank "metar" values in the csv will have a value of np.NaN in the pandas dataframe, which this function checks for first.
        :param df: dataframe. A pandas dataframe that has the csv file of obs read into it.
        :return: df. A pandas dataframe with ob-less rows removed.
        """
    df = df.reset_index()  # reset the index for now because valid time is irrelevant here

    df = df[df["metar"].str.contains(df["station"][0]) == True]
    return set_valid_date_time_as_index(df=df)


def fix_wind_dir(df, column_name='drct', new_column_name_suffix=''):
    """
    This function adds a seconds column for wind direction. It interpolates missing wind direction intelligently. Ex 340 M 020 -> 340 360 020
    :param df: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :param column_name: String. Valid inputs: the name of the wind direction column. Default is used 'drct' because that is what is in the
    csv file. For the merged dfs use either 'drtc_a' or 'drct_b'
    :param new_column_name_suffix: string. What to add to the end of the new column name. Used for merged df. Ex '_a' or '_b'
    :return: df. The pandas dataframe with the obs read into it.
    """
    row_index = 0

    new_column_name = f'new_drct{new_column_name_suffix}'

    column_index_drct = df.columns.get_loc(column_name)  # the column number from left to right at which drct is. Int.
    df[new_column_name] = np.nan  # create a new column for the new wind direction and initialzie it with np.nan
    column_index_new_drct = df.columns.get_loc(new_column_name)  # the column number from left to right at which the new drct is. Int.
    # column_index_valid_column = df.columns.get_loc('valid')  # the index of the valid

    while row_index <= len(df) - 1:  # start at row 0. We have to do -1 to stop at the last row and not read an index that dosn't exists

        skip_this_ob = True  # default value. True when the ob has a real wind dir Ex. 230, 0, 360, etc.
        try:
            start_point_wind_dir = int(float(df.iat[row_index, column_index_drct]))  # both the int and float are required for this function to work
        except ValueError:  # converting 'VRB' or np.nan to int causes a ValueError
            skip_this_ob = False  # when ob has np.nan or 'VRB' or 'M' for the wind dir

        if skip_this_ob is False:  # if the row has VRB as the wind direction

            # default values are True, will be False if there are no obs that have a specified wind direction either before or after
            # this will be most common if there is a VRB ob close ot either the start or end of the df
            found_start_point_wind_dir = True
            found_end_point_wind_dir = True

            # start_point_date_time = pd.to_datetime(df.iat[row_index, column_index_valid_column])  # start point of this chunk that has VRB wind. A chunk is consecutive obs that all have VRB, a chunk can be a little as just 1 ob (one row)
            start_point_date_time = df.index[row_index]  # start point of this chunk that has VRB wind. A chunk is consecutive obs that all have VRB, a chunk can be a little as just 1 ob (one row)

            # find the previous row that has wind direction
            for prev_index in range(row_index-1, -1, -1):  # for the previous obs
                try:
                    start_point_wind_dir = int(float(df.iat[prev_index, column_index_drct]))
                    # start_point_date_time = pd.to_datetime(df.iat[prev_index, column_index_valid_column])
                    start_point_date_time = df.index[prev_index]
                    break  # if we got here then the prev row has a good wind dir
                except ValueError:  # converting 'VRB' or np.nan to int causes a ValueError
                    pass  # skip that ob b/c it doesn't have wind direction, go onto the next one
            else:  # there is not a single ob in front left, just go with 180
                # start_point_wind_dir = 180
                found_start_point_wind_dir = False

            # find the next row that has a valid wind direction
            chunk_size = 1  # how many consecutive obs don't have a good wind dir, we will fix one chunk at a time
            bad_indexes = [row_index]  # a list of all the rows that need to have their wind dir fixed.
            for next_index in range(1, len(df) - row_index):  # for the rest of the df
                try:  # wind the next obs that has an actual wind direction
                    end_point_wind_dir = int(float(df.iat[row_index + next_index, column_index_drct]))
                    # end_point_date_time = pd.to_datetime(df.at[row_index + next_index, column_index_valid_column])
                    end_point_date_time = df.index[row_index + next_index]
                    break  # if we got here then the next row has a good wind dir
                except ValueError:  # converting 'VRB' or np.nan to int causes a ValueError
                    chunk_size += 1  # another bad ob, the chunk grows by one
                    bad_indexes.append(row_index + next_index)  # found another row that doesn't have a valid wind dir, add it to the list of indexes that will be fixed
                    pass  # skip that ob b/c it doesn't have wind direction, go onto the next one
            else:  # if we are at the end of the df and all the last obs are VRB we will want to fill it with some number, lets just go with whatever the wind dir was before the VRB
                # end_point_wind_dir = start_point_wind_dir  # TODO remove if the found stuff works
                found_end_point_wind_dir = False
                # end_point_date_time = pd.to_datetime(df.iat[len(df)-1, column_index_valid_column])  # use the last ob's date_time for the end point if no next_ob has a valid wind dir  # TODO maybe go 1 hour after last ob time?
                end_point_date_time = df.index[len(df)-1]  # use the last ob's date_time for the end point if no next_ob has a valid wind dir  # TODO maybe go 1 hour after last ob time?

            # handle the edge case if no good replacements for the VRB were found due to a lack of obs before or after that had specific wind directions
            if not found_start_point_wind_dir and not found_end_point_wind_dir:  # didn't find a good start or end point
                start_point_wind_dir = end_point_wind_dir = 180  # if neither a good start nor end end point were found just go with 180
            elif not found_start_point_wind_dir:  # implied is also that end point IS found b/c if it was also not then the above would have triggered
                start_point_wind_dir = end_point_wind_dir
            elif not found_end_point_wind_dir:  # only the end point was found, assume the start point is the same as end point
                end_point_wind_dir = start_point_wind_dir

            # now fix the VRB winds
            chunk_length_seconds = (end_point_date_time - start_point_date_time)/np.timedelta64(1, 's')  # the length of the chunk in seconds
            if chunk_length_seconds == 0:  # DEBUG
                print('end_point_date_time', end_point_date_time)
                print('start point date time', start_point_date_time)

            wind_dir_change_amount = get_wind_dir_diff_vec(start_point_wind_dir, end_point_wind_dir)  # The difference of the start and end wind dir
            for next_index_new in range(1, chunk_size+1):  # now set the VRB wind directions
                # this_time_difference = (pd.to_datetime(df.iat[row_index+next_index_new-1, column_index_valid_column]) - start_point_date_time) / np.timedelta64(1, 's')  # get the time difference in seconds from the most previous working ob to this ob
                this_time_difference = ((df.index[row_index+next_index_new-1]) - start_point_date_time) / np.timedelta64(1, 's')  # get the time difference in seconds from the most previous working ob to this ob
                try:  # DEBUG. Until I figure out what is causing this error, this stays here.
                    time_difference_ratio = this_time_difference / chunk_length_seconds
                except ZeroDivisionError:  # DEBUG
                    print('this time difference', this_time_difference)
                    print('chunk length seconds', chunk_length_seconds)
                    raise ZeroDivisionError  # DEBUG
                new_wind_dir = add_or_subtract_wind_dir(start_point_wind_dir, (wind_dir_change_amount * time_difference_ratio))  # set the wind dir

                new_wind_dir = int(round(new_wind_dir, -1))  # round ot the nearest 10s, Ex 316 -> 320

                if new_wind_dir == 0:  # to be consistent with obs we should convert 0 to 360
                    new_wind_dir = 360
                df.iat[row_index+next_index_new-1, column_index_new_drct] = new_wind_dir
                # df.iat[row_index, column_index_val2] = 55

            # DEBUG
            # print(f'start_point_wind_dir {start_point_wind_dir}'
            #       f'\nstart_point_date_time {start_point_date_time}'
            #       f'\nend_point_wind_dir {end_point_wind_dir}'
            #       f'\nend_point_date_time {end_point_date_time}'
            #       f'\nchunk size {chunk_size}'
            #       f'\ntime difference {chunk_length_seconds}'
            #       f'\nwind dir difference {wind_dir_change_amount}')
            #
            # print('= ' * 40)

            row_index += chunk_size  # move to the next wind dir that might be broken

        else:  # the ob is good, go to the next one
            row_index += 1

    df[new_column_name] = df[new_column_name].astype('Int16')  # convert column from float to int16

    return df


def add_or_subtract_wind_dir(wind_dir, change_amount):
    """
    Get a new wind direction based on changing the input wind direction. To subtract change amount should be negative.
    :param wind_dir: int. Between 0 and 360, inclusive
    :param change_amount: int. Between -180 and 180, inclusive
    :return: int. Between 0 and 360, inclusive
    """
    wind_dir += change_amount

    if wind_dir > 360:
        wind_dir -= 360
    elif wind_dir < 0:
        wind_dir += 360

    return wind_dir


def remove_duplicate_rows(df):
    """
    Removes duplicate rows from the df. When obs are COR-ed the old obs still remain, which causes there to be several rows with the same time. There can be only one OB per every instance of time.
    This function keeps the last ob, although it is unknown whether or note that one is the latest version.
    :param df: dataframe. A pandas dataframe with a csv file of obs loaded into it.
    :return: df. A pandas dataframe.
    """
    df = df.loc[~df.index.duplicated(keep='last')]  # remove the duplicate rows, keep the last
    return df


def interpolate_missing_data(path=None, df=None, save_df_to_file=False):
    """
    Interpolates missing data. Interpolates temperature, dew point, wind direction, wind speed, alstg, cloud amount, cloud height, visibility
    :param df: dataframe. A pandas dataframe with a csv file of obs loaded into it.
    :param path: string. The path to a csv file.
    :param save_df_to_file. boolean. If True then the df will be saved to the same file. If False it will not be saved
    :return: dataframe. A pandas dataframe.
    """

    features_to_interpolate = ['tmpf', 'dwpf', 'alti', 'vsby', 'sknt']

    if df is None:  # if df was None and a path was specified, go read the df from file
        df = pd.read_csv(path)

    for feature in features_to_interpolate:
        # print(feature)  # DEBUG
        df[feature] = df[feature].interpolate(method='time', limit_direction='both')
    # df['tmpf'] = df['tmpf'].interpolate(method='time')  # interpolate missing data

    if save_df_to_file:  # save to disk
        df.to_csv(path)

    return df


def convert_M_to_nan(df):
    """
    When a value is missing in an observation, it is denoted as the string 'M'. M is for missing. This function will replace all the 'M's with numpy.nan (not-a-number). This is useful when later interpolating data.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :return: dataframe. The dataframe with np.nan in place of the 'M' strings
    """

    features_to_convert = ['tmpf', 'dwpf', 'alti', 'vsby', 'sknt', 'drct']  # a list of all the features that will need the M replaced with np.nan

    for feature in features_to_convert:
        df[feature] = df[feature].replace('M', np.nan)  # replace the 'M' with np.nan

    return df


def process_obs_df(df, verbose=True):
    """
    Takes as input the raw obs just loaded into the pandas df. First it sets the valid column as the index. Next it interpolates missing mandatory data (ex. temp, alstg, etc.). Next it decodes the weather. Next it decodes the cloud amount
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :param verbose: boolean. If True then messages will be printed
    :return: dataframe. A pandas dataframe that has been processed
    """

    if verbose:
        print('  Starting to process obs of the df of length', len(df))

    df = set_valid_date_time_as_index(df=df)  # set the valid column as index of the df

    df = remove_duplicate_rows(df=df)  # remove duplicate rows that have the same timestamps

    df = remove_rows_with_blank_metar(df=df)  # remove the obs that don't contain an actual metar

    df = convert_M_to_nan(df=df)  # convert all the 'M's to np.nan

    df = specify_cloud_amount_coded(df=df)  # OVC --> 4, FEW --> 1, etc.

    df = convert_columns_to_numeric(df=df)  # convert columns to numeric so that they can be interpolated later

    df = fix_wind_and_gust_speed(df=df)  # remove obs with predom > gust, negative sknt & gust become np.NaN

    df = interpolate_missing_data(df=df)  # interpolate the missing mandatory information

    df = specify_weather_components(df=df)  # decode the weather into more columns

    df = fix_wind_dir(df=df)  # interpolate VRB  and M wind

    df = remove_unused_columns(df=df)  # remove columns that won't be used like relh or 1hr ice accretion

    df = specify_hourly_precip(df=df)  # extracts hourly precip and converts to inches

    if verbose:
        print('  Done processing obs')

    return df

extract_precip_vec = np.vectorize(extract_precip)

def specify_hourly_precip(df):
    """
    This function extracts the hourly precip from METAR string and puts it into it's own column.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :return: dataframe. A pandas dataframe that has a column for hourly precip.
    """
    df['hr_precip'] = extract_precip_vec(df['metar'])
    return df



def convert_columns_to_numeric(df):
    """
    This function take columns and convert them to numeric values. This function is required to be applied to a df before interpolating data.
    NOTE: requires that `skyc#_code` and 'skyl#' columns exists.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :return: dataframe. A pandas dataframe that has columns converted to numeric.
    """

    #the following 3 list are names of columns we would like to process in some way.
    to_float = ['tmpf', 'dwpf', 'alti', 'vsby', 'sknt']
    to_int = ['skyl1', 'skyl2', 'skyl3', 'skyl4', 'skyc1_code', 'skyc2_code', 'skyc3_code', 'skyc4_code', 'drct', 'gust', 'peak_wind_gust', 'peak_wind_drct']
    to_positive = to_float[2:] + to_int  # all the columns should have positive numbers except for temperature and dew point.

    df[to_float] = pd.to_numeric(to_float, downcast='float', errors='coerce')
    df[to_int] = pd.to_numeric(to_int, downcast='unsigned', errors='coerce')
    df[to_positive] = df[to_positive].where(df[to_positive] > 0, np.NaN, inplace=False)  # convert invalid negative numbers to np.NaN

    return df


def remove_unused_columns(df):
    """
    Remove the columns of the df that will not be used, this saves disk space and makes combining df later faster.
    :param df: dataframe. A pandas dataframe with a csv file of obs loaded into it.
    :return: dataframe. A pandas dataframe with the unsed columns removed.
    """
    columns_to_remove = [
        'relh',
        'p01i',
        'mslp',
        'ice_accretion_1hr',
        'ice_accretion_3hr',
        'ice_accretion_6hr'
    ]

    for column in columns_to_remove:
        try:
            del df[column]
        except KeyError:  # for exampple the obs form ADDS do not have 'relh' so we can just skip that one
            pass

    # df.drop(columns=columns_to_remove, inplace=True)

    return df


def get_datetime_from_row_index(df, row_index, return_as_dt=True):
    """
    Finds the valid time of the row in a dataframe. THe valid time is the time of the observation.
    :param row_index: int. The index
    :param return_as_dt: boolean. If True returns the datetime as a pandas datetime object. If False returns as string.
    :return: pandas datetime object or string. The valid time of the observation
    """
    try:  # this work will if the 'valid' column is not loaded as the index into the df.  # TODO only accept df that have the valid column as index.
        dt = df['valid'].iat[row_index]
    except KeyError:  # if the df has the 'valid' column loaded as the index
        dt = df.iloc[row_index].name
    if return_as_dt:
        return pd.to_datetime(dt)  # will be dt
    return dt  # will be string


def update_csv_database(icao_list, obs_dir='.././obs'):
    """
    Updates a set of weather csv files. Use this function to keep all csv for all the airports up to date.
    :param icao_list: list. A list of strings. The string in the list are the 4 letter ICAOs. Ex. ['KSFO', 'KTUL', 'KBKV', 'KSEA', 'KORD', 'KDFW']
    :param obs_dir: String. The folder that will contain all the subfolder for the ICAOs
    :return: nothing yet
    """

    for icao in icao_list:  # update every ICAOs csv file
        # path = obs_dir + '/' + icao + '.csv'  # generate the full path based on the obs dir and icao
        path = f'{obs_dir}/{icao}/{icao}.csv'

        # convert the relative path to an absolute one
        path = Path(path)  # create Path object
        path = path.resolve()  # make path absolute
        path = str(path)  # instead of an path object get a path string

        update_obs_csv(path, icao=icao)  # update the csv (or create it if it does not exists already)
        print('=' * 80)

    print('Done updating all .csv files,')


def get_time_based_slice_from_df(csv_file=None, df=None, start_date_time=None, end_date_time=None, num_hours=None, num_seconds=None):
    """
    There are 3 ways this function can be used.
        1. Provide start_date_time and num_hours. The function will return the slice of the df starting at the specified start date and time and end at that time plus num_hours.
        2. Provide end_date_time and num_hours. The function will return the slice of the df ending at the specified end date and time and starting at that time minus num hours.
        3. Provide start_date_time and end_date_time. The function will return the slice of the df starting and ending at the specified inputs.
    NOTE: For "last" to work as an arguement for end_date_time the df must have the valid column set as its index.
    NOTE: The start and end time do not have to be exact matches to the times in the df. The closest row times will be used.
    :param csv_file: string. The path to the csv file.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :param start_date_time: datetime object. An instance of pd.datetime the start time frame of the df slice for the obs
    :param end_date_time: datetime object or None or "now" or "last. An instance of pd.datetime the end time frame of the df slice for the obs.
    If "now" then the end_date_time will be set to the current run time. If "last" the end date time will be the time of the last ob in the csv or df.
    :param num_hours: float or int. The length of the slice in hours. If start time frame is given then the end time will be start
     date time plus the num_hours. If end date time is given the start will be the end minus num hours
    :param num_seconds: float or int. Same as num_hours but in seconds
    :return: dataframe. A pandas dataframe with the last n hour of obs in the df. The index of the df will be the valid time.
    """
    # read the csv file in a df if no df is given.
    if df is None:  # assume that user wants to read from a csv instead.
        df = pd.read_csv(csv_file)  # read the file into a pd df
        df = set_valid_date_time_as_index(df=df)  # set the valid time column as the index of the

    if end_date_time == "now":
        end_date_time = pd.datetime.utcnow()  # get the UTC/ZULU current time
    elif end_date_time == "last":  # use last ob as the end date time
        end_date_time = df.index[-1]

    if start_date_time and end_date_time:  # both start and end time are given
        pass  # no need to do anything both start and end time are given, the next thing to do is get the rows

    elif start_date_time:  # start date time was given, determine the end time based on start time and num hours or num seconds
        if num_hours:  # num hours is given
            end_date_time = start_date_time + pd.Timedelta(hours=num_hours)
        elif num_seconds:  # num seconds is given
            end_date_time = start_date_time + pd.Timedelta(seconds=num_seconds)

    elif end_date_time:  # end date time is given, determine the start time based on end time minus num hours or num seconds
        if num_hours:
            start_date_time = end_date_time - pd.Timedelta(hours=num_hours)
        elif num_seconds:  # num seconds is given
            start_date_time = end_date_time - pd.Timedelta(seconds=num_seconds)

    # DEBUG
    # print('slice start', start_date_time)
    # print('slice end', end_date_time)

    # now that the start and end time exists, find the row that most closely matches them
    row_start = find_row_closest_to_time(df=df, time_to_match=start_date_time)
    row_end = find_row_closest_to_time(df=df, time_to_match=end_date_time)

    return df[row_start:row_end]


if __name__ == '__main__':
    # path = '/home/harrison_fjord/Documents/ml_ai_ds_forecaster/obs/KBAD/KBAD.csv'
    # path = '/home/julius/Documents/projects/taf-verification/obs/KSFO/KSFO.csv'
    # path2 = '/home/julius/Documents/projects/taf-verification/obs/KSFO/KSFO2.csv'

    # df = pd.read_csv(path)
    # print(len(df))
    # df = remove_rows_with_blank_metar_2(df=df)
    # print(len(df))
    # df.to_csv(path2)

    # get_time_based_slice_from_df(csv_file=path, num_hours=30)
    # df = get_time_based_slice_from_df(csv_file=path, num_seconds=86400, start_date_time=pd.to_datetime('2019-12-21 21:51:20'))
    # pd.set_option
    # print(len(df))
    # print(df)
    update_csv_database(icaos)
