import re
import time
from other_functions.misc import pad_number, replace_chars_in_str_over_range, is_prob_line
from other_functions.month_days import get_number_of_days_in_month, is_leap_year


def move_time(adjust_amount, initial_time=None):
    """
    Adjust the input time by the adjust_amount. Add or subtract time.
    :param initial_time: dict ex. {'time': '1505', 'day': 4, 'month': 8, 'year': 2019} The current time in zulu and the full date
    :param adjust_amount: dict {'time_hours': 0, 'time_minutes': -30, 'day': 0, 'month': 0, 'year': 0} in this example 30 minutes is subtracted.
    Assumes the input will never use a smaller unit of time input than neccasary ie. Do not enter 40days enter 1 month and how every many days are left over
    Do not enter a time like 0070 enter  0160 instead. Another ex. dont add 14 months, add 1 yr and 2 months.
    All values should have the same sign. Dont't add two days but subtracxt 1 hour, Add 1d and 23h. If not undersired effect may occur.
    :return: dict. A dict of the adjusted time. Ex. {'time': new_time, 'day': new_day, 'month': new_month, 'year': new_year}
    """
    if initial_time is None:
        initial_time = get_current_time()

    # current time
    new_hour = int(initial_time['time'][:2])  # get the hours and convert form input_str to int
    new_minute = int(initial_time['time'][2:])  # get the minutes and convert form input_str to int
    new_day = int(initial_time['day'])
    new_month = int(initial_time['month'])
    new_year = int(initial_time['year'])

    # adjust amount
    adjust_minute = int(adjust_amount['time_minutes'])  # get the hours that are supposed to be added or subtracted
    adjust_hour = int(adjust_amount['time_hours'])  # get the hours that are supposed to be added or subtracted
    adjust_day = int(adjust_amount['day'])
    adjust_month = int(adjust_amount['month'])
    adjust_year = int(adjust_amount['year'])

    # add/subtract the minutes
    new_minute += adjust_minute
    if new_minute < 0:  # so many minutes were subtracted we are now into last hour
        new_minute += 60  # for example new minutes is -20, add 60, going to 40 and go back to last hour
        new_hour -= 1  # go back 1 hour
        if new_hour < 0:  # it was already the 0th hour, go back to yesterday at 23rd hour
            new_hour = 23  # 23rd hour
            new_day -= 1  # go back to yesterday
            if new_day < 1:  # It was the 1st, and 1 day was subtracted, go back to the last day of last month
                new_month -= 1  # go back to last month
                if new_month < 1:  # if it was January and we went back a month go back to december last year
                    new_month = 12  # December
                    new_year -= 1  # last year
                new_day = get_number_of_days_in_month(new_month, new_year)  # went back a day into the last day of the previous month

    elif new_minute >= 60:  # so many minutes were added we are now over into the next hour
        new_minute -= 60  # for example if the minute was 80 it should be 20 minutes the next hour
        new_hour += 1  # go to next hour
        if new_hour >= 24:  # the 24th hour should be the 0th hour of the next day
            new_hour = 0  # 0th hour
            new_day += 1  # next day
            if new_day > get_number_of_days_in_month(new_month, new_year):  # if the added brought it to more days there are in that particular month, go to day 1 next month
                new_day = 1  # day 1
                new_month += 1  # next month
                if new_month > 12:  # the next month is January next year
                    new_month = 1
                    new_year += 1
    else:  # minutes were added or subtracted but still in range of 0-59
        pass

    # add/subtract the hours
    new_hour += adjust_hour
    if new_hour < 0:  # Ex new hours is -4. Should be 21 previous day
        new_hour = 24 + new_hour  # plus because new_hour is already negative
        new_day -= 1
        if new_day < 1:  # It was the 1st, and 1 day was subtracted, go back to the last day of last month
            new_month -= 1  # go back to last month
            if new_month < 1:  # if it was January and we went back a month go back to december last year
                new_month = 12  # December
                new_year -= 1  # last year
            new_day = get_number_of_days_in_month(new_month, new_year)  # went back a day into the last day of the previous month
    elif new_hour >= 24:  # Ex. new hour is 29. should be 05 next day
        new_hour -= 24
        new_day += 1
        if new_day > get_number_of_days_in_month(new_month, new_year):  # if the added brought it to more days there are in that particular month, go to day 1 next month
            new_day = 1  # day 1
            new_month += 1  # next month
            if new_month > 12:  # the next month is January next year
                new_month = 1
                new_year += 1
    else:  # hour was added within range
        pass

    # add/subtract the days
    new_day += adjust_day
    if new_day < 1:  # so many days were subtracted it is now last month
        new_month -= 1  # go back to last month
        if new_month < 1:  # if it was January and we went back a month go back to december last year
            new_month = 12  # December
            new_year -= 1  # last year
        new_day = get_number_of_days_in_month(new_month, new_year) + new_day  # went back a day into the last day of the previous month  # plus because number already negative
    elif new_day > get_number_of_days_in_month(new_month, new_year):  # if the added brought it to more days there are in that particular month, go to day 1 next month
        new_day -= get_number_of_days_in_month(new_month, new_year)  # Ex. day is 35 (31 is mnax fo this month)go to 3th of next month
        new_month += 1  # next month
        if new_month > 12:  # the next month is January next year
            new_month = 1
            new_year += 1
    else:  # the day was added in the range
        pass

    # add/subtract the months
    new_month += adjust_month
    if new_month < 1:  # so many months were subtracted it is now negative
        new_month = 12 + new_month  # Ex new_month is -1, should be november last year  # plus because new month is already negative
        new_year -= 1  # last year
    elif new_month > 12:  # Ex new_month is 13, should be feburary next year
        new_month -= 12
        new_year += 1

    # add/subtract the years
    new_year += adjust_year

    # after the year was chagned make sure it wasn't the last day of leap year Feburary
    if new_month == 2:  # if it is Feburay check for leap year
        if is_leap_year(new_year):
            if new_day == 28:
                new_day = 29

    # For when the month was adjusted after the day. Example it was the last day of May (31days) but now is June (30days)
    if new_day > get_number_of_days_in_month(new_month, new_year):
        new_day = get_number_of_days_in_month(new_month, new_year)

    return {'time': f'{pad_number(new_hour, 2)}{pad_number(new_minute, 2)}',
            'day': new_day,
            'month': new_month,
            'year': new_year}


def get_current_time(time_struct=None):
    """
    Get the current zulutime and date.
    :param time_struct: time.struct_time.The result of running time.gmtime() needed to avoid time racing. Supposed to be supplied.
    :return: dict. The current time in this format: {'time': '1503', 'day': 2, 'month': 5, 'year': 2019}
    """
    if time_struct is None:
        time_struct = time.gmtime()

    return {
        'time': f'{pad_number(time_struct[3], 2)}{pad_number(time_struct[4], 2)}',
        'day': time_struct[2],
        'month': time_struct[1],
        'year': time_struct[0],
    }


def which_date_came_first(date_1, date_2):
    """
    Determines which date came first.
    :param date_1: dict. A dict of the adjusted time. Ex. {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
    :param date_2: dict. A dict of the adjusted time. Ex. {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
    :return: int. 1 if date_one came first, 2 if date_2 came first. None if they both are the same time and neither came first
    """
    time_1 = int(date_1['time'])
    day_1 = int(date_1['day'])
    month_1 = int(date_1['month'])
    year_1 = int(date_1['year'])

    time_2 = int(date_2['time'])
    day_2 = int(date_2['day'])
    month_2 = int(date_2['month'])
    year_2 = int(date_2['year'])

    if year_1 < year_2:
        return 1
    elif year_2 < year_1:
        return 2

    # both years are the same, check months
    if month_1 < month_2:
        return 1
    elif month_2 < month_1:
        return 2

    # both months are the same, check day
    if day_1 < day_2:
        return 1
    elif day_2 < day_1:
        return 2

    # both days are the same, check time
    if time_1 < time_2:
        return 1
    elif time_2 < time_1:
        return 2

    # neither date came first
    return None


def has_date_difference_been_found(small_date, big_date, adjust_time):
    """
    A helper function for calculate_date_differene(). Will be used in that function. As the elements of time
    (minutes, hours, ..., years) are gone over the difference is obtained between the two different dates for that element.
    If that differnce added to the smaller time is equal to the new time then the time difference has been found.
    Need because:
    date_1 = {'time': "2300", 'day': 31, 'month': 5, 'year': 2019}
    date_2 = {'time': "0100", 'day': 1, 'month': 6, 'year': 2019}
    is only a 2hr difference.

    Determines if small_date + adjust_time == big_date
    :param small_date: dict. A dict of the time. Ex. {'time': '1243', 'day': 15, 'month': 10, 'year': 2019}
    :param big_date: dict. A dict of the time. Ex. {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
    :param adjust_time: dict {'time_hours': 0, 'time_minutes': -30, 'day': 0, 'month': 0, 'year': 0}
    :return: boolean. True if the date difference has been found. False if not
    """
    return move_time(small_date, adjust_time) == big_date


def calculate_date_difference(date_1, date_2):
    """
    Calculates the time difference between two dates. Gets the absolute time difference, (not negative).
    :param date_1: dict. A dict of the adjusted time. Ex. {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
    :param date_2: dict. A dict of the adjusted time. Ex. {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
    :return: dict. The time difference between the two dates. {'time_hours': 24, 'time_minutes': 0, 'day': 0, 'month': 0, 'year': 0}
    """
    if date_1 == date_2:  # if there is no difference
        return {'hour': 0,
                'minute': 0,
                'day': 0,
                'month': 0,
                'year': 0}

    # first determine which date came first so we can subtract the smaller from the larger and avoid negative time differe
    if which_date_came_first(date_1, date_2) == 1:  # newer one - one that came first # smaller one is the one that came first
        small_time = date_1
        big_time = date_2
    else:
        big_time = date_1
        small_time = date_2

    small_time_hour = int(small_time['time'][:2])
    small_time_minute = int(small_time['time'][2:])
    small_time_day = int(small_time['day'])
    small_time_month = int(small_time['month'])
    small_time_year = int(small_time['year'])

    big_time_hour = int(big_time['time'][:2])
    big_time_minute = int(big_time['time'][2:])
    big_time_day = int(big_time['day'])
    big_time_month = int(big_time['month'])
    big_time_year = int(big_time['year'])

    original_month = big_time_month


    # Start with subtracting year end with minute
    # start with year
    new_year = big_time_year - small_time_year

    # subtract the month and adjust the year if need be
    new_month = big_time_month - small_time_month
    if new_month < 0:  # lowest month can be is 0 before a year needs to be subtracted Ex, -14 should be
        new_year -= 1
        new_month = 12 + new_month
    if new_month > 12:  # should never happen ex 12-0, still 12, 0-5 is negative
        raise Exception(f'Month is above 12 when getting new month.\n{new_month}')
    else:  # month was subtracted but is still in range
        pass

    # subtract they day and adjust month and year if necessary
    new_day = big_time_day - small_time_day
    if new_day < 0:  # lowest day can be is 0 before a month needs to be subtracted
        new_day = get_number_of_days_in_month(original_month, new_year) + new_day
        new_month -= 1
        if new_month < 0:  # lowest month can be is 0 before a year needs to be subtracted
            new_year -= 1
            new_month = 12 + new_month  # plus because new_month is already negative
        elif new_month > 12:  # should never happen ex 12-0, still 12, 0-5 is negative
            raise Exception(f'new_month is above 12 when getting new month.\n{new_month}')
        else:  # month was subtracted but is still in range
            pass
    elif new_day > get_number_of_days_in_month(original_month, new_year):  # should never trigger becausing subtracting it should keep it less
        raise Exception(
            f'new_day is larger than max days in the month: {get_number_of_days_in_month(original_month, new_year)}.\n'
            f'new_month: {new_month}, new_year: {new_year}')
    else:  # day is still within range
        pass

    # subtract the hours and adjust the days, months, and years if necessary
    new_hour = big_time_hour - small_time_hour
    if new_hour < 0:  # if the hour difference is less than 0 go back a day
        new_hour = 24 + new_hour  # plus because new_hour already negative
        new_day -= 1
        if new_day < 0:  # lowest day can be is 0 before a month needs to be subtracted
            new_day = get_number_of_days_in_month(original_month, new_year) + new_day
            new_month -= 1
            if new_month < 0:  # lowest month can be is 0 before a year needs to be subtracted
                new_year -= 1
                new_month = 12 + new_month  # plus because new_month is already negative
            elif new_month > 12:  # should never happen ex 12-0, still 12, 0-5 is negative
                raise Exception(f'new_month is above 12 when getting new month.\n{new_month}')
            else:  # month was subtracted but is still in range
                pass
        elif new_day > get_number_of_days_in_month(original_month,
                                                   new_year):  # should never trigger becausing subtracting it should keep it less
            raise Exception(
                f'new_day is larger than max days in the month: {get_number_of_days_in_month(original_month, new_year)}.\n'
                f'new_month: {new_month}, new_year: {new_year}')
        else:  # day is still within range
            pass
    elif new_hour >= 24:  # should never trigger
        raise Exception(f'new_hour is above or equal to 24.\n{new_hour}')
    else:   # hour was subtracted within range
        pass
    # subtract the minutes
    new_minute = big_time_minute - small_time_minute
    if new_minute < 0:  # is the difference in minute sis negative go back 1 hour.
        new_hour -= 1
        new_minute = 60 + new_minute  # plus because new_minute is already negative
        if new_hour < 0:  # if the minute is negative also go back 1 hr
            new_hour = 24 + new_hour  # plus because new_hour already negative
            new_day -= 1
            if new_day < 0:  # lowest day can be is 0 before a month needs to be subtracted
                new_day = get_number_of_days_in_month(original_month, new_year) + new_day
                new_month = 12 + new_month
                if new_month < 0:  # lowest month can be is 0 before a year needs to be subtracted
                    new_year -= 1
                    new_month = 12 + new_month  # plus because new_month is already negative
                elif new_month > 12:  # should never happen ex 12-0, still 12, 0-5 is negative
                    raise Exception(f'new_month is above 12 when getting new month.\n{original_month}')
                else:  # month was subtracted but is still in range
                    pass
            elif new_day > get_number_of_days_in_month(original_month,
                                                       new_year):  # should never trigger becausing subtracting it should keep it less
                raise Exception(
                    f'new_day is larger than max days in the month: {get_number_of_days_in_month(original_month, new_year)}.\n'
                    f'new_month: {original_month}, new_year: {new_year}')
            else:  # day is still within range
                pass
        elif new_hour >= 24:  # should never trigger
            raise Exception(f'new_hour is above or equal to 24.\n{new_hour}')
    elif new_minute >= 60:  # should never trigger ex 59-59 = 0, 12-40 = -28
        raise Exception(f'new_minute is above or equal to 60:\c{new_minute}')
    else:  # minutes was subtracted within range. Ex 23 - 5 = 18
        pass

    # to check the right thing is being returned see if the found time difference + small time == big time. Do this for all elements
    # check minutes
    if has_date_difference_been_found(small_time, big_time, {'time_hours': 0, 'time_minutes': new_minute, 'day': 0, 'month': 0, 'year': 0}):
        new_hour, new_day, new_month, new_year = 0, 0, 0, 0  # if adjusting only the minutes works set all the others ones to 0

    # check hours
    elif has_date_difference_been_found(small_time, big_time, {'time_hours': new_hour, 'time_minutes': new_minute, 'day': 0, 'month': 0, 'year': 0}):
        new_day, new_month, new_year = 0, 0, 0

    # check days
    elif has_date_difference_been_found(small_time, big_time, {'time_hours': new_hour, 'time_minutes': new_minute, 'day': new_day, 'month': 0, 'year': 0}):
        new_month, new_year = 0, 0

    # check months
    elif has_date_difference_been_found(small_time, big_time, {'time_hours': new_hour, 'time_minutes': new_minute, 'day': new_day, 'month': new_month, 'year': 0}):
        new_year = 0

    # no need to check years because there is nothing after years to set equal to zero

    return {'hour': new_hour,
            'minute': new_minute,
            'day': new_day,
            'month': new_month,
            'year': new_year}


def get_taf_time_length(taf, time_struct):
    """
    Get the legnth of the TAF in hours. Length is defines as the time difference bewtween the start and end time of the
    TAF. NOTE: TAF cannot be longer than 1 month
    :param time_struct: time.struct_time. The result of running time.gmtime(). Used to prevent time racing
    :param taf: list. The entire TAF as a list of strings. Every line is it's own string
    :return: int. The length of the TAF in hours. Ex. 30 for a 30hour long TAF.
    """
    start_time = get_taf_issue_or_start_time_and_date(taf[0], time_struct, 'start')
    end_time = get_taf_end_time_and_date(taf, time_struct)

    length = calculate_date_difference(end_time, start_time)  # get the length as a dict
    length_hours = length['hour'] + (24 * length['day'])  # some TAFs are longer than a day so it might show up as 1 day 6 hr, we want only hours
    return length_hours


def fix_taf_times(taf, month=None, year=None):
    """
    BECMG sometimes say 1223/1224. The 24th hour on the 12th does not exists. It should be the 00th hour on the 13th.
    Only TAFs with BECMG lines might need to be fixed. At most will have to fix 1 line in the entire TAF.
    :param year: int. The year the TAF was issued in.
    :param month: int. The month the TAF was issued in
    :param taf: list. The entire TAF as a list of strings. Every line is it's own string
    :return: list. List of strings. Same as input but with fixed times.
    turns:
    ['TAF KBAD 120200Z 1202/1308 30012KT 9999 SCT020 QNH2993INS',
    'BECMG 1223/1224 35009KT 9999 SKC QNH2994INS TX35/1221Z TN26/1311Z']
    into:
    ['TAF KBAD 120200Z 1202/1308 30012KT 9999 SCT020 QNH2993INS',
    'BECMG 1223/1300 35009KT 9999 SKC QNH2994INS TX35/1221Z TN26/1311Z']
    """
    # if no month and date was input assume it is the current month and year
    gm_time = time.gmtime()
    if month is None:
        month = gm_time[1]
    if year is None:
        year = gm_time[0]
    gm_day = gm_time[2]  # get the current day

    regex_left_day = r'(?P<left_day>\d{2})(?=\d{2}/\d{4})'
    regex_right_day = r'(?<=\d{4}/)(?P<right_day>\d{2})(?=\d{2})'
    regex_right_hour = r'(?<=\d{4}/\d{2})(?P<right_hour>\d{2})'
    issue_day = int(re.search(regex_left_day, taf[0]).group())  # the day of the month the TAF was issued in
    if issue_day > gm_day:  # TAF is issued ont he 31 but today is the 2nd it must have been issued on the 31st last monnth
        month -= 1
        if month == 0:  # it was January and a month was subtracted. Goto December of last year
            month = 12
            year -= 1

    for index, line in enumerate(taf):
        if 'BECMG' in line or 'TEMPO' in line or index == 0 or is_prob_line(line):  # only BECMG lines might need to be fixed and or the first line ex. 1300/1324 should be 1300/1400
            right_hour_search = re.search(regex_right_hour, line)  # stores the value and locaiton of the right hour
            right_hour = right_hour_search.group()  # get the actual hour of the right side as
            if right_hour == '24':  # only lines that the right side ending at the 24th hour need to be fixed.
                right_day_search = re.search(regex_right_day, line)  # get the day of the right side
                right_day = right_day_search.group()  # get the actual right day, ex. 12
                right_day_location = right_day_search.span()  # the index in the string where the right day was found
                right_day = int(right_day) + 1  # increase the day of the right side by 1
                right_day = pad_number(right_day, 2)  # If the day of the month is single digit, pad it

                if int(right_day) > get_number_of_days_in_month(month, year):  # if so many days were added, it should now be the next month
                    month += 1  # not needed b/c month is not encoded into TAF, btu good to know
                    right_day = '01'  # first day of next month

                # now set the new TAF line
                new_right_day_and_hour = '{day}00'.format(day=right_day)
                taf[index] = replace_chars_in_str_over_range(line, right_day_location[0], new_right_day_and_hour)
                break  # At most line can have a right hour of 24 so no need to search for a 2nd.
    return taf


def adjust_date_higher_taf(line_day, issue_day, issue_month, issue_year):
    """
    A helper function. If a line of TAF has a day of 1 and the TAF was issued on the 31st, that line must be the
    1st of next month.
    :param line_day: str or int. The day of the month as seen in the line of the TAF.
    :param issue_day: str or int. The day the TAF was issued on from the first line.
    :param issue_month: str or int. The month the TAF was issued on
    :param issue_year: str or int. The month the TAF was issued on
    :return: dict. A dicitonary of the adjusted month and year
    """
    line_month = int(issue_month)
    line_year = int(issue_year)

    if int(line_day) < int(issue_day):  # Day of the line is 1 and today is the 31st it must be the 1st of next month
        line_month += 1  # goto next month
        if line_month == 13:  # there is no 13th month, go to January of next year
            line_month = 1
            line_year += 1

    return {'month': line_month, 'year': line_year}  # only month and year can be adjusted, no need to return day


def get_taf_issue_or_start_time_and_date(first_line, time_struct, issue_or_start='issue'):
    """
    TAF only encodes the day and time into the TAF. Month and year are not encoded at all. This detemriens the month
    and year the TAF was issued. If the day of the TAF is larger than the actual real world day than it assume the
    TAF was issued last month. For this to work TAF must have been issued either this month or last month.
    :param first_line: str. The first line of the TAF
    :param time_struct: time.struct_time. The result of running time.gmtime(). Used to prevent time racing
    :param issue_or_start: string. 'issue' or start' Wheter to get the exact issue time from the KBAD [150600]Z or from [1506]/1612
    :return: dict. The adjusted month and year in a dict. {'time': '1255', 'day': 22, 'month': 4, 'year': 2019}
    """

    # get the things that will not change: issue day and time
    if issue_or_start == 'issue':  # issue time in brackets TAF KBAD [150659]Z
        regex_issue_minute = r'(?<=\d{4})(?P<issue_minute>\d{2})(?=Z)'
        regex_issue_hour = r'(?<=\d{2})(?P<issue_hour>\d{2})(?=\d{2}Z)'
        regex_issue_day = r'(?P<issue_day>\d{2})(?=\d{4}Z)'

        issue_or_start_time = re.search(regex_issue_hour, first_line).group() + re.search(regex_issue_minute, first_line).group()
        issue_or_start_day = int(re.search(regex_issue_day, first_line).group())
    elif issue_or_start == 'start':  # start time in brackets KBAD 150659 [0700]/0806
        regex_left_hour = r'(?<=\d{2})(?P<left_hour>\d{2})(?=/\d{4})'
        regex_left_day = r'(?P<left_day>\d{2})(?=\d{2}/\d{4})'
        issue_or_start_time = f'{re.search(regex_left_hour, first_line).group()}00'
        issue_or_start_day = int(re.search(regex_left_day, first_line).group())
    else:
        raise Exception(f'Could not determine issue or start time:\n{issue_or_start}')

    # get the individidual elements of the time_struct
    gm_day = time_struct[2]
    issue_month = time_struct[1]  # for assume that TAF was issued on the current month
    issue_year = time_struct[0]  # for assume that TAF was issued on the current month

    # adjust issue month and year if need be
    if issue_or_start_day > gm_day:  # TAF has a day of 28, today is 2nd. Assume it is last months TAF
        issue_month -= 1  # go to last month
        if issue_month == 0:  # if going to last month went to month 0 go to December a year ago
            issue_year -= 1  # go back a year
            issue_month = 12  # and set it to december

    return {'time': issue_or_start_time, 'day': issue_or_start_day, 'month': issue_month, 'year': issue_year}


def get_taf_end_time_and_date(taf, time_struct):
    """
     Finds when the TAF expires. TAF KBAD 170500Z 1705/1811 end at 1100Z on the 18th of the 7th month and 2019th year.
     Assuming this is the 7th month and 2019th year.
    :param taf: list. The entire TAF as a list of strings. Every line is it's own string. Only the first line of the TAF is used
    :param time_struct: time.struct_time. The result of running time.gmtime(). Used to prevent time racing
    :return: dict. {'time': '1503', 'day': 2, 'month': 5, 'year': 2019}
    """
    first_line = taf[0]  # first line contain the end time of the TAF
    regex_right_day = r'(?<=\d{4}/)(?P<right_day>\d{2})(?=\d{2})'  # TAF KBAD 150000Z 1500/[16]06
    regex_right_hour = r'(?<=\d{4}/\d{2})(?P<right_hour>\d{2})'  # TAF KBAD 150000Z 1500/16[06]

    issue_date_and_time = get_taf_issue_or_start_time_and_date(taf[0], time_struct, issue_or_start='start')  # month and year might have to be adjusted later for which issue date and time are needed
    issue_day = issue_date_and_time['day']
    issue_month = issue_date_and_time['month']
    issue_year = issue_date_and_time['year']


    # get the end times, day of month and time are known for sure
    end_day = int(re.search(regex_right_day, first_line).group())  # get the day from the from the DDHH/[DD]HH group.
    end_hour = re.search(regex_right_hour, first_line).group()  # get the hour the TAF ends DDHH/DD[HH]
    end_month = issue_month  # for now assume that taf_test ends the same month and year as it start in
    end_year = issue_year


    # adjusted_date = adjust_date_higher_taf(taf_end_day, issue_day, issue_month, issue_year)  # if the TAF ends on 01 and today is the 31st, it is next month
    # issue_month = adjusted_date['month']
    # end_year = adjusted_date['year']

    # issue_day = adjusted_date['day']  # adjusted date does not return day, only month and year

    # If today is the 28th of June, the TAF start on the 30th, it must be the 30th of May, and end on the 1st it should be 1st of June.

    if end_day < issue_day:  # TAF end on the 1st of June but start on the 30th of May
        end_month += 1
        if end_month == 13:  # if next month is January next year
            end_month = 1
            if end_year == issue_year:
                end_year += 1
                pass


    return {'time': f'{end_hour}00',
            'day': end_day,
            'month': end_month,
            'year': end_year}


def get_taf_time_frame(taf, time_struct):
    """
    Get the start and end time of the TAF. TAF start time is the KSHV 151858Z [1519]/1619 group, not from issue time
    :param taf: list. The entire TAF as a list of strings. Every line is it's own string
    :param time_struct: time.struct_time. The result of running time.gmtime(). Used to prevent time racing
    :return: dict. A dicts of two dicts with the start and end time frame
    """

    # get the star time
    start = get_taf_issue_or_start_time_and_date(taf[0], time_struct, issue_or_start='start')

    # get the end time
    end = get_taf_end_time_and_date(taf, time_struct)

    return {'start': start, 'end': end}


def get_line_time_range(taf, index, time_struct):
    """
    Get the soft and hard start and end times of the line of the TAF.
    :param taf: list. The entire TAF as list of strings where each line is it's own input_str in the list.
    :param index: input_str. Which index (line-1) of the TAF the date and time is requested
    :param time_struct: time.struct_time.The result of running time.gmtime() needed to avoid time racing when
    running this function for every line.
    :return: dict. A dictionary of the time
    """
    # Regex to find the times. finds 132056Z, 1405/1406, FM131500
    regex_all_times = r'(((?P<issue_day>\d{2})(?P<issue_hour>\d{2})(?P<issue_minute>\d{2})(?=Z))?(((?<=FM))(?P<nws_day>\d{2})(?P<nws_hour>\d{2})(?P<nws_minute>\d{2}))?(((?P<left_day>\d{2})(?P<left_hour>\d{2}))/((?P<right_day>\d{2})(?P<right_hour>\d{2})))?)'
    regex_fm_group = r'FM\d{6}'  # regex just for FM group

    # individual regexs
    regex_left_day = r'(?P<left_day>\d{2})(?=\d{2}/\d{4})'
    regex_left_hour = r'(?<=\d{2})(?P<left_hour>\d{2})(?=/\d{4})'
    regex_right_day = r'(?<=\d{4}/)(?P<right_day>\d{2})(?=\d{2})'
    regex_right_hour = r'(?<=\d{4}/\d{2})(?P<right_hour>\d{2})'

    regex_fm_day = r'(?<=FM)(?P<fm_day>\d{2})(?=\d{4})'
    regex_fm_hour = r'(?<=FM\d{2})(?P<fm_hour>\d{2})(?=\d{2})'
    regex_fm_minute = r'(?<=FM\d{4})(?P<fm_minute>\d{2})'

    taf_first_line = taf[0]  # get the first line of the TAF
    taf_last_line = taf[-1]
    issue_day = int(re.search(regex_left_day, taf_first_line).group())  # get the day from the from the [DD]HH/DDHH group. The day the TAF was issued, might be wrong if DDHHMMZ is different
    taf_end_day = int(re.search(regex_right_day, taf_first_line).group())  # get the day from the from the DDHH/[DD]HH group.  # TODO replace with get_taf_end_time_and_date, wont do it for now because it is only two lines calling the function and storing the output would be 3 lines.
    taf_end_hour = re.search(regex_right_hour, taf_first_line).group()  # get the hour the TAF ends DDHH/DD[HH]  # TODO replace with get_taf_end_time_and_date

    # detemrine the initial month and year the TAF was issued in
    fixed_month_year = get_taf_issue_or_start_time_and_date(taf_first_line, time_struct)
    issue_month = fixed_month_year['month']
    issue_year = fixed_month_year['year']

    # what will be returned by default
    # soft and hard exists because with BECMG groups 2 lines can be valid at a time
    date_time_dict = {
        'soft_start_time': None,
        'soft_start_day': None,
        'soft_start_month': None,
        'soft_start_year': None,
        'hard_start_time': None,
        'hard_start_day': None,
        'hard_start_month': None,
        'hard_start_year': None,
        'soft_end_time': None,
        'soft_end_day': None,
        'soft_end_month': None,
        'soft_end_year': None,
        'hard_end_time': None,
        'hard_end_day': None,
        'hard_end_month': None,
        'hard_end_year': None
    }

    # START TIME(S), end time also set if line is a TEMPO or PROB line
    # get the hard and if applicable the soft start time from either the DDHH/DDHH or FMDDHHMM group
    line_current = taf[index]  # the current line of th TAF
    if 'BECMG' in line_current:  # If True there will be a soft start in addition to the hard start. Also works
        # on the first line.

        # SOFT START date and time
        soft_start_time = '{}00'.format(re.search(regex_left_hour, line_current).group())  # get the 4 digits time
        soft_start_day = re.search(regex_left_day, line_current).group()  # get the day BECMG [DD]HH/DDHH

        adjusted_month_and_year = adjust_date_higher_taf(soft_start_day, issue_day, issue_month, issue_year)
        soft_start_month = adjusted_month_and_year['month']
        soft_start_year = adjusted_month_and_year['year']

        # set the soft start date and time in the dict
        date_time_dict['soft_start_time'] = soft_start_time
        date_time_dict['soft_start_day'] = int(soft_start_day)
        date_time_dict['soft_start_month'] = soft_start_month
        date_time_dict['soft_start_year'] = soft_start_year

        # HARD START date and time
        hard_start_time = '{}00'.format(re.search(regex_right_hour, line_current).group())  # get the 4 digits time
        hard_start_day = re.search(regex_right_day, line_current).group()  # get the day BECMG [DD]HH/DDHH


    # if line is a TEMPO line. Also add the end time since it is given in the same line
    elif 'TEMPO' in line_current or index == 0 or is_prob_line(line_current):  # if the current line is a TEMPO line than it has hard start and stop times. If it is the first line, valid time is from the left side of the DDHH/DDHH group, same as for the tempo group
        # HARD START date and time
        hard_start_time = '{}00'.format(re.search(regex_left_hour, line_current).group())  # get the 4 digits time
        hard_start_day = re.search(regex_left_day, line_current).group()  # get the day BECMG [DD]HH/DDHH

        # END time of the TEMPO but not the first line
        # TEMPO line gives the end time in the line, it is the only one to do so. HARD END date and time
        if 'TEMPO' in line_current or is_prob_line(line_current):  # if it is the current line also add the end time
            hard_end_time = '{}00'.format(re.search(regex_right_hour, line_current).group())  # get the 4 digits time
            hard_end_day = re.search(regex_right_day, line_current).group()  # get the day BECMG [DD]HH/DDHH


    # line is FM line like FM130800 35009KT 7SM
    elif re.search(regex_fm_group, line_current).group() is not None:
        hard_start_time = re.search(regex_fm_hour, line_current).group() + re.search(regex_fm_minute, line_current).group()  # the current time in HHMM format
        hard_start_day = re.search(regex_fm_day, line_current).group()

    else:
        raise Exception('Could not determine if the line is:'
                        'the first line'
                        'BECMG line'
                        'TEMPO / PROB line'
                        'FMDDHHMM aka NWS FM line')

    # Save all the various start times to the dict
    # set HARD START Times, soft is set in the only only one that has soft start time, BECMG
    adjusted_month_and_year = adjust_date_higher_taf(hard_start_day, issue_day, issue_month, issue_year)
    hard_start_month = adjusted_month_and_year['month']
    hard_start_year = adjusted_month_and_year['year']

    date_time_dict['hard_start_time'] = hard_start_time
    date_time_dict['hard_start_day'] = int(hard_start_day)
    date_time_dict['hard_start_month'] = hard_start_month
    date_time_dict['hard_start_year'] = hard_start_year

    # END TIME(S)
    # find the next line in the TAF that will stop the current line from being valid. Either BECMG, FM, and end of TAF
    for line_after_current in taf[index + 1:]:  # for the lines after the current line in the taf_test

        # if the next current line is a TEMPO or PROB line don't obther checking and setting end time here because it was already set
        if 'TEMPO' in line_current or is_prob_line(line_current):
            break

        elif 'TEMPO' in line_after_current or is_prob_line(line_after_current):  # the next line is a TEMPO, go to the next one after that
            pass

        elif 'BECMG' in line_after_current:  # next line is BECMG. current line will have soft and hard ending
            # SOFT END date and time
            soft_end_time = '{}00'.format(re.search(regex_left_hour, line_after_current).group())  # get the 4 digits time
            soft_end_day = re.search(regex_left_day, line_after_current).group()  # get the day BECMG [DD]HH/DDHH

            adjusted_month_and_year = adjust_date_higher_taf(soft_end_day, issue_day, issue_month, issue_year)
            soft_end_month = adjusted_month_and_year['month']
            soft_end_year = adjusted_month_and_year['year']

            # set the soft start date and time in the dict
            date_time_dict['soft_end_time'] = soft_end_time
            date_time_dict['soft_end_day'] = int(soft_end_day)
            date_time_dict['soft_end_month'] = soft_end_month
            date_time_dict['soft_end_year'] = soft_end_year

            # HARD END date and time
            hard_end_time = '{}00'.format(re.search(regex_right_hour, line_after_current).group())  # get the 4 digits time
            hard_end_day = re.search(regex_right_day, line_after_current).group()  # get the day BECMG [DD]HH/DDHH
            break

        elif re.search(regex_fm_group, line_after_current):  # if the next line is a FM line
            hard_end_time = re.search(regex_fm_hour, line_after_current).group() + re.search(regex_fm_minute, line_after_current).group()  # the current time in HHMM format
            hard_end_day = re.search(regex_fm_day, line_after_current).group()
            break

        elif line_after_current == taf_last_line:  # is the line after is the last line and we were not able to get the end time from it use taf_test end time. This occurs when the 2nd to last line is a BECMG or FM line and the one after is a TEMPO or PROB one
            hard_end_time = '{}00'.format(taf_end_hour)
            hard_end_day = taf_end_day

    else:  # if the for loop didnt run once the current line is already the last line and get end time for the first line DDHH/[DDHH] group
        hard_end_time = '{}00'.format(taf_end_hour)
        hard_end_day = taf_end_day

    # set the hard start date and time in the dict
    adjusted_month_and_year = adjust_date_higher_taf(hard_end_day, issue_day, issue_month, issue_year)
    hard_end_month = adjusted_month_and_year['month']
    hard_end_year = adjusted_month_and_year['year']

    date_time_dict['hard_end_time'] = hard_end_time
    date_time_dict['hard_end_day'] = int(hard_end_day)
    date_time_dict['hard_end_month'] = hard_end_month
    date_time_dict['hard_end_year'] = hard_end_year

    return date_time_dict  # return the dict of all the possible times, some values may be None


def get_zulu_time(time_struct=None):
    """
    Gets the current time in zulu.
    :return: a string version of the current zulu time in HHMM format
    """
    if time_struct is None:
        time_struct = time.gmtime()

    return time.strftime('%H%M', time_struct)


def adjust_date_lower(time_zulu, day, month, year, reference_day=None):  # a helper function
    """
    Uses common sense to figure out the month and year of a METAR.
    This is just for METARs, Lets say there is a METAR like KBAD 290556Z meaning 29th of the month at 0556Z.
    it is the 1st of Feb so the 29th in the METAR must be the 29th of January. This function figures that out.
    METARs do not show month or year, only day of the month and time zulu. time_zulu and day will never be changed by this function as the are specified by the METAR and therefor accurate.
    time_zulu and day are just needed as inputs but are returned anyway to be redundant.
    :param time_zulu: input_str. Current zulutime in HHMM format ex. '2046', '0052', '1035'
    :param day: int. Day of the month of the METAR. Ex. 2, 31, 15. A
    :param month: int. what month is is, 1, 4, 12. Function assume it is either this month or last month.
    :param year: int. Usually The current year 2019, 2020, 2012. Can be any year. Note that the PK WND year will based on this
    :param reference_day. int. Optional for when you want to run this function on obs that are older (last month or more)
    :return: dict. of the adjusted month, day, year ex. {'time_zulu': '1050', 'day': 25, 'month': 5, 'year': 2019}
    """
    # in case they got passed as a string
    day = int(day)
    month = int(month)
    year = int(year)

    # Let'taf_as_one_str say there is an ob that has a time of 1409z but currently it is only 0841z than we assume that it is 1409z the day prior
    # this should not be triggered ever, if it is there is something wrong with the code or less likely, the obs.  # TODO what does this comment refer to? Doesn't look like it's relevant. remove it?
    if reference_day is None:  # generate a reference day based on the current day
        reference_day = time.gmtime()[2]  # get the current day
    else:  # nothing to do the user specified the reference day
        pass

    if day == reference_day:  # if the main METAR day is the same as actual day we check the times, this one should be triggered
        if int(time_zulu) > int(get_zulu_time()):  # if time zulu from the ob is larger than the
            # actual zulu time, should only be triggered when there is a PK WND remark for the previous day
            # ex. KBAD 31015Z bla bla PK WND 25034/2359 the 2359 happened on the 30th
            month -= 1  # if the day is zero is means that we are actually on the last dya of the previous month
            if month == 0:  # no such thing as 0 month, go back to last year'taf_as_one_str december
                month = 12
                year -= 1
            # day = get_number_of_days_in_month(month, year)  # the day might be set to 0, but we want 1 instead

    elif gm_day < day:  # if actual day (day we run this script on) is less than the day in the ob
        # the ob day must be last months days because for ex. the 23th must be the 23th of feb if today is only the 20th of april
        month -= 1  # make month last month if the day is ahead of this months day
        if month == 0:  # no such thing as 0 month, go back to last year'taf_as_one_str december
            month = 12
            year -= 1
    return {'time': time_zulu, 'day': day, 'month': month, 'year': year}  # return the adjusted month and year with the original day and time zulu


def find_row_closest_to_time(df, time_to_match):
    """
    Find the row of the df that has the timestamp closest to the specified time_to_match. Peforms a binary search of the df.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :param time: datetime. A pandas datetime object. The row to  be found will be mostly closely to this datetime
    :return: int. The row index of the ob that most closest matches the time_to_match argument.
    """
    # if the time_to_match is not even in the time range of the obs df returns the closest end point
    if time_to_match < df.index[0]:  # time to match is before the first ob
        return 0
    elif time_to_match > df.index[-1]:  # time to match is after the last ob
        return len(df) - 1

    # the initial bounds are the end points of the df
    lower_bound = 0
    upper_bound = len(df) - 1

    while True:  # break from the loop when a match has been found
        move_amount = int((upper_bound - lower_bound) / 2)  # the amount to move by is half the distance between the lower and upper bound
        guess_index = lower_bound + move_amount  # the next guess is the lower bound plus the move amount
        guess_date_time = df.index[guess_index]  # get the datetime object of the guess row

        if guess_date_time > time_to_match:  # guess was too high, upper bound is now guess index
            upper_bound = guess_index
        elif guess_date_time < time_to_match:  # guess was too low, lower bound is now guess index
            lower_bound = guess_index
        elif guess_date_time == time_to_match:  # guess is an exact match to an ob
            return guess_index

        if upper_bound - lower_bound == 1:  # the time to match is not an exact match to one of the obs. instead it lies between two obs. If lower and upper are next to each other then the ob is in between them.
            # determine the time difference from the time to match to either bound
            time_diff_from_upper_bound = (df.index[upper_bound] - time_to_match).total_seconds()
            time_diff_from_lower_bound = (time_to_match - df.index[lower_bound]).total_seconds()

            # return the row that is closer to the time to match
            if time_diff_from_lower_bound < time_diff_from_upper_bound:
                return lower_bound
            else:
                return upper_bound





def get_time_len_of_df(df):
    """
    Get the length of the dataframe, not the row count but the time difference between the first and last ob. Length in seconds.
    NOTE: This function assumes that the valid column of hte df is set as the index of the df.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :return: float. The length of the df in seconds
    """

    return (df.index[len(df)-1] - df.index[0]).total_seconds()


if __name__ == '__main__':
    path = '/home/harrison_fjord/Documents/ml_ai_ds_forecaster/obs/KBAD/KBAD.csv'
    from other_functions.csv_tools import set_valid_date_time_as_index
    import pandas as pd
    df = pd.read_csv(path)
    df = set_valid_date_time_as_index(df=df)
    # time_to_match = pd.to_datetime('2006-10-16 04:24:00')
    # print(find_row_closest_to_time(df=df, time_to_match=time_to_match))
    print(get_time_len_of_df(df=df))