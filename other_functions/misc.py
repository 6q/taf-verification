import re
import platform    # For getting the operating system name
import subprocess  # For executing a shell command

def pad_number(number, desired_length=4):
    """
    Adds '0' to the beginning of number so that it reaches the desired length. Turns 4 into '0004', 54 into '0054' etc.
    :param desired_length: int. How long the length of the output should be
    :param number: int or input_str. Enter a numbeer as a int or input_str
    :return: input_str. A number  with '0'taf_as_one_str in front of it.
    """
    how_many_zeros_to_add = desired_length - len(str(number))
    return how_many_zeros_to_add * '0' + str(number)


def replace_chars_in_str_over_range(input_str, index_start, replacement):
    """
    Replaces part of a string with another string. Start at index_left
    :param input_str: stirng. The stirng in which you want to cxhange something
    :param index_start: int. The index at which the replacement starts
    :param replacement: string. What to replace part of the stirng with
    :return: string. The string with the desired part replaced.
    turns:
    replace_chars_in_str_over_range('Hello World. My name is python', 6, 'PLURD'))
    into
    'Hello PLURD. My name is python'
    """
    input_str = list(input_str)  # convert the string to a list
    input_str[index_start:index_start + len(replacement)] = replacement  # replace the original starting at index with replacement
    return ''.join(input_str)  # convert the list of chars back to a single input_str


def pretty_number(number):
    """
    Convert the input number to a pretty string version of the number.
    EX. -12340089 -> -12,340,089,    1219 -> 1,219
    :param number: int. Any number
    :return: string. The pretty version of the number Ex. '3,000', '-5,000,504'
    """
    # the negative sign will screw up the commas it might turn out to -,506. For now make positive
    number_is_negative = False
    if number < 0:
        number_is_negative = True
        number *= -1

    number_reversed_list = list(str(number))[::-1]  # convert to list and go through it backwards
    for index, digit in enumerate(number_reversed_list):
        index += 1  # increase by 1 because to start at 1 not 0
        if index % 4 == 0 and index != 0:  # want to add the , every 3rd digit, every 4th char is a comma
            number_reversed_list.insert(index-1, ',')  # -1 because is checks for every 4 but , show up every 3rd

    if number_is_negative:  # add the negative sign again
        number_reversed_list.insert(len(number_reversed_list), '-')

    return ''.join(number_reversed_list[::-1])  # reverse the reversed list and return it


def is_prob_line(taf_line):
    """
    Some TAFs use PROB## lines. PRBO30 means there is a 30 % chance of this happening. Similar to TEMPO lines.
    :param taf_line: str. Any line of the TAF.
    :return: boolean. True if the line is a PROB## line, False otherwise
    """
    return bool(re.search(r'PROB(?P<percent_chance>\d{2}) ', taf_line))


def is_fm_line(taf_line):
    """
    Determines if a line of the TAF is a FM line. Ex. 'FM230900 05002KT P6SM VCTS OVC015CB'
    :param line: string. Any line of the TAF
    :return: boolean. True is the line is a FM line, False if it is not
    """
    return bool(re.search(r'FM\d{6}', taf_line))


def convert_temp(temp, current_unit='f'):
    """
    Convert Fahrenheit to Celsius and vice versa.
    :param temp: float
    :param current_unit: string, 'f' if the input temp is in Fahrenheit. 'c' if the input temp is in Celsius.
    :return: float. The converted temp rounded to 2 decimal places
    """
    temp = float(temp)
    if current_unit == 'f':
        return round((temp - 32.0) / 1.8, 2)
    elif current_unit == 'c':
        return round((1.8 * temp) + 32.0, 2)


def boolean_to_up_or_down(boolean):
    """
    Used with the ping function. A quick function that is useful when printing to the user if a server is up (True) or if the server is down (False)
    :param boolean:
    :return:
    """
    return 'up' if boolean else 'down'


def ping(host):
    """
    :param host: string. The URL or IP to be pinged
    :return: boolean. True if host (str) responds to a ping request. False if not
    Remember that a host may not respond to a ping (ICMP) request even if the host name is valid.
    """

    # Option for the number of packets as a function of
    param = '-n' if platform.system().lower() == 'windows' else '-c'

    # Building the command. Ex: "ping -c 1 google.com"
    command = ['ping', param, '1', host]

    return subprocess.call(command, stdout=subprocess.DEVNULL, stderr=subprocess.DEVNULL) == 0  # 0 means the command did not return an error


if __name__ == '__main__':
    print(ping('goasdasdasdasd323232233232ogle.com'))
