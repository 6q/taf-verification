import re

from other_functions.visibility_conversion import visibility_conversion_table
import other_functions.regexes as regexes


def get_wind(ob_str):
    """
    Get the wind speed and wind gust if applicable from the OB. Does NOT get PK WND.
    :param ob_str: One complete METAR as a string.
    :return: a dict of wind direction, speed and gust if applicable.
    """
    regex = r"((\d{3})|(VRB))(\d{2,3})(KT|(G)(\d{2,3})KT)"  # split wind into its own groups for dir spd and gst

    # get the individual elements from the wind
    try:
        wind_dir = re.search(regex, ob_str).group(1)
        wind_speed_sustained = re.search(regex, ob_str).group(4)
    except AttributeError:  # If the wind is missing set it to None
        wind_dir = None
        wind_speed_sustained = None

    # not all ob have winds gusts, if they dont add None as the gust
    try:
        wind_speed_gust = re.search(regex, ob_str).group(7)
    except (IndexError, AttributeError):
        wind_speed_gust = None

    return {'wind_dir': wind_dir,
            'wind_speed_sustained': wind_speed_sustained,
            'wind_speed_gust': wind_speed_gust}


def convert_visibility(input_visibility):
    """
    Can convert SM to meters and meters to SM
    :param input_visibility: str. Vis in SM or meters as a string.
    :return: the converted visibility. input_str for SM and meters (input_str)
    """
    # if there is no vis, it is encoded as M for missing
    if input_visibility == 'M':
        return 'M'

    regex_meters = r'\d{4}'
    if re.search(regex_meters, input_visibility):  # in meters
        return visibility_conversion_table[input_visibility]  # return the SM version

    else:  # in SM
        try:  # try to convert the SM vis into an int, will thrown and error on lower vis ex '1 1/2'sm
            if int(input_visibility) >= 7:
                return '9999'
            else:  # in SM but lower than 7 and not an int ex 1/2, 5/8, 1 5/8
                return visibility_conversion_table[input_visibility]
        except ValueError:  # throws and error with lower vis  ex 1 1/2SM. When it is not just one digit, ex 7SM, 4SM
            return visibility_conversion_table[input_visibility]


def get_visibility(ob_str):
    """
    Gets the visibility from the METAR.
    :param ob_str: One complete METAR as a string.
    :return: str. The visibility ex '1 1/2' '7'
    """
    regex_for_sm = r'(?<![a-z]|[A-Z])((\d/\d)|(\d \d/\d)|(\d{,2}))(?=SM)'  # acceptable formats, 7SM, 10SM, 1/2SM, 1 1/2SM

    if 'P6SM' in ob_str:  # NWS TAFs use P6SM instead of 7SM
        visibility = '7'
    elif re.search(r'\dSM ', ob_str):  # vis is encoded in SM
        visibility = re.search(regex_for_sm, ob_str).group(0)
    else:  # must be encoded in meters
        try:
            regex_for_meters = r'(?<!FM)(?<!NEXT )(?<!WSHFT )(?<= )(?P<vis_meters>\d{4})(?= )(?! NEXT|/|\d|INS| LTG)'
            # regex_for_meters = r'(?<!NEXT |/|FM|\d|QNH)(?P<vis_meters>\d{4})(?! NEXT|/|\d|INS)'
            visibility = re.search(regex_for_meters, ob_str).group('vis_meters')
        except AttributeError:
            visibility = 'M'

    return visibility


def extract_precip(ob_str):
    match = re.search(regexes.regex_hourly_precip, ob_str)
    if match is None:
        return None
    else:
        hourly_precip = match.group('hr_precip')
        return int(hourly_precip)



def get_weather(ob_str, filter_icao=True, filter_rmks=True, return_type='dict'):
    """
    Decodes the weather group from the ob and returns it broken down by weather element
    :param ob_str: One complete METAR as a string.
    :param filter_rmks: boolean. In order to now the the remarks section (everything after RMK) yield false positives, ignore it.
    :param filter_icao: boolean. In order to not have the ICAO yield a false positive for a weather match we can ignore the first
    4 chars of the metar.
    :param return_type: string. Either 'tuple' or 'dict' What to return the weather elements as
    :param check_if_already_processed. boolean. Must be used in conjunction with calling np vectorize and the pandas df.
    :param is_processed_column. Must be used in conjunction with calling np vectorize and the pandas df.
    :return: a dict of the weather broken down by elements, None for those not in the ob, None for all if no weather happened
    """
    # print(ob_str)  # DEBUG
    if ob_str is None:  # no ob string was entered, return None for all weather attributes.
        no_ob = True
    else:
        no_ob = False

    # ?P<wx_intensity>: "+" or -"-" or "" is for wx intensity, not necessarily in every ob
    # ?P<vicinity_or_not>: "VC" or "" not every ob will include VC and it is not required
    # ?P<description>,  no necessarily included
    # regex_weather = r'(?P<wx_intensity>[-+](?!FC)(?=BC|BL|DR|FZ|MI|PR(?!ES)|SH|TS|DZ|GR|GS|IC|PL|RA|SG|SN|UP|DS|FC|PO|SQ|SS))?(?P<vicinity_or_not>(?<!(O))VC)?(?P<description>(BC|(?<!VIS)BL|DR|FZ|MI|PR(?!ES)|SH(?!FT)|TS(?! OHD| ALQDS| [NESW]| VIC| VC))(?!B\d|E\d))?(?P<precipitation>(DZ|GR|GS|(?<!LTG)IC|PL|RA|SG|SN(?!T)|UP)(?!B\d|E\d))?(?P<obscuration>BR|DU|FG|FU|HZ|PY|SA|VA)?(?P<other>DS(?!NT)|(?<!TEM)PO|SQ|SS)?'  # does not do +FC, FC because the + does not indicate intensity, we will search for rotation separately
    # regexes.regex_weather

    # ICAO are the first four letters if the input is and entire METAR
    if filter_icao:
        start_index = 4
    else:
        start_index = 0

    if not no_ob:  # if no_ob is True then the ob is just None and the below code won't work
        # Remarks is everything after the RMK in a METAR
        try:
            if filter_rmks:
                if ' RMK' in ob_str:  # to filter the RMKs out we first need to determine if there are RMKs in the ob
                    end_index = ob_str.find(' RMK') + 1  # the space in front of RMK ensures a station with RMK in the ICAO does noto get matched, +1 to go to the R not the space
                else:  # if there are no RMKs, use the entire string
                    filter_rmks = False  # RMKs could not be found in the ob, don't filter them
            if filter_rmks is False:
                end_index = len(ob_str) - 1  # set the end index to the last index of the string
        except TypeError:
            print(ob_str)
            raise Exception(f'ob_str: {ob_str}')

        # weather = re.finditer(regex_weather, ob_str[start_index:end_index])  # find in the METAR minus the excluding the first 4 chars for the ICAO and excluding the remarks  # REGEX not compiled
        weather = regexes.regex_weather.finditer(ob_str[start_index:end_index])  # find in the METAR minus the excluding the first 4 chars for the ICAO and excluding the remarks  # for when REGEX is compiled
    # define weather attributes by default as None so no error is thrown when adding it to the JSON
    wx_intensity = None
    vicinity_or_not = None
    description = None
    precipitation = None
    obscuration = None
    other = None
    rotation = None  # used for tornadoes, funnel clouds, water spout
    has_liquid_precip = None
    has_solid_precip = None


    if not no_ob:  # there is a ob and we can process some weather elements
        # check for tornadoes, water spouts, and funnel cloud. This assume there is only either +FC or FC or no FC anything
        if ' +FC ' in ob_str:  # if tornado in ob
            rotation = 'tornado'
        if ' FC ' in ob_str:  # water spout or funnel cloud
            rotation = 'funnel cloud or water spout'

        precipitation = ''  # used to store the precips found, will set be back to None below if there were actually no precips found

        for match_num, match in enumerate(weather, start=1):
            if match.group('wx_intensity') is not None:
                wx_intensity = match.group('wx_intensity')
                # print('checking wx_intensity')

            if match.group('vicinity_or_not') is not None:
                vicinity_or_not = match.group('vicinity_or_not')
                # print(vicinity_or_not)

            if match.group('description') is not None:
                description = match.group('description')
                # print(description)

            if match.group('precipitation') is not None:
                precipitation += match.group('precipitation')

                precip_liquids = ['RA', 'DZ']  # all the different liquid precips
                precip_solids = ['GR', 'GS', 'IC', 'PL', 'SG', 'SN']  # all the different solid precips

                if any(precip_liquid in precipitation for precip_liquid in precip_liquids):  # checks if at least one of the liquid precips is in the obs precip
                    has_liquid_precip = True

                if any(precip_solid in precipitation for precip_solid in precip_solids):  # checks if at least one of hte solid precips in in the obs precip
                    has_solid_precip = True

                # for special case FZRA / FZ DZ
                try:
                    if 'FZ' in description and ('RA' in precipitation or 'DZ' in precipitation):  # checking freezing rain (FZRA) is tricky because it split over two vars. Freezing rain should be counted as both liquid and solid
                        has_solid_precip = True
                        has_liquid_precip = True
                except TypeError:  # will trigger if description is None. In that case we know we don't have FRZA
                    pass

                # for special case UP, will determine if liquid or solid based on temp
                if 'UP' in precipitation:
                    temp = get_temp_and_dew_point(ob_str)  # get the temperature and dew point group
                    if temp is not None:  # if there actually is temp in the ob. If temp was not encoded into the ob then skip this step
                        temp = temp['temp']
                        if temp < -3:  # if below 3C then assume it was solid precip
                            has_solid_precip = True
                        elif temp > 3:  # if temp was above 3C assume that the precip was liquid
                            has_liquid_precip = True
                        else:  # precip was between -3C and 3C, assume it was both liquid and solid
                            has_liquid_precip = True
                            has_solid_precip = True

            if match.group('obscuration') is not None:
                obscuration = match.group('obscuration')
                # print(obscuration)

            if match.group('other') is not None:
                other = match.group('other')
                # print(other)

    if precipitation == '':  # no precips were actually found, set back to None
        precipitation = None

    # return as tuple, used for numpy vectorize
    if return_type == 'tuple':
        return wx_intensity, vicinity_or_not, description, precipitation, obscuration, other, rotation, has_liquid_precip, has_solid_precip


    # return as dict
    return {
        'wx_intensity': wx_intensity,
        'vicinity_or_not': vicinity_or_not,
        'description': description,
        'precipitation': precipitation,
        'obscuration': obscuration,
        'other': other,
        'rotation': rotation,
        # 'precip_liquid_or_solid': precip_liquid_or_solid
        'has_liquid_precip': has_liquid_precip,
        'has_solid_precip': has_solid_precip
        }


def get_clouds(ob_str):
    """
    Gets the cloud layer(taf_as_one_str) and amount from the METAR.
    :param ob_str: One complete METAR as a string.
    :return: dict of the clouds broken down by layer where the element is broken into amount and base.
    """
    regex = r'(CLR|SKC)|((FEW|SCT|BKN|OVC|VV)(\d{3}))'
    clouds = re.finditer(regex, ob_str, re.DOTALL)
    clouds_out = {}  # the dict of the cloud layers and bases

    # for every individual cloud layer in the clouds group
    for iter_count, cloud in enumerate(clouds):
        # add a cloud group for CLR with no base
        if cloud.group() == 'CLR':
            clouds_out['cloud_layer_{}'.format(iter_count + 1)] = {'amount': 'CLR', 'base': None}

        else:  # if there a cloud layer, aka not CLR
            cloud_amount = cloud.group(3)
            cloud_base = cloud.group(4)

            # unlike with the if statement is is necessary to have two [][] in order to add more than one layer
            clouds_out['cloud_layer_{}'.format(iter_count + 1)] = {'amount': cloud_amount, 'base': cloud_base}

    return clouds_out


def get_temp_and_dew_point(ob_str, get_exact=False):
    """
    Get'taf_as_one_str the temperature from the METAR. Either from the Temp/Dew point group or from the T group. If user wants to get temp from the T group but there is no T group it will try to get temp from the main temp and dew point group.
    :param ob_str: One complete METAR as a string.
    :param get_exact: Can get the exact temperature as found in the T group
    :return: dict of the temp and dew point as either int or float. If it can't find temp/dew point then it returns None
    """
    if get_exact:  # uses the T group
        regex = r'T(?P<sign_temp>\d)(?P<temp>\d{3})(?P<sign_dew>\d)(?P<dew_point>\d{3})'
        temp_and_dew_point = re.search(regex, ob_str)
        if temp_and_dew_point is None:  # If we couldn't find the T group try from the main part of the ob
            get_exact = False  # get exact failed if we got here
        else:  # there is a T group
            temp = int(temp_and_dew_point.group('temp')) / 10  # get temp numbers only ex 12, 4, M14 will show up as 14. /10 to get the decimal
            if temp_and_dew_point.group('sign_temp') == '1':  # if there is an 1 in front of temp that means temp is negative
                temp *= -1  # make the temp negative

            dew_point = int(
                temp_and_dew_point.group('dew_point')) / 10  # get dew point numbers only ex 12, 4, M14 will show up as 14
            if temp_and_dew_point.group('sign_dew') == '1':  # 1 indicated dew point is negative
                dew_point *= -1
    if get_exact is False:  # uses the temp/dew point group. Will be triggered if the T group fails
        regex = r'(?P<sign_temp>M?)(?P<temp>\d{2})/(?P<sign_dew>M?)(?P<dew_point>\d{2})'
        temp_and_dew_point = re.search(regex, ob_str)
        if temp_and_dew_point is None:  # there is no temp/dewpoint group in the ob
            return None

        temp = int(temp_and_dew_point.group('temp'))  # get temp numbers only ex 12, 4, M14 will show up as 14
        if temp_and_dew_point.group('sign_temp') == 'M':  # if there is an M in front of temp that means temp is negative
            temp *= -1

        dew_point = int(
            temp_and_dew_point.group('dew_point'))  # get dew point numbers only ex 12, 4, M14 will show up as 14
        if temp_and_dew_point.group('sign_dew') == 'M':  # if dew point is negative
            dew_point *= -1

    return {'temp': temp, 'dew_point': dew_point}  # return a dict of the temp and dew point


if __name__ == '__main__':
    # s = 'KAVP 030029Z VRB03KT 6SM RASN FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'
    s = 'METAR KBOS 080300Z 25010KT 5SM -RASNPL 04/00 A//// RMK SLP983 T00390000'
    s = None
    print(get_weather(s))