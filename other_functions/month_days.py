num_days_in_a_month = {
    1: 31,
    2: 28,
    3: 31,
    4: 30,
    5: 31,
    6: 30,
    7: 31,
    8: 31,
    9: 30,
    10: 31,
    11: 30,
    12: 31
}

leap_years = [2000, 2004, 2008, 2012, 2016, 2020, 2024, 2028, 2032, 2036, 2040, 2044, 2048, 2052, 2056, 2060, 2064, 2068, 2072, 2076, 2080, 2084, 2088, 2092, 2096]

def get_number_of_days_in_month(month, year):
    # month: int ex 1 for January
    # year: int. needed because of leap years
    if month == 2:  # if it is February we need to check if it is a leap year
        if year in leap_years:
            return 29
        else:
            return 28
    else:
        try:
            return num_days_in_a_month[month]
        except KeyError:
            raise KeyError(f'Input month of {month} not in range of 1-12 (inclusive both ends)')

# print(get_number_of_days_in_month(2, 2000))

def is_leap_year(year):
    """
    Determine is a year is a leap year. Is limited to 2000-2096
    :param year: int. They year, Ex 2019
    :return: boolean. Wheter or not the year is a leap year.
    """
    return year in leap_years

def is_year_leap_year2(year):  # DOES NOT WORK YET
    """ Is leap year if
    The year can be evenly divided by 4
    If the year can be evenly divided by 100, it is NOT a leap year, unless
    The year is also evenly divisible by 400. Then it is a leap year.
    :param year: int. the year, ex 2019, 2040
    :return: boolean, True if the year is a leap year, False is not
    """
    if (year % 4) == 0:
        return False
    elif (year % 100) % 2 != 0:
        return True
    elif (year % 400) % 2 != 0:
        return True
    else:
        return True

# print(is_year_leap_year(2014))
# print(is_year_leap_year(2006))
# print(is_year_leap_year(2009))
# print(is_year_leap_year(2012))
# print(is_year_leap_year(2024))
# print(is_year_leap_year(2072))
# print(is_year_leap_year(2069))
