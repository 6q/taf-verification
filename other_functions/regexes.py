import re
regex_pk_wind = r'(?<=PK WND )(?P<wind_dir>\d{3})(?P<wind_speed>\d{2})(?:/)(?P<time>\d{2,4})'
regex_pk_wind = re.compile(regex_pk_wind)


regex_weather = r'(?P<wx_intensity>[-+](?!FC)(?=BC|BL|DR|FZ|MI|PR(?!ES)|SH|TS|DZ|GR|GS|IC|PL|RA|SG|SN|UP|DS|FC|PO|SQ|SS))?(?P<vicinity_or_not>(?<!(O))VC)?(?P<description>(BC|(?<!VIS)BL|DR|FZ|MI|PR(?!ES)|SH(?!FT)|TS(?! OHD| ALQDS| [NESW]| VIC| VC))(?!B\d|E\d))?(?P<precipitation>(DZ|GR|GS|(?<!LTG)IC|PL|RA|SG|SN(?!T)|UP)(?!B\d|E\d))?(?P<obscuration>BR|DU|FG|FU|HZ|PY|SA|VA)?(?P<other>DS(?!NT)|(?<!TEM)PO|SQ|SS)?'  # does not do +FC, FC because the + does not indicate intensity, we will search for rotation separately
regex_weather = re.compile(regex_weather)
# pk_wind_dir = re.search(pk_wind_regex, ob_str).group('wind_dir')
# pk_wind_speed = re.search(pk_wind_regex, ob_str).group('wind_speed')
# pk_wind_time = re.search(pk_wind_regex, ob_str).group('time')
regex_hourly_precip = r'(?:P)(?P<hr_precip>\d{4})(?: )'
regex_pk_wind = re.compile(regex_hourly_precip)






