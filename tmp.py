from random import random
import matplotlib.pyplot as plt

data = [random() for i in range(300000000)]  # a list of a million random float (0.0 to 1.0)
match = data[-30:]  # the last 300 float in the list. I want to find the closest duplicate of this list in the
# data = data[:-30]  # everything but the last 300 items
data_without_match = data[:-30]

best_match_start_index = 0  # where we keep track of the index at which the so far best match has been found
lowest_error_so_far = 10000000  # set to some stupid high value so it will replaced
for index, value in enumerate(data_without_match[:-30]):  # this list is about 1mil long minus the 30 for the reference match
    error_this_index = 0  # the lower the better
    for index_match, value_match in enumerate(match):  # this list is 300 long
        error_this_index += (data[index+index_match] - value_match) ** 2  # add the sqaured difference to the error_this_index
    if error_this_index < lowest_error_so_far:  # if the error from the last loop is better than the one so far we found a better match
        best_match_start_index = index+1  # keep track of where the better match is


print('reference match: ', [f'{i:.2f}' for i in match], 'sum:', sum(match), 'avg (mean):', sum(match)/30)
best_match_list = data[best_match_start_index:best_match_start_index+30]
# best_match_list2 = data[best_match_start_index:best_match_start_index+30-1]
print('best_match_found:', [f'{i:.2f}' for i in best_match_list], 'sum:', sum(best_match_list), 'avg (mean):', sum(best_match_list)/30)

x_value_for_plot = [i for i in range(30)]
plt.plot(x_value_for_plot, match, label='actual')
plt.plot(x_value_for_plot, best_match_list, label='best_match_found')
# plt.plot(x_value_for_plot, best_match_list2, label='best_match_found_2')


plt.legend()
plt.show()