import urllib.request
import xml.etree.ElementTree as etree
import re
import time
import json
import os

from pandas import to_datetime

from other_functions.common_metar_taf_elements import get_wind, convert_visibility, get_visibility, get_weather, \
    get_clouds, get_temp_and_dew_point
from other_functions.regexes import regex_pk_wind
from other_functions.time_based_functions import which_date_came_first, adjust_date_lower, move_time, get_current_time
from other_functions.misc import pad_number, convert_temp



def get_icao(ob_str):
    """
    Find the ICAO by returning the chars before the first white space
    :param ob_str: One complete METAR as a string.
    :return: str. The ICAO
    """
    return ob_str[:ob_str.find(' ')]  # Assumes the ICAO is the first word (series of chars) before the first whitespace


def download_adds_obs_as_xml(icao=None, hours_before=None, time_frame_start=None, time_frame_end=None, path=None, file_name=None, del_file_after_getting_xml_root=True, debug_print=False):
    """
    Gets the METARs from ADDS in .xml format. Just downloads it.
    :param icao: str. The ICAO of the station
    :param hours_before: int. How many hours before the current time back OBs should be retrieved from
    :param time_frame_start: dict. {'time': "2300", 'day': 31, 'month': 5, 'year': 2019} The start time of the period of the obs that are requested
    :param time_frame_end: {'time': "2000", 'day': 1, 'month': 6, 'year': 2019} The end time of the period of the obs that are requested
    :param path: string. The path of where the xml will be saved. Not the file name. Just the folder the xml will be in.
    :param file_name: string. The name of the downloaded xml file. Not the whole path, just the name of the file.
    :param del_file_after_getting_xml_root: boolean. Since root is returned that can be used instead of reading the file. So if you plan on using the returned root, then it is save to delete the file.
    :param debug_print: boolean. If true this function will print some debug statements as it goes. Silent is False
    :return: a usable version of the .xml file from ADDs
    """
    icao = icao.upper()  # just in case the use specified icao lower case.

    # first catch some errors likely to happen
    if icao is None:  # no ICAO was supplied
        raise Exception('Could not download OBs, no ICAO supplied')

    if time_frame_start is None and hours_before is None:  # if neither have these were specified then we don't know what the start time of the download is supposed ot be
        raise Exception('You need to either specify hours_before or specify time_frame_start')

    # get the URL for when the user specifies hours_before
    if hours_before:  #  If some value was specified for hours_before # get the OBs a certain number of hours from the past.
        # download previous observations from ADDS in XML format
        obs_url = 'https://aviationweather.gov/adds/dataserver_current/httpparam' \
              '?dataSource=metars&requestType=retrieve&format=xml&stationString=' \
              '{icao}&hoursBeforeNow={hours}'.format(icao=icao, hours=hours_before)

    # get start time form either the input or generate it
    if time_frame_start and hours_before is None:  # if the user specified a start time and did not specify anything for hours before
        start_year = time_frame_start['year']
        start_month = pad_number(time_frame_start['month'], 2)
        start_day = pad_number(time_frame_start['day'], 2)
        start_hours = time_frame_start['time'][:2]  # in 1245 the 12 is the hours
        start_minutes = time_frame_start['time'][2:]  # in 1245 the 45 is the minutes
        time_frame_start_formatted = f'{start_year}-{start_month}-{start_day}T{start_hours}:{start_minutes}:00'
        if debug_print:
            print('ADSS Download time frame start', time_frame_start_formatted)

    # generate the end time based on current time
    if time_frame_end is None and time_frame_start:  # the user specified a start time but not an end time, so we assume the end time is the current time
        current_time = get_current_time()
        end_year = current_time['year']
        end_month = pad_number(current_time['month'], 2)
        end_day = pad_number(current_time['day'], 2)
        end_hours = current_time['time'][:2]
        end_minutes = current_time['time'][2:]
        time_frame_end_formatted = f'{end_year}-{end_month}-{end_day}T{end_hours}:{end_minutes}:00'  # format the end they way the ADDS URL likes it.
        if debug_print:
            print('ADSS Download time frame end  ', time_frame_end_formatted)

    if hours_before is None:  # if hours_before is None then we specified at least a start time frame and can download based on that. The URL for a specified start (and end) time frame is different from the URL for hours_before
        obs_url = f'https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&startTime={time_frame_start_formatted}Z&endTime={time_frame_end_formatted}Z&stationString={icao}'
        # https://aviationweather.gov/adds/dataserver_current/httpparam?dataSource=metars&requestType=retrieve&format=xml&startTime=2019-11-10T23:00:45Z&endTime=2019-11-11T01:00:45Z&stationString=PHTO

        # file_name_time = f'sy{start_year}_sm{start_month}_sd{start_day}_st{start_time}__ey{end_year}_em{end_month}_ed{end_day}_et{end_time}'  # TODO remove this line eventaully

    if not path:  # no path was specified, generate one for the user
        path = f'obs/{icao}'

    if not file_name:  # no file name was specified, generate one for the user
        file_name = f'{icao}.xml'

    # make the folder for the icao if it doesn't already exist
    if not os.path.exists(path):
        os.makedirs(path)

    path_and_file_name = path + '/' + file_name  # combine path and file name into one string

    if debug_print:
        print(f'The URL for the downloaded obs from ADDS is:\n  {obs_url}')

    urllib.request.urlretrieve(obs_url, path_and_file_name)  # download the XML file

    tree = etree.parse(path_and_file_name)  # save the file
    root = tree.getroot()  # read the xml file into memory

    # delete the file if requested.
    if del_file_after_getting_xml_root:
        os.remove(path_and_file_name)

    return root


def obs_from_iastate(icao, hours_before_now=None, download_all=False, start_date_specified=None, end_date_specified=None, file_name=None, path=None, print_url=False):
    """
    Downloads observations from IASATE. Can download obs from a certain time frame or just a certain number of hours in the past.
    :param icao: the four letter icao of the station.
    :param hours_before_now: int. Gets obs in the time frame from current time - hours_before_now to current time. Do not use both days_before and hours_before_now
    :param days_before_now: int. How many days before now to download the obs from. Do not use both days_before and hours_before_now
    :param start_date_specified: dict. The start time period of the of the time frame over which the obs will be collected. {'time': "0100", 'day': 1, 'month': 6, 'year': 2019}
    :param end_date_specified: dict. The end time period of the of the time frame over which the obs will be collected. {'time': "0100", 'day': 1, 'month': 6, 'year': 2019}
    :param download_all: boolean. If True it will download all the OBS up to this point.
    :param file_name: string: The filename of the csv file that will be generated
    :param path. String. The path to where the .csv file should be saved.
    :param print_url: boolean. If True then the url from which obs will be downloaded will be printed to the user.
    :return: string. The path to the downloaded csv file
    """
    time_struct = time.gmtime()  # used to prevent time racing
    current_time = get_current_time(time_struct)  # current time will be used as end time and to get the start time
    tomorrow = move_time(adjust_amount={'time_hours': 0, 'time_minutes': 0, 'day': 1, 'month': 0, 'year': 0}, initial_time=current_time)  # to get today's IASTATE obs we need to request to obs of tomorrow.

    start = move_time({'time_hours': 0, 'time_minutes': 0, 'day': -3, 'month': 0, 'year': 0}, initial_time=None)  # get the start time, 3 days prior to the current time
    if download_all:  # have the start time before even there were obs just to be sure we get them all.
        start_year = 1900
        # start_year = 2015  # DEBUG, prevents waiting 10min for the download to finish
        start_month = 1
        start_day = 1
    elif hours_before_now:  # download from now to a certain number of hours in the past
        start = move_time({'time_hours': 0, 'time_minutes': 0, 'day': -3, 'month': 0, 'year': 0})  # get the start time, 3 days prior
        start_year = start['year']
        start_month = start['month']
        start_day = start['day']
    elif start_date_specified:
        start_year = start_date_specified['year']
        start_month = start_date_specified['month']
        start_day = start_date_specified['day']
    else:
        raise Exception("Invalid download time frame.")

    # end time components
    if end_date_specified:
        end_year = end_date_specified['year']
        end_month = end_date_specified['month']
        end_day = end_date_specified['day']
    else:  # just use the current time as the end time
        end_year = tomorrow['year']
        end_month = tomorrow['month']
        end_day = tomorrow['day']

    url = f'https://mesonet.agron.iastate.edu/cgi-bin/request/asos.py?station={icao}&data=all&year1={start_year}&month1={start_month}&day1={start_day}&year2={end_year}&month2={end_month}&day2={end_day}&tz=Etc%2FUTC&format=onlycomma&latlon=no&missing=M&trace=T&direct=no&report_type=2'
    if print_url:  # print the URL
        print(f'The URL from which IASTATE obs will be downloaded from is\n  {url}.')

    # path_and_file_name = f'{path}/{icao}_obs_sy{start_year}_sm{start_month}_sd{start_day}__ey{end_year}_em{end_month}_ed{end_day}'

    # path
    if path is None:  # generate the path for the user
        path = f'obs/{icao}'  # the dir where the file will be stored
    else:  # use user specified path
        pass

    # filename
    if file_name is None:  # generate the file name for the user based on the ICAO
        path_and_file_name = f'{path}/{icao}'
        path_and_file_name_csv = path_and_file_name + '.csv'  # also add the .csv to the file
    else:
        path_and_file_name = f'{path}/{file_name}'  # use the user specified file name
        path_and_file_name_csv = path_and_file_name  # this one does not need to auto add .csv. It is up to the user what the file should be called

    path_and_file_name_json = path_and_file_name + '.json'  # TODO get rid of the json stuff if it is no longer used. (I think I am going to stick with csv as the format b/c it saves disk space and reads nicely into pandas df)

    # make the folder for the icao if it doesn't already exist
    if not os.path.exists(path):
        os.makedirs(path)

    # download the file
    urllib.request.urlretrieve(url, path_and_file_name_csv)

    # make a json version of the file and save it
    # obs_to_json(get_obs_from_csv(path_and_file_name_csv), path_and_file_name_json)

    # debug
    # print(f'2 files successfully created.\n{path_and_file_name_csv}\n{path_and_file_name_json}')
    print(f'Observations for {icao} successfully downloaded and saved to:\n{path_and_file_name_csv}')

    return path_and_file_name_csv  # no errors happened if we got to this point


def get_raw_obs_from_xml(xml_root, reverse_order=False):
    """
    Converts the root gotten from the downloaded .xml into a list of ob.
    :param xml_root: the xml root.
    :param reverse_order: if True oldest ob is first, if False newest ob is first
    :return: list. A list of the obs. Each complete METAR is one str.
    """
    obs = []  # a dict of all the ob where key is the ob number and the value is the entire ob as a list of the individual strings
    number_of_obs = int(xml_root[6].attrib.get("num_results"))  # how many obs there are total in the xml
    # print(number_of_obs)

    for index in range(number_of_obs):  # for as many ob there are
        # print(count, number_of_obs)
        if reverse_order:
            index = number_of_obs - index - 1  # -1 is needed because number of ob starts at 1, index starts at 0
        else:
            pass
        ob_str = xml_root[6][index].find("raw_text").text  # find the raw ob as one string

        obs.append(ob_str)  # = ob_lst  # add the ob number and ob list of strings to the dict
    return obs  # return the list of obs


def get_alstg(ob_str, return_format='input_str'):
    """
    Get the ALSTG from METAR.
    :param ob_str: input_str One complete METAR as a string.
    :param return_format: what type to return as either a string or an int
    :return: str or int return the altimeter setting. Either '2992' or 2992
    """
    # ob_str: input_str. The complete METAR as a string
    # Finds the altimeter setting and returns it as an int ex 2992
    regex = r'A(\d{4}) '
    alstg = re.search(regex, ob_str).group(1)
    if return_format == 'input_str':
        return alstg
    else:
        return int(alstg)


def get_date_and_time_from_metar(ob_str):
    """
    Gives an exact date for a METAR based on the day and time_zulu.
    :param ob_str: One complete METAR as a string.
    :return: dict containing the time in zulu (str) , day (int), month (int), year (int)
    """
    regex = r'(\d{2})(\d{4})(?=Z)'
    day = int(re.search(regex, ob_str).group(1))
    time_zulu = re.search(regex, ob_str).group(2)

    # month and year are not encoded into the METAR. METAR only shows which day of the month it is and the time zulu
    # use gmtime because it is zulutime, UTC. Month and year might wrong. Month and year will be adjusted before returning.
    gm_time = time.gmtime()
    month = gm_time[1]  # the current month, in int form,
    year = gm_time[0]  # the current year, in int form

    adjusted_date = adjust_date_lower(time_zulu, day, month, year)  # if the current day of the month is lower than the one in the ob the month must be 1 lower
    return adjusted_date  # returns a dict of the day month and zulu time ex. {'time_zulu': '1050', 'day': 25, 'month': 5, 'year': 2019}


def get_raw_obs_from_txt(txt_file, revere_order=True):
    """
    :param: txt_file str. path and file name to the obs. Every index should include only one OB.
    :param: reverse_order. bool True or False. True if newest ob should be first, False if oldest ob should be first.
    This is supposed to be used when ADDS is down and the user has a alternative source they get obs from.
    ADDS only lets you got about 360hour into the past and has the newest ob on top. obs for json wants the obs oldest first
    :return a list of obs. Note: in order to be used for raw_obs_to_json_friendly_format(obs) as the obs input it
    needs to be in the order of newest ob first.
    """
    obs_out = []

    with open(txt_file) as obs_in:
        obs_in = obs_in.readlines()

        # first determine the order of the obs if it is oldest top or newest top. ADDS is newest first(top).
        date_line_1 = get_date_and_time_from_metar(obs_in[0])
        date_line_2 = get_date_and_time_from_metar(obs_in[1])
        order_obs = which_date_came_first(date_line_1, date_line_2)

        if order_obs == 1:  # newest top
            # determine the number of lines in the file so we can start at that number and -1 our way up
            obs_in = obs_in[::-1]
        elif order_obs == 2:  # oldest on top, the order is correct
            pass
        else:  # order cannot be determined, this shouldn't really ever be triggered
            pass

        for count, line in enumerate(obs_in):
            obs_out.append(line.strip('\n'))  # count starts at 0 so +1 to get the name. Add the name and bo to the dict.

        return obs_out


def get_obs_from_csv(csv_file):
    """
    Iowa State University (IASTATE) allows for the download of METARS from as far back as 1928. It let's you download these METARS
    as a .csv file. This function can read that csv file and prepare for use in a database.
    :param csv_file: string. The path to the csv file. Extension does not actually have to be csv
    :return: dictionary. The .json version of the csv file
    """
    obs_out = {}  # the json version of the csv file
    with open(csv_file) as obs_in:
        obs_in = obs_in.readlines()

        first_line_keys = obs_in[0].split(',')  # the first line contain the keys, all the other lines are just values
        # print(first_line_keys)

        # icao
        try:
            icao_index = first_line_keys.index('station')
        except ValueError:
            raise ValueError("The icao is not included in the csv file. The first line of the csv file should have a key 'station'")

        icao = obs_in[1].split(',')[icao_index]

        # set up the base dict with just the icao in it for now
        obs_out['meta'] = {
            'icao': icao
        }

        obs_out['data'] = {}  # set up the dict that will contain all the ob

        # full date and time
        try:
            date_time_index = first_line_keys.index('valid')
        except ValueError:
            raise ValueError("No valid time found of the observation. The first line of the csv file should have a key 'valid'")

        # wind direction
        try:
            wind_dir_index = first_line_keys.index('drct')
        except ValueError:
            raise ValueError("No wind direction found of the observation. The first line of the csv file should have a key 'drct'")

        # wind speed
        try:
            wind_speed_index = first_line_keys.index('sknt')
        except ValueError:
            raise ValueError("No wind speed found of the observation. The first line of the csv file should have a key 'sknt'")

        # wind gust
        try:
            wind_gust_index = first_line_keys.index('gust')
        except ValueError:
            raise ValueError("No wind speed found of the observation. The first line of the csv file should have a key 'gust'")

        # visibility
        try:
            vis_index = first_line_keys.index('vsby')
        except ValueError:
            raise ValueError("No visibility found of the observation. The first line of the csv file should have a key 'vsby'")

        # weather
        try:
            wx_index = first_line_keys.index('wxcodes')
        except ValueError:
            raise ValueError("No weather found of the observation. The first line of the csv file should have a key 'wxcodes'")

        # sky level 1 coverage and height
        try:
            sky_level_1_cov_index = first_line_keys.index('skyc1')
            sky_level_1_height_index = first_line_keys.index('skyl1')
        except ValueError:
            raise ValueError("No cloud layer 1 height and or coverage found of the observation. The first line of the csv file should have a key 'skyc1' and another key 'skyl1'")

        # sky level 2 coverage and height
        try:
            sky_level_2_cov_index = first_line_keys.index('skyc2')
            sky_level_2_height_index = first_line_keys.index('skyl2')
        except ValueError:
            raise ValueError("No cloud layer 1 height and or coverage found of the observation. The first line of the csv file should have a key 'skyc2' and another key 'skyl2'")

        # sky level 3 coverage and height
        try:
            sky_level_3_cov_index = first_line_keys.index('skyc3')
            sky_level_3_height_index = first_line_keys.index('skyl3')
        except ValueError:
            raise ValueError("No cloud layer 3 height and or coverage found of the observation. The first line of the csv file should have a key 'skyc3' and another key 'skyl3'")

        # sky level 4 coverage and height
        try:
            sky_level_4_cov_index = first_line_keys.index('skyc4')
            sky_level_4_height_index = first_line_keys.index('skyl4')
        except ValueError:
            raise ValueError("No cloud layer 4 height and or coverage found of the observation. The first line of the csv file should have a key 'skyc4' and another key 'skyl4'")

        # temp
        try:
            temperature_index = first_line_keys.index('tmpf')
        except ValueError:
            raise ValueError("No temperature found of the observation. The first line of the csv file should have a key 'tmpf'")

        # dew point
        try:
            dew_point_index = first_line_keys.index('dwpf')
        except ValueError:
            raise ValueError("No dew point found of the observation. The first line of the csv file should have a key 'dwpf'")

        # alitemeter setting
        try:
            alstg_index = first_line_keys.index('alti')
        except ValueError:
            raise ValueError("No altimeter setting found of the observation. The first line of the csv file should have a key 'alti'")

        # peak wind
        try:
            pk_wind_time_index = first_line_keys.index('peak_wind_time')
            pk_wind_dir_index = first_line_keys.index('peak_wind_time')
            pk_wind_gust_index = first_line_keys.index('peak_wind_gust')
        except ValueError:
            raise ValueError("No peak wind found of the observation. The first line of the csv file should have a key 'peak_wind_time', 'peak_wind_time', 'peak_wind_gust'")

        # print(len(obs_in[0].split(',')))
        count = 0  # not using enumerate or range because sometimes count will be +2 (instead of the usual +1) after a loop completes
        for index, line in enumerate(obs_in[1:]):
            tmp_dict = {}  # a dict for just this line
            line = line.split(',')  # split the string into list
            # print(line[time_index])

            # get the individual date and time elements
            year = line[date_time_index][:4]
            month = line[date_time_index][5:7]
            day = line[date_time_index][8:10]
            hour = line[date_time_index][11:13]
            minute = line[date_time_index][14:16]

            # wind direction
            try:
                wind_dir = int(float(line[wind_dir_index]))
            except ValueError:
                wind_dir = line[wind_dir_index]

            # wind speed
            try:
                wind_speed = int(float(line[wind_speed_index]))
            except ValueError:
                wind_speed = line[wind_speed_index]

            # wind gust
            try:
                wind_gust = int(float(line[wind_gust_index]))
            except ValueError:
                wind_gust = line[wind_gust_index]

            # visibility
            try:
                vis = float(line[vis_index])
            except ValueError:
                vis = line[vis_index]

            # sky condition / clouds
            sky_level_1_cov = line[sky_level_1_cov_index]
            sky_level_1_height = line[sky_level_1_height_index]
            sky_level_1_height = int(float(sky_level_1_height)) if sky_level_1_height != 'M' else sky_level_1_height

            sky_level_2_cov = line[sky_level_2_cov_index]
            sky_level_2_height = line[sky_level_2_height_index]
            sky_level_2_height = int(float(sky_level_2_height)) if sky_level_2_height != 'M' else sky_level_2_height

            sky_level_3_cov = line[sky_level_3_cov_index]
            sky_level_3_height = line[sky_level_3_height_index]
            sky_level_3_height = int(float(sky_level_3_height)) if sky_level_3_height != 'M' else sky_level_3_height

            sky_level_4_cov = line[sky_level_4_cov_index]
            sky_level_4_height = line[sky_level_4_height_index]
            sky_level_4_height = int(float(sky_level_4_height)) if sky_level_4_height != 'M' else sky_level_4_height
            # convert the height to ints if possible

            # temperature
            try:
                temperature = convert_temp(float(line[temperature_index]))
            except ValueError:
                temperature = line[temperature_index]

            # dew point
            try:
                dew_point = convert_temp(float(line[dew_point_index]))
            except ValueError:
                dew_point = line[dew_point_index]

            # alstg
            try:  # try to convert to a float
                alstg = float(line[alstg_index])
            except ValueError:  # will throw and error if METAR is missing alstg
                alstg = line[alstg_index]

            # weather
            weather = get_weather(line[wx_index], filter_icao=False, filter_rmks=False)
            # print(wx)

            # peak wind direction
            try:
                pk_wind_dir = int(float(line[pk_wind_dir_index]))
            except ValueError:
                pk_wind_dir = line[pk_wind_dir_index]

            # peak wind speed
            try:
                pk_wind_gust = int(float(line[pk_wind_gust_index]))
            except ValueError:
                pk_wind_gust = line[pk_wind_gust_index]

            # peak wind time
            pk_wind_year = line[pk_wind_time_index][:4]
            pk_wind_month = line[pk_wind_time_index][5:7]
            pk_wind_day = line[pk_wind_time_index][8:10]
            pk_wind_hour = line[pk_wind_time_index][11:13]
            pk_wind_minute = line[pk_wind_time_index][14:16]

            # PK WND always occur before the main OB is released, so add a separate entry for PK WND before adding the main ob
            if pk_wind_dir != 'M':  #  add an entry for PK WND only if there is a PK WND
                obs_out['data'][f'{count}'] = {
                    'is_peak_wind': True,
                    'date_time': {
                        'year': pk_wind_year,
                        'month': pk_wind_month,
                        'day': pk_wind_day,
                        'hour': pk_wind_hour,
                        'minute': pk_wind_minute
                    },
                    'wind': {
                        'pk_wind_dir': pk_wind_dir,
                        'pk_wind_gust': pk_wind_gust,
                    }
                }

                count += 1

            # add the ob to the main dict. Does not do PK WND
            obs_out['data'][f'{count}'] = {
                'is_peak_wind': False,
                'date_time': {
                    'year': year,
                    'month': month,
                    'day': day,
                    'hour': hour,
                    'minute': minute
                },
                'wind': {
                    'wind_dir': wind_dir,
                    'wind_speed': wind_speed,
                    'wind_gust': wind_gust
                },
                'visibility': vis,
                'weather': weather,
                'clouds': {
                    'layer_1_cov': sky_level_1_cov,
                    'layer_1_height': sky_level_1_height,
                    'layer_2_cov': sky_level_2_cov,
                    'layer_2_height': sky_level_2_height,
                    'layer_3_cov': sky_level_3_cov,
                    'layer_3_height': sky_level_3_height,
                    'layer_4_cov': sky_level_4_cov,
                    'layer_4_height': sky_level_4_height,
                },
                'temperature': temperature,
                'dew_point': dew_point,
                'alstg': alstg
            }
            count += 1

    return obs_out


            # print(f'K{icao} {year}{month}{day}{hour}{minute}Z {wind_dir}{wind_speed}KT {vis}SM {sky_level_1_cov}{sky_level_1_height} {sky_level_2_cov}{sky_level_2_height} {sky_level_3_cov}{sky_level_3_height} {sky_level_4_cov}{sky_level_4_height} {temperature}/{dew_point} A{(alstg)}')


def set_ob_date_time(ob_dict, time_zulu, day, month, year):
    """
    Takes in an ob_dict and gives it a complete date and time and returns it.
    :param ob_dict: a dictionary of the ob ex. ob = {'ICAO': 'KBAD', '1': {ob_stuff_blabla}}
    :param time_zulu: str. time in zulu ex. 1302, 0043
    :param day: int ex. 3, 28, 12
    :param month: int ex. 1,5, 12
    :param year: int ex 2019 2025
    :return: a dict  with the time in zulu (str) , day, month and year added as int
    """
    ob_dict['time'] = dict(issue_time=time_zulu, issue_day=day, issue_month=month, issue_year=year)


def raw_obs_to_json_friendly_format(obs):
    """
    Converts the raw obs into a dictionary of of obs with the individual elements broken down. Processes the obs input from the back forward.
    PK WND is also gotten inside this function
    :param obs: lst of str. List of of obs where each entire METAR is one str. Note that the order needs to be most recent ob first
    :return: a dict of obs. The order of the obs in the dict will be oldest first, reverse of the input. ICAO is added to the beginning of file
    """
    obs_for_json = {}  # set up the empty dict

    # add a meta group that has the icao and how many obs are in the JSON
    obs_for_json['meta'] = {}
    obs_for_json['meta']['icao'] = get_icao(obs[0])  # set up the base dictionary that will include all the obs, to start put the ICAO in it

    number_of_obs = int(len(obs))  # how many obs are in the xml file
    number_of_obs_left = number_of_obs - 1  # how many obs still need ot be processed
    obs_processed = 0  # lets also use this as the ob_names number, +1 after every loop and +1 for every PK WND remark

    obs_for_json['data'] = {}  # set up the empty dict where all the obs will go, does not include meta

    wind_pks_already_processed = []  # lst of tuples of the pk wnd date time. Use dot avoid adding the same pk wnd twice
    while number_of_obs_left > -1:  # while there are still unprocessed obs in the obs dict
        ob_temporary = {}  # set up the empty tmp dict
        ob_name = str(obs_processed)  # the name of the ob as it will appear in the json file ex 0, 2, 5, 34

        ob_str = obs[number_of_obs_left]  # get the raw METAR string form the obs dict

        # get date and time, time in zulu, day, month, year. Requires not adjusting.
        date_time_group_adjusted = get_date_and_time_from_metar(ob_str)
        time_z = date_time_group_adjusted['time']
        day = int(date_time_group_adjusted['day'])
        month = int(date_time_group_adjusted['month'])
        year = date_time_group_adjusted['year']

        set_ob_date_time(ob_temporary, time_z, day, month, year)

        # here we just check if there is a PK WND remark, if there is we add it as a separate ob at the end of the loop
        # added at end because we can only add one ob_temporary at a time and this PK WND remarks one will be it'taf_as_one_str own ob
        # in the json file
        try:  # separate the PK WND remark into it'taf_as_one_str elements
            pk_wind_regex = r'(?<=PK WND )(?P<wind_dir>\d{3})(?P<wind_speed>\d{2})(?:/)(?P<time>\d{2,4})'
            pk_wind_dir = re.search(pk_wind_regex, ob_str).group('wind_dir')
            pk_wind_speed = re.search(pk_wind_regex, ob_str).group('wind_speed')
            pk_wind_time = re.search(pk_wind_regex, ob_str).group('time')

            # get the date, for now copy the one from the ob, it will be adjusted a few lines below.
            pk_wnd_day = day
            pk_wnd_month = month
            pk_wnd_year = year

            if pk_wind_time not in wind_pks_already_processed:  # if we have not processed this ob
                # some sites report the PK WND remark time as 12042/12, where the time is 12 after the hour of the ob so if
                # there OB with a time of 1055Z and it has that above pk wnd remark it means that the pk wnd happened at 1012Z
                if len(pk_wind_time) == 2:
                    pk_wind_time = time_z[:2] + pk_wind_time

                # check that the PK WND did not happen the day prior
                if pk_wind_time > time_z:  # if pk wind time is larger than ob time it was last days pk wind. Would happen with a 0055z ob that has a pk wind of 2359z
                    pk_wnd_day -= 1  # go back a day

                    # if by some chance we went back 1 day into the last month and or year
                    adjusted_pk_wnd_date = adjust_date_lower(pk_wind_time, pk_wnd_day, pk_wnd_month, pk_wnd_year)
                    pk_wnd_day = adjusted_pk_wnd_date['day']
                    pk_wnd_month = adjusted_pk_wnd_date['month']
                    pk_wnd_year = adjusted_pk_wnd_date['year']

                ob_temporary['time']['day'] = pk_wnd_day

                # add the date time group to the ob using time the PK WND
                set_ob_date_time(ob_temporary, pk_wind_time, pk_wnd_day, pk_wnd_month, pk_wnd_year)

                # add the PK WND broken up into its elements to the ob
                ob_temporary['peak_wind'] = {
                    'pk_wind_dir': pk_wind_dir,  # not an int b/c we want 030 for 30deg no 30
                    'pk_wind_speed': int(pk_wind_speed),
                }

                # Create a OB for the PK WND remark
                obs_for_json['data'][ob_name] = ob_temporary

                # now we gotta restore the ob_temporary , b/c I want to keep the date time group I will just delete the peak
                # wind remark, also reset the ob time.
                ob_temporary = {}  # reset the temporary ob so we can re-create the basic for the main (non PKL WND) part of the ob with a new name
                ob_temporary['weather'] = {}  # set up the empty weather dict
                wind_pks_already_processed.append((pk_wnd_year, pk_wnd_month, pk_wnd_day, pk_wind_time))  # add the just processed PK WND to those already processed
                obs_processed += 1  # the PK WND ob just got processed so the next ob will be one higher ob_number_#
                ob_name = str(obs_processed)  # the name of the next ob, has to be +1 because count
                # ob_temporary['time'] = set_ob_date_time(ob_temporary, time_z, day, month, year)  # Set the ob time TODO remove 'rr'
                set_ob_date_time(ob_temporary, time_z, day, month, year)  # Set the ob time TODO remove 'rr'

            else:  # if PK WND has already been processed
                pass

        except AttributeError:  # if there is no PK WND remark it will throw AttributeError
            pass

        # wind, does not get PK WND
        ob_temporary['wind'] = get_wind(ob_str)

        # get visibility in meters
        ob_temporary['visibility'] = convert_visibility(get_visibility(ob_str))

        # weather
        ob_temporary['weather'] = get_weather(ob_str)

        # sky condition aka clouds
        ob_temporary['clouds'] = get_clouds(ob_str)

        # temperature and dew point
        temp_and_dew_point = get_temp_and_dew_point(ob_str, get_exact=False)
        ob_temporary['temperature'] = temp_and_dew_point['temp']
        ob_temporary['dew_point'] = temp_and_dew_point['dew_point']

        # ALSTG
        alstg = get_alstg(ob_str, return_format='int')
        ob_temporary['alstg'] = alstg

        obs_for_json['data'][ob_name] = ob_temporary  # add the temporary ob to the dict for json of obs

        obs_processed += 1  # at the end of the loop 1 ob is processed, or 2 if there was a PK WND remark
        number_of_obs_left -= 1  # at the end of the loop there is one less ob to process, this car also control when to stop the loop.

        # save the number of obs processed to the meta
        obs_for_json['meta']['number_of_obs'] = obs_processed

        # add to the meta the time frame to OBs cover
        obs_for_json['meta']['start_year'] = obs_for_json['data']['0']['time']['issue_year']
        obs_for_json['meta']['start_month'] = obs_for_json['data']['0']['time']['issue_month']
        obs_for_json['meta']['start_day'] = obs_for_json['data']['0']['time']['issue_day']
        obs_for_json['meta']['start_time'] = obs_for_json['data']['0']['time']['issue_time']

        obs_for_json['meta']['end_year'] = obs_for_json['data'][ob_name]['time']['issue_year']
        obs_for_json['meta']['end_month'] = obs_for_json['data'][ob_name]['time']['issue_month']
        obs_for_json['meta']['end_day'] = obs_for_json['data'][ob_name]['time']['issue_day']
        obs_for_json['meta']['end_time'] = obs_for_json['data'][ob_name]['time']['issue_time']


    return obs_for_json  # returns a dict of all the obs


def obs_to_json(obs, file_name=None):
    """
    Writes obs into a json file.
    :param obs: dict. A dictionary of ob where the key is the name like ob_name 0 and the value is the ob broken into individual elements
    :param file_name: str. Name of the file to save to. Must be .json
    :return: str. The file name the obs was saved to.
    """
    if file_name is None:  # no file name is given, generate one
        icao = obs['meta']['icao']
        start_year = obs['meta']['start_year']
        start_month = obs['meta']['start_month']
        start_day = obs['meta']['start_day']
        start_time = obs['meta']['start_time']

        end_year = obs['meta']['end_year']
        end_month = obs['meta']['end_month']
        end_day = obs['meta']['end_day']
        end_time = obs['meta']['end_time']
        file_name = f'temp/OBs_{icao}_sy{start_year}_sm{start_month}_sd{start_day}_st{start_time}__ey{end_year}_em{end_month}_ed{end_day}_et{end_time}.json'

    else:
        pass  # file name is given


    with open(file_name, 'w') as outfile:
        json.dump(obs, outfile, indent=4)

    return file_name  # filename is used later to process the correct file


def get_pk_wnd_from_ob(ob_str, ob_date_time, return_date_time_format='dt'):
    """
    Get's the components of the peak wind from an ob. Needs the ob and the ob date and time as input because PK WND is not encoded into it's own ob and as such does not specify the year, month or day that it occured, that has to be inferred from the
    date and time of the observation.
    :param ob_str: The entire ob as a string. KBVO 120353Z AUTO 35010G25KT 10SM CLR M06/M13 A3070 RMK AO2 PK WND 36027/0343 SLP402 T10611128
    :param ob_date_time: string or pd.datetime. A string of the time like '2019-05-11T02:34' or a pandas datetime object like pd.to_datetime('2019-05-11T02:34'). The time of the peak wind depends on the time of the main ob part.
    :param return_date_time_format. Enter either 'dt' or 'dict'. The return format for the date and time of the peak wind. Can be either a pandas datetime object or a dict.
    if 'dict' enter something like {'year': 2019, 'month': 5, 'day': 11, 'hour': 2, 'minute': 34} if dt enter a pd.datetime object. Ex. pd.datetime.utcnow() or pd.to_datetime('2019-05-11T02:34')
    :return: dict. A dictionary of the peak wind components. {'dir': '220', 'speed': 14, 'date_time': {'year': 2019, 'month': 5, 'day': 11, 'time': "0234"}}
    """
    if 'PK WND' not in ob_str:  # there is no PK WND remark -> The ob does not have a PK WND. Just return None
        return None

    if type(ob_date_time) == str:  # ob date_time will need to be converted to a pd.datetime if it is given as a string
        ob_date_time = to_datetime(ob_date_time)  # convert to pandas datetime object

    # We only get to this point if there is a PK WND remark
    year = ob_date_time.year
    month = ob_date_time.month
    day = ob_date_time.day

    pk_wind_dir = re.search(regex_pk_wind, ob_str).group('wind_dir')
    pk_wind_speed = re.search(regex_pk_wind, ob_str).group('wind_speed')
    pk_wind_time = re.search(regex_pk_wind, ob_str).group('time')

    # when the hour of the PK WND is the same hour as the OB, then the hour will not be included, Ex. KBAD 200256Z ... PK WND 05021/27 This means the PK WND happened at 0227Z. We need to at the 02 back
    if len(pk_wind_time) == 2:  # only the minutes are encdoded, add the hour back
        pk_wind_time = f' {pad_number(ob_date_time.hour, 2)}{pk_wind_time}'

    pk_wind_date_time = adjust_date_lower(pk_wind_time, day, month, year)
    pk_wnd_year = pk_wind_date_time['year']
    pk_wnd_month = pk_wind_date_time['month']
    pk_wnd_day = pk_wind_date_time['day']
    pk_wnd_time = pk_wind_date_time['time']


    if return_date_time_format == 'dict':  # create the date_time var in dict format
        date_time = {'year': year, 'month': month, 'day': day, 'time': pk_wind_date_time}
    elif return_date_time_format == 'dt':  # create the date_time var in pd.datetime format
        date_time = to_datetime(f'{pk_wnd_year}-{pk_wnd_month}-{pk_wnd_day}T{pk_wnd_time}')

    return {'dir': pk_wind_dir, 'speed': pk_wind_speed, 'date_time': date_time}  # return the PK WND info and the date_time of it


if __name__ == '__main__':
    import pandas as pd
    ob_str = 'KHST 200256Z AUTO 04017G22KT 10SM SCT017 BKN031 OVC040 20/18 A3013 RMK AO2 PK WND 05026/27 RAE0157DZB0157E13 SLP206 P0000 60003 T02030179 52012'
    ob_date_time = pd.to_datetime('2019-12-20T02:56:00Z')
    get_pk_wnd_from_ob(ob_str, ob_date_time, return_date_time_format='dt')
    # obs = download_adds_obs_as_xml(icao='KTUL', time_frame_start={'time': "0000", 'day': 20, 'month': 8, 'year': 2019}, time_frame_end={'time': "1200", 'day': 21, 'month': 8, 'year': 2019})  # download the xml
    # obs = download_adds_obs_as_xml(icao='KJFK', hours_before=50)
    # obs = get_raw_obs_from_xml(obs, reverse_order=False)
    # obs_for_json = raw_obs_to_json_friendly_format(obs)
    # time.sleep(0.1)
    # pprint(obs_for_json)
    # obs_to_json(obs_for_json)
    # test_obs_csv = get_obs_from_csv('old_metar/test_data.txt')
    # test_obs_csv = get_obs_from_csv('old_metar/KDFW.txt')
    # obs_to_json(test_obs_csv, 'temp/AAAA.json')
    # print('done')
    # obs_from_iastate('KSFO', download_all=True)
    # print('done')
    # print(get_raw_obs_from_txt('observations.txt'))
    # obs2 = get_raw_obs_from_txt('observations.txt')
    # obs_for_json_2 = raw_obs_to_json_friendly_format(obs2)
    # obs_to_json(obs_for_json_2, file_name='obs2.json')
    start_time = {'time': '0400', 'day': 7, 'month': 11, 'year': 2019}
    root = download_adds_obs_as_xml(icao='KSGF', time_frame_start=start_time, del_file_after_getting_xml_root=True)

