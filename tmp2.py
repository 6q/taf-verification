import pandas as pd
import numpy as np
from other_functions.csv_tools import get_wind_dir_diff


def convert_num(num):
    """
    Convert a number.
    :param num: int. The input number
    :return: int. The processed number
    """
    return (num * 2) - 3


def fix_wind_dir(df):
    """
    This function adds a seconds column for wind direction. It interpolates missing wind direction intelligently. Ex 340 M 020 -> 340 360 020
    :param df: dataframe. A pandas dataframe that has the csv file of obs read into it.
    :return: df. The pandas dataframe with the obs read into it.
    """
    row_index = 0
    while row_index <= len(df) - 1:  # start at row 0. We have to do -1 to stop at the last row and not read an index that dosn't exists

        skip_this_ob = True  # default value. True when the ob has a real wind dir Ex. 230, 0, 360, etc.
        try:
            start_point_wind_dir = int(df.at[row_index, 'wind_direction'])
            # break  # if we got here then the prev row has a good wind dir
        except ValueError:  # converting 'VRB' or np.nan to int causes a ValueError
            skip_this_ob = False  # when ob has np.nan or 'VRB' or 'M' for the wind dir
            pass  # skip that ob b/c it doesn't have wind direction, go onto the next one

        if skip_this_ob is False:  # if the row has VRB as the wind direction
            start_point_date_time = pd.to_datetime(df.at[row_index, 'date_time'])  # start point of this chunk that has VRB wind. A chunk is consecutive obs that all have VRB, a chunk can be a little as just 1 ob (one row)

            # find the previous row that has wind direction
            for prev_index in range(row_index-1, 0, -1):  # for the previous obs
                try:
                    start_point_wind_dir = int(df.at[prev_index, 'wind_direction'])
                    start_point_date_time = pd.to_datetime(df.at[prev_index, 'date_time'])
                    break  # if we got here then the prev row has a good wind dir
                except ValueError:  # converting 'VRB' or np.nan to int causes a ValueError
                    pass  # skip that ob b/c it doesn't have wind direction, go onto the next one
            else:  # there is not a single ob in front left, just go with 180
                start_point_wind_dir = 180
                # no need to define start_point_date_time, it is already defined above the for loop

            # find the next row that has a valid wind direction
            chunk_size = 1  # how many consecutive obs don't have a good wind dir, we will fix one chunk at a time
            bad_indexes = [row_index]  # a list of all the rows that need to have their wind dir fixed.
            for next_index in range(1, len(df) - row_index):  # for the rest of the df
                try:  # wind the next obs that has an actual wind directon
                    end_point_wind_dir = int(df.at[row_index + next_index, 'wind_direction'])
                    end_point_date_time = pd.to_datetime(df.at[row_index + next_index, 'date_time'])
                    break  # if we got here then the next row has a good wind dir
                except ValueError:  # converting 'VRB' or np.nan to int causes a ValueError
                    chunk_size += 1  # another bad ob, the chunk grows by one
                    bad_indexes.append(row_index + next_index)  # found another row that doesn't have a valid wind dir, add it to the list of indexes that will be fixed
                    pass  # skip that ob b/c it doesn't have wind direction, go onto the next one
            else:  # if we are at the end of the df and all the last obs are VRB we will want to fill it with some number, lets just go with whatever the wind dir was before the VRB
                end_point_wind_dir = start_point_wind_dir
                end_point_date_time = pd.to_datetime(df.at[len(df)-1, 'date_time'])  # use the last ob's date_time for the end point if no next_ob has a valid wind dir  # TODO maybe go 1 hour after last ob time?

            # now fix the VRB winds
            chunk_length_seconds = (end_point_date_time - start_point_date_time)/np.timedelta64(1, 's')  # the length of the chunk in seconds

            wind_dir_change_amount = get_wind_dir_diff(start_point_wind_dir, end_point_wind_dir)  # The difference of the start and end wind dir
            for next_index_new in range(1, chunk_size+1):  # now set the VRB wind directions
                this_time_difference = (pd.to_datetime(df.at[row_index+next_index_new-1, 'date_time']) - start_point_date_time) / np.timedelta64(1, 's')  # get the time difference in seconds from the most previous working ob to this ob
                time_difference_ratio = this_time_difference / chunk_length_seconds
                new_wind_dir = add_or_subtract_wind_dir(start_point_wind_dir, (wind_dir_change_amount * time_difference_ratio))  # set the wind dir

                new_wind_dir = int(round(new_wind_dir, -1))  # round ot the nearest 10s, Ex 316 -> 320

                if new_wind_dir == 0:  # to be consistent with obs we should convert 0 to 360
                    new_wind_dir = 360
                df.at[row_index+next_index_new-1, 'new_wind_direction'] = new_wind_dir

            # print(f'start_point_wind_dir {start_point_wind_dir}'
            #       f'\nstart_point_date_time {start_point_date_time}'
            #       f'\nend_point_wind_dir {end_point_wind_dir}'
            #       f'\nend_point_date_time {end_point_date_time}'
            #       f'\nchunk size {chunk_size}'
            #       f'\ntime difference {chunk_length_seconds}'
            #       f'\nwind dir difference {wind_dir_change_amount}')
            #
            # print('= ' * 40)

            row_index += chunk_size  # move to the next wind dir that might be broken

        else:  # the ob is good, go to the next one
            row_index += 1
    df['new_wind_direction'] = df['new_wind_direction'].astype('Int16')  # convert column from float to int16
    return df


def add_or_subtract_wind_dir(wind_dir, change_amount):
    """
    Get a new wind direction based on changing the input wind direction. To subtract change amount should be negative.
    :param wind_dir: int. Between 0 and 360, inclusive
    :param change_amount: int. Between -180 and 180, inclusive
    :return: int. Between 0 and 360, inclusive
    """
    wind_dir += change_amount

    if wind_dir > 360:
        wind_dir -= 360
    elif wind_dir < 0:
        wind_dir += 360

    return wind_dir


def set_valid_date_time_as_index(df):
    """
    Takes as input a pandas df that has a .csv of ob loaded into it and set the 'valid' column to the index. Sets the 'valid' to a column of a pd.datetime objects before setting it as the index.
    :param df: dataframe. A pandas dataframe with a csv file loaded into it.
    :return: dataframe. The pandas dataframe with the 'valid' column set as the index.
    """
    df['valid'] = pd.to_datetime(df['valid'])  # convert 'valid' column to pd.datetime objects
    df = df.set_index('valid')  # set the 'valid' as index
    # df.set_index('valid')  # not using this method because it won't work properly when saving the df to csv with index='valid'

    return df


if __name__ == '__main__':
    # print(add_or_subtract_wind_dir(0, 180))
    import pandas as pd
    df = pd.read_csv('test.csv')
    # df = pd.read_csv('/home/julius/Documents/projects/taf-verification/obs/KSFO/KSFO.csv')
    df['valid'] = pd.to_datetime(df['valid'])  # convert 'valid' column to pd.datetime objects
    df = df.set_index('valid')  # set the 'valid' as index

    column_index_drct = df.columns.get_loc('value')
    df['value2'] = np.nan
    column_index_val2 = df.columns.get_loc('value2')
    for row_index in range(len(df)):
        # df.iloc[row_index]['drct']
        df.iat[row_index, column_index_val2] = 55

        # df.iloc[row_index]['value2'] = 555
        # df.set_value(index=row_index, col=column_index_val2, value=555)
        # df.at[row_index]


    # df.at[5, 'wind_directions'] = 233
    # df = fix_wind_dir(df)

    # t1 = pd.to_datetime('2019-12-03 02:53:12')
    # t2 = pd.to_datetime('2019-12-03 03:19:44')

    # print((t2-t1)/np.timedelta64(1, 's'))

    df.to_csv('test3.csv')
    # df.to_csv('/home/julius/Documents/projects/taf-verification/obs/KSFO/KSFO.csv')
    print(df)
