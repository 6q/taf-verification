from unittest import TestCase
import read_ob_and_taf_json

class TestAllWeather(TestCase):
    def test_are_weather_entries_same_time(self):
        # all same
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019

        entry_1.month = 11
        entry_2.month = 11
        entry_3.month = 11

        entry_1.day = 24
        entry_2.day = 24
        entry_3.day = 24

        entry_1.time = "1304"
        entry_2.time = "1304"
        entry_3.time = "1304"

        all_weather = read_ob_and_taf_json.AllWeather()

        self.assertEqual(all_weather.are_weather_entries_same_time(entry_1, entry_2, entry_3), True)

        # one of them has a different year
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2018
        entry_2.year = 2019
        entry_3.year = 2019

        entry_1.month = 11
        entry_2.month = 11
        entry_3.month = 11

        entry_1.day = 24
        entry_2.day = 24
        entry_3.day = 24

        entry_1.time = "1304"
        entry_2.time = "1304"
        entry_3.time = "1304"

        all_weather = read_ob_and_taf_json.AllWeather()

        self.assertEqual(all_weather.are_weather_entries_same_time(entry_1, entry_2, entry_3), False)

        # one of them has a different time
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019

        entry_1.month = 11
        entry_2.month = 11
        entry_3.month = 11

        entry_1.day = 24
        entry_2.day = 24
        entry_3.day = 24

        entry_1.time = "1304"
        entry_2.time = "1304"
        entry_3.time = "1300"

        all_weather = read_ob_and_taf_json.AllWeather()

        self.assertEqual(all_weather.are_weather_entries_same_time(entry_1, entry_2, entry_3), False)

        # one of them has a different month, day
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019

        entry_1.month = 11
        entry_2.month = 12
        entry_3.month = 11

        entry_1.day = 24
        entry_2.day = 20
        entry_3.day = 24

        entry_1.time = "1304"
        entry_2.time = "1304"
        entry_3.time = "1304"

        all_weather = read_ob_and_taf_json.AllWeather()

        self.assertEqual(all_weather.are_weather_entries_same_time(entry_1, entry_2, entry_3), False)

        # one of them has a different everything
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2015
        entry_2.year = 2012
        entry_3.year = 2019

        entry_1.month = 2
        entry_2.month = 12
        entry_3.month = 5

        entry_1.day = 5
        entry_2.day = 6
        entry_3.day = 27

        entry_1.time = "2304"
        entry_2.time = "0500"
        entry_3.time = "1304"

        all_weather = read_ob_and_taf_json.AllWeather()

        self.assertEqual(all_weather.are_weather_entries_same_time(entry_1, entry_2, entry_3), False)

        # one of them has a different everything
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2015
        entry_2.year = 2015

        entry_1.month = 12
        entry_2.month = 12

        entry_1.day = 6
        entry_2.day = 6

        entry_1.time = "0500"
        entry_2.time = "0500"

        all_weather = read_ob_and_taf_json.AllWeather()

        self.assertEqual(all_weather.are_weather_entries_same_time(entry_1, entry_2), True)

    def test_WeatherEntry__eq__(self):
        # same both are predom taf
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2015
        entry_2.year = 2015

        entry_1.month = 12
        entry_2.month = 12

        entry_1.day = 6
        entry_2.day = 6

        entry_1.time = "0500"
        entry_2.time = "0500"

        entry_1.is_predom = True
        entry_2.is_predom = True

        entry_1.taf_predom_wind_dir = "030"
        entry_2.taf_predom_wind_dir = "030"

        entry_1.taf_predom_wind_spd = "08"
        entry_2.taf_predom_wind_spd = "08"

        entry_1.taf_predom_wind_gust = None
        entry_2.taf_predom_wind_gust = None

        entry_1.taf_predom_vis = "9999"
        entry_2.taf_predom_vis = "9999"

        entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_2.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}

        entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_2.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}

        entry_1.taf_predom_alstg = 2995
        entry_2.taf_predom_alstg = 2995

        self.assertEqual(entry_1 == entry_2, True)

        # differen predom taf
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2015
        entry_2.year = 2015

        entry_1.month = 12
        entry_2.month = 12

        entry_1.day = 6
        entry_2.day = 6

        entry_1.time = "0500"
        entry_2.time = "0600"

        entry_1.is_predom = True
        entry_2.is_predom = True

        entry_1.taf_predom_wind_dir = "030"
        entry_2.taf_predom_wind_dir = "030"

        entry_1.taf_predom_wind_spd = "08"
        entry_2.taf_predom_wind_spd = "08"

        entry_1.taf_predom_wind_gust = None
        entry_2.taf_predom_wind_gust = None

        entry_1.taf_predom_vis = "9999"
        entry_2.taf_predom_vis = "9999"

        entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_2.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}

        entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_2.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}

        entry_1.taf_predom_alstg = 2995
        entry_2.taf_predom_alstg = 2995

        self.assertEqual(entry_1 == entry_2, False)

    def test_merge_duplicates(self):

        # merges ob and predom taf
        entry_ob = read_ob_and_taf_json.WeatherEntry()
        entry_predom = read_ob_and_taf_json.WeatherEntry()
        entry_tempo = read_ob_and_taf_json.WeatherEntry()

        entry_ob.year = 2015
        entry_predom.year = 2015
        entry_tempo.year = 2015

        entry_ob.month = 2
        entry_predom.month = 2
        entry_tempo.month = 2

        entry_ob.day = 15
        entry_predom.day = 15
        entry_tempo.day = 15

        entry_ob.time = "1657"
        entry_predom.time = "1657"
        entry_tempo.time = "1657"

        entry_ob.is_ob = True
        entry_ob.ob_wind_dir = "250"
        entry_ob.ob_wind_spd = "03"
        entry_ob.ob_wind_gust = None
        entry_ob.ob_vis = "9999"
        entry_ob.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_ob.ob_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_ob.ob_temp = 25
        entry_ob.ob_temp_dew = 22
        entry_ob.ob_alstg = 3001

        entry_predom.is_predom = True
        entry_predom.taf_predom_wind_dir = "220"
        entry_predom.taf_predom_wind_spd = "10"
        entry_predom.taf_predom_wind_gust = "16"
        entry_predom.taf_predom_vis = "8000"
        entry_predom.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_predom.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '015'}}
        entry_predom.taf_predom_alstg = 2998

        # what the merged entry should look like
        correct_return = read_ob_and_taf_json.WeatherEntry()
        correct_return.year = 2015
        correct_return.month = 2
        correct_return.day = 15
        correct_return.time = "1657"
        correct_return.is_ob = True
        correct_return.ob_wind_dir = "250"
        correct_return.ob_wind_spd = "03"
        correct_return.ob_wind_gust = None
        correct_return.ob_vis = "9999"
        correct_return.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        correct_return.ob_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        correct_return.ob_temp = 25
        correct_return.ob_temp_dew = 22
        correct_return.ob_alstg = 3001

        correct_return.is_predom = True
        correct_return.taf_predom_wind_dir = "220"
        correct_return.taf_predom_wind_spd = "10"
        correct_return.taf_predom_wind_gust = "16"
        correct_return.taf_predom_vis = "8000"
        correct_return.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        correct_return.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '015'}}
        correct_return.taf_predom_alstg = 2998

        merged_entry = read_ob_and_taf_json.WeatherEntry.merge_duplicates(entry_predom, entry_ob)
        self.assertEqual(merged_entry == correct_return, True)



        # merges predom taf and tempo taf
        entry_ob = read_ob_and_taf_json.WeatherEntry()
        entry_predom = read_ob_and_taf_json.WeatherEntry()
        entry_tempo = read_ob_and_taf_json.WeatherEntry()

        entry_ob.year = 2015
        entry_predom.year = 2015
        entry_tempo.year = 2015

        entry_ob.month = 2
        entry_predom.month = 2
        entry_tempo.month = 2

        entry_ob.day = 15
        entry_predom.day = 15
        entry_tempo.day = 15

        entry_ob.time = "1657"
        entry_predom.time = "1657"
        entry_tempo.time = "1657"

        entry_predom.is_predom = True
        entry_predom.taf_predom_wind_dir = "220"
        entry_predom.taf_predom_wind_spd = "10"
        entry_predom.taf_predom_wind_gust = "16"
        entry_predom.taf_predom_vis = "8000"
        entry_predom.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                      'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_predom.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '015'}}
        entry_predom.taf_predom_alstg = 2998

        entry_tempo.is_tempo = True
        entry_tempo.taf_tempo_wind_dir = "250"
        entry_tempo.taf_tempo_wind_spd = "03"
        entry_tempo.taf_tempo_wind_gust = None
        entry_tempo.taf_tempo_vis = "9999"
        entry_tempo.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None,
                                'obscuration': None, 'other': None, 'rotation': None}
        entry_tempo.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}

        # what the merged entry should look like
        correct_return = read_ob_and_taf_json.WeatherEntry()
        correct_return.year = 2015
        correct_return.month = 2
        correct_return.day = 15
        correct_return.time = "1657"

        correct_return.is_predom = True
        correct_return.taf_predom_wind_dir = "220"
        correct_return.taf_predom_wind_spd = "10"
        correct_return.taf_predom_wind_gust = "16"
        correct_return.taf_predom_vis = "8000"
        correct_return.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                        'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        correct_return.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '015'}}
        correct_return.taf_predom_alstg = 2998

        correct_return.is_tempo = True
        correct_return.taf_tempo_wind_dir = "250"
        correct_return.taf_tempo_wind_spd = "03"
        correct_return.taf_tempo_wind_gust = None
        correct_return.taf_tempo_vis = "9999"
        correct_return.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        correct_return.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}

        merged_entry = read_ob_and_taf_json.WeatherEntry.merge_duplicates(entry_predom, entry_tempo)
        self.assertEqual(merged_entry, correct_return)



        # merges all 3
        entry_ob = read_ob_and_taf_json.WeatherEntry()
        entry_predom = read_ob_and_taf_json.WeatherEntry()
        entry_tempo = read_ob_and_taf_json.WeatherEntry()

        entry_ob.year = 2015
        entry_predom.year = 2015
        entry_tempo.year = 2015

        entry_ob.month = 2
        entry_predom.month = 2
        entry_tempo.month = 2

        entry_ob.day = 15
        entry_predom.day = 15
        entry_tempo.day = 15

        entry_ob.time = "1657"
        entry_predom.time = "1657"
        entry_tempo.time = "1657"

        entry_ob.is_ob = True
        entry_ob.ob_wind_dir = "250"
        entry_ob.ob_wind_spd = "03"
        entry_ob.ob_wind_gust = None
        entry_ob.ob_vis = "9999"
        entry_ob.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None,
                          'obscuration': None, 'other': None, 'rotation': None}
        entry_ob.ob_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_ob.ob_temp = 25
        entry_ob.ob_temp_dew = 22
        entry_ob.ob_alstg = 3001

        entry_predom.is_predom = True
        entry_predom.taf_predom_wind_dir = "220"
        entry_predom.taf_predom_wind_spd = "10"
        entry_predom.taf_predom_wind_gust = "16"
        entry_predom.taf_predom_vis = "8000"
        entry_predom.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                      'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_predom.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '015'}}
        entry_predom.taf_predom_alstg = 2998

        entry_tempo.is_tempo = True
        entry_tempo.taf_tempo_wind_dir = "250"
        entry_tempo.taf_tempo_wind_spd = "03"
        entry_tempo.taf_tempo_wind_gust = None
        entry_tempo.taf_tempo_vis = "9999"
        entry_tempo.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                    'precipitation': None,
                                    'obscuration': None, 'other': None, 'rotation': None}
        entry_tempo.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}

        # what the merged entry should look like
        correct_return = read_ob_and_taf_json.WeatherEntry()
        correct_return.year = 2015
        correct_return.month = 2
        correct_return.day = 15
        correct_return.time = "1657"

        correct_return.is_ob = True
        correct_return.ob_wind_dir = "250"
        correct_return.ob_wind_spd = "03"
        correct_return.ob_wind_gust = None
        correct_return.ob_vis = "9999"
        correct_return.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        correct_return.ob_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        correct_return.ob_temp = 25
        correct_return.ob_temp_dew = 22
        correct_return.ob_alstg = 3001

        correct_return.is_predom = True
        correct_return.taf_predom_wind_dir = "220"
        correct_return.taf_predom_wind_spd = "10"
        correct_return.taf_predom_wind_gust = "16"
        correct_return.taf_predom_vis = "8000"
        correct_return.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                        'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        correct_return.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '015'}}
        correct_return.taf_predom_alstg = 2998

        correct_return.is_tempo = True
        correct_return.taf_tempo_wind_dir = "250"
        correct_return.taf_tempo_wind_spd = "03"
        correct_return.taf_tempo_wind_gust = None
        correct_return.taf_tempo_vis = "9999"
        correct_return.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                       'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        correct_return.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}

        merged_entry = read_ob_and_taf_json.WeatherEntry.merge_duplicates(entry_predom, entry_tempo, entry_ob)

        # print(correct_return == merged_entry)
        # print(merged_entry)

        self.assertEqual(merged_entry, correct_return)

    def test_WeatherEntry__lt__(self):
        # both same time
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019

        entry_1.month = 10
        entry_2.month = 10

        entry_1.day = 17
        entry_2.day = 17

        entry_1.time = "1304"
        entry_2.time = "1304"

        self.assertEqual(entry_1 < entry_2, False)
        del entry_1
        del entry_2

        # entry_1 has lower time
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019

        entry_1.month = 10
        entry_2.month = 10

        entry_1.day = 17
        entry_2.day = 17

        entry_1.time = "1204"
        entry_2.time = "1304"

        self.assertEqual(entry_1 < entry_2, True)

        # entry_1 has higher day but lower time
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019

        entry_1.month = 10
        entry_2.month = 10

        entry_1.day = 19
        entry_2.day = 17

        entry_1.time = "1104"
        entry_2.time = "1304"

        self.assertEqual(entry_1 < entry_2, False)

        # entry_1 has higher month
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019

        entry_1.month = 11
        entry_2.month = 10

        entry_1.day = 17
        entry_2.day = 17

        entry_1.time = "1304"
        entry_2.time = "1304"

        self.assertEqual(entry_1 < entry_2, False)

    def test_sort(self):
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019
        entry_4.year = 2019

        entry_1.month = 10
        entry_2.month = 10
        entry_3.month = 10
        entry_4.month = 10

        entry_1.day = 19
        entry_2.day = 13
        entry_3.day = 16
        entry_4.day = 15

        entry_1.time = "1304"
        entry_2.time = "1304"
        entry_3.time = "1304"
        entry_4.time = "1304"

        print(entry_1 < entry_2)

        entries_unsorted = [entry_1, entry_2, entry_3, entry_4]
        print(entries_unsorted)
        entries_sorted = sorted(entries_unsorted, key=lambda entry: (entry.year, entry.month, entry.day, int(entry.time)))

        correct_return = [entry_2, entry_4, entry_3, entry_1]

        print(entries_sorted)
        self.maxDiff = None
        # self.assertEqual(entries_sorted, correct_return)

    def test_check_and_merge_all_duplicates(self):

        # one set of duplicates, 2 items long
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()
        entry_6 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019
        entry_4.year = 2019
        entry_5.year = 2019
        entry_6.year = 2019

        entry_1.month = 4
        entry_2.month = 4
        entry_3.month = 4
        entry_4.month = 4
        entry_5.month = 4
        entry_6.month = 4

        entry_1.day = 21
        entry_2.day = 21
        entry_3.day = 21
        entry_4.day = 21
        entry_5.day = 21
        entry_6.day = 21

        entry_1.time = "1455"
        entry_2.time = "1600"
        entry_3.time = "1155"
        entry_4.time = "1600"
        entry_5.time = "1356"
        entry_6.time = "1257"

        # entry_1 is a PREDOM TAF, NOT duplicate
        entry_1.is_predom = True
        entry_1.taf_predom_wind_dir = "120"
        entry_1.taf_predom_wind_spd = "09"
        entry_1.taf_predom_wind_gust = None
        entry_1.taf_predom_vis = "9999"
        entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_1.taf_predom_alstg = 2997

        # entry_2 is a TEMPO TAF, duplicate
        entry_2.is_tempo = True
        entry_2.taf_tempo_wind_dir = "150"
        entry_2.taf_tempo_wind_spd = "15"
        entry_2.taf_tempo_wind_gust = "20"
        entry_2.taf_tempo_vis = "4800"
        entry_2.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_2.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # entry_3 OB, NOT duplicate
        entry_3.is_ob = True
        entry_3.ob_wind_dir = "110"
        entry_3.ob_wind_spd = "12"
        entry_3.ob_wind_gust = "18"
        entry_3.ob_vis = "9000"
        entry_3.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None, 'obscuration': "HZ", 'other': None, 'rotation': None}
        entry_3.ob_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}
        entry_3.ob_temp = 21
        entry_3.ob_temp_dew = 8
        entry_3.ob_alstg = 3001

        # entry_4 is PREDOM TAF, duplicate
        entry_4.is_predom = True
        entry_4.taf_predom_wind_dir = "160"
        entry_4.taf_predom_wind_spd = "12"
        entry_4.taf_predom_wind_gust = None
        entry_4.taf_predom_vis = "9999"
        entry_4.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_4.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        entry_4.taf_predom_alstg = 2990

        # entry_5 is OB, not duplicate
        entry_5.is_ob = True
        entry_5.ob_wind_dir = "170"
        entry_5.ob_wind_spd = "13"
        entry_5.ob_wind_gust = None
        entry_5.ob_vis = "9999"
        entry_5.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                  'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_5.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        entry_5.ob_temp = 15
        entry_5.ob_temp_dew = 4
        entry_5.ob_alstg = 3003

        # entry_6, is PREDOM TAF, not duplicate
        entry_6.is_predom = True
        entry_6.taf_predom_wind_dir = "210"
        entry_6.taf_predom_wind_spd = "04"
        entry_6.taf_predom_wind_gust = None
        entry_6.taf_predom_vis = "9000"
        entry_6.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': "RA", 'obscuration': None, 'other': None, 'rotation': None}
        entry_6.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'BKN', 'base': '040'}}
        entry_6.taf_predom_alstg = 2993

        merged_entry_1 = read_ob_and_taf_json.WeatherEntry()
        # time for merged_entry
        merged_entry_1.year = 2019
        merged_entry_1.month = 4
        merged_entry_1.day = 21
        merged_entry_1.time = "1600"

        # from entry_2
        merged_entry_1.is_tempo = True
        merged_entry_1.taf_tempo_wind_dir = "150"
        merged_entry_1.taf_tempo_wind_spd = "15"
        merged_entry_1.taf_tempo_wind_gust = "20"
        merged_entry_1.taf_tempo_vis = "4800"
        merged_entry_1.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        merged_entry_1.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # from entry_4
        merged_entry_1.is_predom = True
        merged_entry_1.taf_predom_wind_dir = "160"
        merged_entry_1.taf_predom_wind_spd = "12"
        merged_entry_1.taf_predom_wind_gust = None
        merged_entry_1.taf_predom_vis = "9999"
        merged_entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        merged_entry_1.taf_predom_alstg = 2990


        all_weather = read_ob_and_taf_json.AllWeather()
        all_weather.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]
        all_weather_unchanged = read_ob_and_taf_json.AllWeather()
        all_weather_unchanged.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]

        all_weather.check_and_merge_all_duplicates()

        all_checks = []
        # print(all_weather_unchanged.weather_entries[2])
        all_checks.append(all_weather.weather_entries[0] == all_weather_unchanged.weather_entries[2])  # 3
        all_checks.append(all_weather.weather_entries[1] == all_weather_unchanged.weather_entries[5])  # 6
        all_checks.append(all_weather.weather_entries[2] == all_weather_unchanged.weather_entries[4])  # 5
        all_checks.append(all_weather.weather_entries[3] == all_weather_unchanged.weather_entries[0])  # 1
        all_checks.append(all_weather.weather_entries[4] == merged_entry_1)
        all_checks.append(len(all_weather.weather_entries) == 5)

        self.assertEqual(all(all_checks), True)

        # one set of duplicates, 3 items long
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()
        entry_6 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019
        entry_4.year = 2019
        entry_5.year = 2019
        entry_6.year = 2019

        entry_1.month = 4
        entry_2.month = 4
        entry_3.month = 4
        entry_4.month = 4
        entry_5.month = 4
        entry_6.month = 4

        entry_1.day = 21
        entry_2.day = 21
        entry_3.day = 21
        entry_4.day = 21
        entry_5.day = 21
        entry_6.day = 21

        entry_1.time = "1455"
        entry_2.time = "1600"
        entry_3.time = "1155"
        entry_4.time = "1600"
        entry_5.time = "1600"
        entry_6.time = "1257"

        # entry_1 is a PREDOM TAF, NOT duplicate
        entry_1.is_predom = True
        entry_1.taf_predom_wind_dir = "120"
        entry_1.taf_predom_wind_spd = "09"
        entry_1.taf_predom_wind_gust = None
        entry_1.taf_predom_vis = "9999"
        entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_1.taf_predom_alstg = 2997

        # entry_2 is a TEMPO TAF, duplicate
        entry_2.is_tempo = True
        entry_2.taf_tempo_wind_dir = "150"
        entry_2.taf_tempo_wind_spd = "15"
        entry_2.taf_tempo_wind_gust = "20"
        entry_2.taf_tempo_vis = "4800"
        entry_2.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_2.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # entry_3 OB, NOT duplicate
        entry_3.is_ob = True
        entry_3.ob_wind_dir = "110"
        entry_3.ob_wind_spd = "12"
        entry_3.ob_wind_gust = "18"
        entry_3.ob_vis = "9000"
        entry_3.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None,
                         'obscuration': "HZ", 'other': None, 'rotation': None}
        entry_3.ob_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}
        entry_3.ob_temp = 21
        entry_3.ob_temp_dew = 8
        entry_3.ob_alstg = 3001

        # entry_4 is PREDOM TAF, duplicate
        entry_4.is_predom = True
        entry_4.taf_predom_wind_dir = "160"
        entry_4.taf_predom_wind_spd = "12"
        entry_4.taf_predom_wind_gust = None
        entry_4.taf_predom_vis = "9999"
        entry_4.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_4.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        entry_4.taf_predom_alstg = 2990

        # entry_5 is OB, not duplicate
        entry_5.is_ob = True
        entry_5.ob_wind_dir = "170"
        entry_5.ob_wind_spd = "13"
        entry_5.ob_wind_gust = None
        entry_5.ob_vis = "9999"
        entry_5.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                         'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_5.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        entry_5.ob_temp = 15
        entry_5.ob_temp_dew = 4
        entry_5.ob_alstg = 3003

        # entry_6, is PREDOM TAF, not duplicate
        entry_6.is_predom = True
        entry_6.taf_predom_wind_dir = "210"
        entry_6.taf_predom_wind_spd = "04"
        entry_6.taf_predom_wind_gust = None
        entry_6.taf_predom_vis = "9000"
        entry_6.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': "RA", 'obscuration': None, 'other': None, 'rotation': None}
        entry_6.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'BKN', 'base': '040'}}
        entry_6.taf_predom_alstg = 2993


        merged_entry_1 = read_ob_and_taf_json.WeatherEntry()
        # time for merged_entry
        merged_entry_1.year = 2019
        merged_entry_1.month = 4
        merged_entry_1.day = 21
        merged_entry_1.time = "1600"

        # from entry_2
        merged_entry_1.is_tempo = True
        merged_entry_1.taf_tempo_wind_dir = "150"
        merged_entry_1.taf_tempo_wind_spd = "15"
        merged_entry_1.taf_tempo_wind_gust = "20"
        merged_entry_1.taf_tempo_vis = "4800"
        merged_entry_1.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                       'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        merged_entry_1.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # from entry_4
        merged_entry_1.is_predom = True
        merged_entry_1.taf_predom_wind_dir = "160"
        merged_entry_1.taf_predom_wind_spd = "12"
        merged_entry_1.taf_predom_wind_gust = None
        merged_entry_1.taf_predom_vis = "9999"
        merged_entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                        'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        merged_entry_1.taf_predom_alstg = 2990
        # from entry_5
        merged_entry_1.is_ob = True
        merged_entry_1.ob_wind_dir = "170"
        merged_entry_1.ob_wind_spd = "13"
        merged_entry_1.ob_wind_gust = None
        merged_entry_1.ob_vis = "9999"
        merged_entry_1.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                         'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_1.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        merged_entry_1.ob_temp = 15
        merged_entry_1.ob_temp_dew = 4
        merged_entry_1.ob_alstg = 3003


        all_weather = read_ob_and_taf_json.AllWeather()
        all_weather.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]
        all_weather_unchanged = read_ob_and_taf_json.AllWeather()
        all_weather_unchanged.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]

        all_weather.check_and_merge_all_duplicates()

        all_checks = []
        # print(all_weather_unchanged.weather_entries[2])
        all_checks.append(all_weather.weather_entries[0] == all_weather_unchanged.weather_entries[2])  # 3
        all_checks.append(all_weather.weather_entries[1] == all_weather_unchanged.weather_entries[5])  # 6
        all_checks.append(all_weather.weather_entries[2] == all_weather_unchanged.weather_entries[0])  # 5
        all_checks.append(all_weather.weather_entries[3] == merged_entry_1)
        all_checks.append(len(all_weather.weather_entries) == 4)

        self.assertEqual(all(all_checks), True)

        # 2 sets of duplicates, 3 items long and 2 items long
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()
        entry_6 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019
        entry_4.year = 2019
        entry_5.year = 2019
        entry_6.year = 2019

        entry_1.month = 4
        entry_2.month = 4
        entry_3.month = 4
        entry_4.month = 4
        entry_5.month = 4
        entry_6.month = 4

        entry_1.day = 21
        entry_2.day = 21
        entry_3.day = 21
        entry_4.day = 21
        entry_5.day = 21
        entry_6.day = 21

        entry_1.time = "0955"
        entry_2.time = "1600"
        entry_3.time = "0955"
        entry_4.time = "1600"
        entry_5.time = "1600"
        entry_6.time = "1257"

        # entry_1 is a PREDOM TAF, NOT duplicate
        entry_1.is_predom = True
        entry_1.taf_predom_wind_dir = "120"
        entry_1.taf_predom_wind_spd = "09"
        entry_1.taf_predom_wind_gust = None
        entry_1.taf_predom_vis = "9999"
        entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_1.taf_predom_alstg = 2997

        # entry_2 is a TEMPO TAF, duplicate
        entry_2.is_tempo = True
        entry_2.taf_tempo_wind_dir = "150"
        entry_2.taf_tempo_wind_spd = "15"
        entry_2.taf_tempo_wind_gust = "20"
        entry_2.taf_tempo_vis = "4800"
        entry_2.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_2.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # entry_3 OB, NOT duplicate
        entry_3.is_ob = True
        entry_3.ob_wind_dir = "110"
        entry_3.ob_wind_spd = "12"
        entry_3.ob_wind_gust = "18"
        entry_3.ob_vis = "9000"
        entry_3.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None,
                         'obscuration': "HZ", 'other': None, 'rotation': None}
        entry_3.ob_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}
        entry_3.ob_temp = 21
        entry_3.ob_temp_dew = 8
        entry_3.ob_alstg = 3001

        # entry_4 is PREDOM TAF, duplicate
        entry_4.is_predom = True
        entry_4.taf_predom_wind_dir = "160"
        entry_4.taf_predom_wind_spd = "12"
        entry_4.taf_predom_wind_gust = None
        entry_4.taf_predom_vis = "9999"
        entry_4.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_4.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        entry_4.taf_predom_alstg = 2990

        # entry_5 is OB, not duplicate
        entry_5.is_ob = True
        entry_5.ob_wind_dir = "170"
        entry_5.ob_wind_spd = "13"
        entry_5.ob_wind_gust = None
        entry_5.ob_vis = "9999"
        entry_5.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                         'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_5.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        entry_5.ob_temp = 15
        entry_5.ob_temp_dew = 4
        entry_5.ob_alstg = 3003

        # entry_6, is PREDOM TAF, not duplicate
        entry_6.is_predom = True
        entry_6.taf_predom_wind_dir = "210"
        entry_6.taf_predom_wind_spd = "04"
        entry_6.taf_predom_wind_gust = None
        entry_6.taf_predom_vis = "9000"
        entry_6.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': "RA", 'obscuration': None, 'other': None, 'rotation': None}
        entry_6.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'BKN', 'base': '040'}}
        entry_6.taf_predom_alstg = 2993

        merged_entry_1 = read_ob_and_taf_json.WeatherEntry()
        # time for merged_entry
        merged_entry_1.year = 2019
        merged_entry_1.month = 4
        merged_entry_1.day = 21
        merged_entry_1.time = "1600"

        # from entry_2
        merged_entry_1.is_tempo = True
        merged_entry_1.taf_tempo_wind_dir = "150"
        merged_entry_1.taf_tempo_wind_spd = "15"
        merged_entry_1.taf_tempo_wind_gust = "20"
        merged_entry_1.taf_tempo_vis = "4800"
        merged_entry_1.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                       'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        merged_entry_1.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # from entry_4
        merged_entry_1.is_predom = True
        merged_entry_1.taf_predom_wind_dir = "160"
        merged_entry_1.taf_predom_wind_spd = "12"
        merged_entry_1.taf_predom_wind_gust = None
        merged_entry_1.taf_predom_vis = "9999"
        merged_entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                        'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        merged_entry_1.taf_predom_alstg = 2990
        # from entry_5
        merged_entry_1.is_ob = True
        merged_entry_1.ob_wind_dir = "170"
        merged_entry_1.ob_wind_spd = "13"
        merged_entry_1.ob_wind_gust = None
        merged_entry_1.ob_vis = "9999"
        merged_entry_1.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_1.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        merged_entry_1.ob_temp = 15
        merged_entry_1.ob_temp_dew = 4
        merged_entry_1.ob_alstg = 3003

        merged_entry_2 = read_ob_and_taf_json.WeatherEntry()
        # time for merged_entry
        merged_entry_2.year = 2019
        merged_entry_2.month = 4
        merged_entry_2.day = 21
        merged_entry_2.time = "0955"

        # from entry_1
        merged_entry_2.is_predom = True
        merged_entry_2.taf_predom_wind_dir = "120"
        merged_entry_2.taf_predom_wind_spd = "09"
        merged_entry_2.taf_predom_wind_gust = None
        merged_entry_2.taf_predom_vis = "9999"
        merged_entry_2.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_2.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        merged_entry_2.taf_predom_alstg = 2997

        # from merged_entry_3
        merged_entry_2.is_ob = True
        merged_entry_2.ob_wind_dir = "110"
        merged_entry_2.ob_wind_spd = "12"
        merged_entry_2.ob_wind_gust = "18"
        merged_entry_2.ob_vis = "9000"
        merged_entry_2.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None,
                         'obscuration': "HZ", 'other': None, 'rotation': None}
        merged_entry_2.ob_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}
        merged_entry_2.ob_temp = 21
        merged_entry_2.ob_temp_dew = 8
        merged_entry_2.ob_alstg = 3001


        all_weather = read_ob_and_taf_json.AllWeather()
        all_weather.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]
        all_weather_unchanged = read_ob_and_taf_json.AllWeather()
        all_weather_unchanged.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]

        all_weather.check_and_merge_all_duplicates()

        all_checks = []
        # print(all_weather_unchanged.weather_entries[2])
        all_checks.append(all_weather.weather_entries[0] == merged_entry_2)  # 3
        all_checks.append(all_weather.weather_entries[1] == all_weather_unchanged.weather_entries[5])  # 6
        all_checks.append(all_weather.weather_entries[2] == merged_entry_1)
        all_checks.append(len(all_weather.weather_entries) == 3)

        # print(all_checks)

        self.assertEqual(all(all_checks), True)

        # no duplicates
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()
        entry_6 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019
        entry_4.year = 2019
        entry_5.year = 2019
        entry_6.year = 2019

        entry_1.month = 4
        entry_2.month = 4
        entry_3.month = 4
        entry_4.month = 4
        entry_5.month = 4
        entry_6.month = 4

        entry_1.day = 21
        entry_2.day = 22
        entry_3.day = 2
        entry_4.day = 25
        entry_5.day = 28
        entry_6.day = 26

        entry_1.time = "1455"
        entry_2.time = "1600"
        entry_3.time = "1155"
        entry_4.time = "1600"
        entry_5.time = "1356"
        entry_6.time = "1257"

        # entry_1 is a PREDOM TAF, NOT duplicate
        entry_1.is_predom = True
        entry_1.taf_predom_wind_dir = "120"
        entry_1.taf_predom_wind_spd = "09"
        entry_1.taf_predom_wind_gust = None
        entry_1.taf_predom_vis = "9999"
        entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_1.taf_predom_alstg = 2997

        # entry_2 is a TEMPO TAF, duplicate
        entry_2.is_tempo = True
        entry_2.taf_tempo_wind_dir = "150"
        entry_2.taf_tempo_wind_spd = "15"
        entry_2.taf_tempo_wind_gust = "20"
        entry_2.taf_tempo_vis = "4800"
        entry_2.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_2.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # entry_3 OB, NOT duplicate
        entry_3.is_ob = True
        entry_3.ob_wind_dir = "110"
        entry_3.ob_wind_spd = "12"
        entry_3.ob_wind_gust = "18"
        entry_3.ob_vis = "9000"
        entry_3.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None,
                         'obscuration': "HZ", 'other': None, 'rotation': None}
        entry_3.ob_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}
        entry_3.ob_temp = 21
        entry_3.ob_temp_dew = 8
        entry_3.ob_alstg = 3001

        # entry_4 is PREDOM TAF, duplicate
        entry_4.is_predom = True
        entry_4.taf_predom_wind_dir = "160"
        entry_4.taf_predom_wind_spd = "12"
        entry_4.taf_predom_wind_gust = None
        entry_4.taf_predom_vis = "9999"
        entry_4.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_4.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        entry_4.taf_predom_alstg = 2990

        # entry_5 is OB, not duplicate
        entry_5.is_ob = True
        entry_5.ob_wind_dir = "170"
        entry_5.ob_wind_spd = "13"
        entry_5.ob_wind_gust = None
        entry_5.ob_vis = "9999"
        entry_5.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                         'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_5.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        entry_5.ob_temp = 15
        entry_5.ob_temp_dew = 4
        entry_5.ob_alstg = 3003

        # entry_6, is PREDOM TAF, not duplicate
        entry_6.is_predom = True
        entry_6.taf_predom_wind_dir = "210"
        entry_6.taf_predom_wind_spd = "04"
        entry_6.taf_predom_wind_gust = None
        entry_6.taf_predom_vis = "9000"
        entry_6.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': "RA", 'obscuration': None, 'other': None, 'rotation': None}
        entry_6.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'BKN', 'base': '040'}}
        entry_6.taf_predom_alstg = 2993


        all_weather = read_ob_and_taf_json.AllWeather()
        all_weather.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]
        all_weather_unchanged = read_ob_and_taf_json.AllWeather()
        all_weather_unchanged.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]

        all_weather.check_and_merge_all_duplicates()

        all_checks = []
        # print(all_weather_unchanged.weather_entries[2])
        all_checks.append(all_weather.weather_entries[0] == all_weather_unchanged.weather_entries[2])  # 3
        all_checks.append(all_weather.weather_entries[1] == all_weather_unchanged.weather_entries[0])  # 6
        all_checks.append(all_weather.weather_entries[2] == all_weather_unchanged.weather_entries[1])  # 5
        all_checks.append(all_weather.weather_entries[3] == all_weather_unchanged.weather_entries[3])  # 1
        all_checks.append(all_weather.weather_entries[4] == all_weather_unchanged.weather_entries[5])  # 1
        all_checks.append(all_weather.weather_entries[5] == all_weather_unchanged.weather_entries[4])  # 1
        all_checks.append(len(all_weather.weather_entries) == 6)

        self.assertEqual(all(all_checks), True)

        # 2 sets of duplicates, 3 items long and 2 items long, has PK WND that has same time as taf
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()
        entry_6 = read_ob_and_taf_json.WeatherEntry()

        entry_1.year = 2019
        entry_2.year = 2019
        entry_3.year = 2019
        entry_4.year = 2019
        entry_5.year = 2019
        entry_6.year = 2019

        entry_1.month = 4
        entry_2.month = 4
        entry_3.month = 4
        entry_4.month = 4
        entry_5.month = 4
        entry_6.month = 4

        entry_1.day = 21
        entry_2.day = 21
        entry_3.day = 21
        entry_4.day = 21
        entry_5.day = 21
        entry_6.day = 21

        entry_1.time = "0955"
        entry_2.time = "1600"
        entry_3.time = "0955"
        entry_4.time = "1600"
        entry_5.time = "1600"
        entry_6.time = "1257"

        # entry_1 is a PREDOM TAF, duplicate
        entry_1.is_predom = True
        entry_1.taf_predom_wind_dir = "120"
        entry_1.taf_predom_wind_spd = "09"
        entry_1.taf_predom_wind_gust = None
        entry_1.taf_predom_vis = "9999"
        entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        entry_1.taf_predom_alstg = 2997

        # entry_2 is a TEMPO TAF, duplicate
        entry_2.is_tempo = True
        entry_2.taf_tempo_wind_dir = "150"
        entry_2.taf_tempo_wind_spd = "15"
        entry_2.taf_tempo_wind_gust = "20"
        entry_2.taf_tempo_vis = "4800"
        entry_2.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        entry_2.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # entry_3 OB, duplicate
        entry_3.is_ob = True
        entry_3.ob_wind_dir = "110"
        entry_3.ob_wind_spd = "12"
        entry_3.ob_wind_gust = "18"
        entry_3.ob_vis = "9000"
        entry_3.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None, 'precipitation': None,
                         'obscuration': "HZ", 'other': None, 'rotation': None}
        entry_3.ob_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}
        entry_3.ob_temp = 21
        entry_3.ob_temp_dew = 8
        entry_3.ob_alstg = 3001
        entry_3.ob_is_pk_wnd = True
        entry_3.ob_pk_wnd_dir = "070"
        entry_3.ob_pk_wnd_spd = "48"

        # entry_4 is PREDOM TAF, duplicate
        entry_4.is_predom = True
        entry_4.taf_predom_wind_dir = "160"
        entry_4.taf_predom_wind_spd = "12"
        entry_4.taf_predom_wind_gust = None
        entry_4.taf_predom_vis = "9999"
        entry_4.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_4.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        entry_4.taf_predom_alstg = 2990

        # entry_5 is OB, duplicate
        entry_5.is_ob = True
        entry_5.ob_wind_dir = "170"
        entry_5.ob_wind_spd = "13"
        entry_5.ob_wind_gust = None
        entry_5.ob_vis = "9999"
        entry_5.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                         'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        entry_5.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        entry_5.ob_temp = 15
        entry_5.ob_temp_dew = 4
        entry_5.ob_alstg = 3003

        # entry_6, is PREDOM TAF, not duplicate
        entry_6.is_predom = True
        entry_6.taf_predom_wind_dir = "210"
        entry_6.taf_predom_wind_spd = "04"
        entry_6.taf_predom_wind_gust = None
        entry_6.taf_predom_vis = "9000"
        entry_6.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                 'precipitation': "RA", 'obscuration': None, 'other': None, 'rotation': None}
        entry_6.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'BKN', 'base': '040'}}
        entry_6.taf_predom_alstg = 2993

        merged_entry_1 = read_ob_and_taf_json.WeatherEntry()
        # time for merged_entry
        merged_entry_1.year = 2019
        merged_entry_1.month = 4
        merged_entry_1.day = 21
        merged_entry_1.time = "1600"

        # from entry_2
        merged_entry_1.is_tempo = True
        merged_entry_1.taf_tempo_wind_dir = "150"
        merged_entry_1.taf_tempo_wind_spd = "15"
        merged_entry_1.taf_tempo_wind_gust = "20"
        merged_entry_1.taf_tempo_vis = "4800"
        merged_entry_1.taf_tempo_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                       'precipitation': None, 'obscuration': "BR", 'other': None, 'rotation': None}
        merged_entry_1.taf_tempo_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}

        # from entry_4
        merged_entry_1.is_predom = True
        merged_entry_1.taf_predom_wind_dir = "160"
        merged_entry_1.taf_predom_wind_spd = "12"
        merged_entry_1.taf_predom_wind_gust = None
        merged_entry_1.taf_predom_vis = "9999"
        merged_entry_1.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                        'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_1.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '180'}}
        merged_entry_1.taf_predom_alstg = 2990
        # from entry_5
        merged_entry_1.is_ob = True
        merged_entry_1.ob_wind_dir = "170"
        merged_entry_1.ob_wind_spd = "13"
        merged_entry_1.ob_wind_gust = None
        merged_entry_1.ob_vis = "9999"
        merged_entry_1.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_1.ob_sky_con = {'cloud_layer_1': {'amount': 'SCT', 'base': '190'}}
        merged_entry_1.ob_temp = 15
        merged_entry_1.ob_temp_dew = 4
        merged_entry_1.ob_alstg = 3003

        merged_entry_2 = read_ob_and_taf_json.WeatherEntry()
        # time for merged_entry
        merged_entry_2.year = 2019
        merged_entry_2.month = 4
        merged_entry_2.day = 21
        merged_entry_2.time = "0955"

        # from entry_1
        merged_entry_2.is_predom = True
        merged_entry_2.taf_predom_wind_dir = "120"
        merged_entry_2.taf_predom_wind_spd = "09"
        merged_entry_2.taf_predom_wind_gust = None
        merged_entry_2.taf_predom_vis = "9999"
        merged_entry_2.taf_predom_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                        'precipitation': None, 'obscuration': None, 'other': None, 'rotation': None}
        merged_entry_2.taf_predom_sky_con = {'cloud_layer_1': {'amount': 'FEW', 'base': '250'}}
        merged_entry_2.taf_predom_alstg = 2997

        # from merged_entry_3
        merged_entry_2.is_ob = True
        merged_entry_2.ob_wind_dir = "110"
        merged_entry_2.ob_wind_spd = "12"
        merged_entry_2.ob_wind_gust = "18"
        merged_entry_2.ob_vis = "9000"
        merged_entry_2.ob_wx = {'wx_intensity': None, 'vicinity_or_not': None, 'description': None,
                                'precipitation': None,
                                'obscuration': "HZ", 'other': None, 'rotation': None}
        merged_entry_2.ob_sky_con = {'cloud_layer_1': {'amount': 'OVC', 'base': '010'}}
        merged_entry_2.ob_temp = 21
        merged_entry_2.ob_temp_dew = 8
        merged_entry_2.ob_alstg = 3001
        merged_entry_2.ob_is_pk_wnd = True
        merged_entry_2.ob_pk_wnd_dir = "070"
        merged_entry_2.ob_pk_wnd_spd = "48"


        all_weather = read_ob_and_taf_json.AllWeather()
        all_weather.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]
        all_weather_unchanged = read_ob_and_taf_json.AllWeather()
        all_weather_unchanged.weather_entries = [entry_1, entry_2, entry_3, entry_4, entry_5, entry_6]

        all_weather.check_and_merge_all_duplicates()

        all_checks = []
        # print(all_weather_unchanged.weather_entries[2])
        all_checks.append(all_weather.weather_entries[0] == merged_entry_2)  # 3
        all_checks.append(all_weather.weather_entries[1] == all_weather_unchanged.weather_entries[5])  # 6
        all_checks.append(all_weather.weather_entries[2] == merged_entry_1)
        all_checks.append(len(all_weather.weather_entries) == 3)

        # print(all_checks)

        self.assertEqual(all(all_checks), True)

    def test_get_data_types(self):
        test_entry = read_ob_and_taf_json.WeatherEntry()
        test_entry.is_tempo = True
        self.assertEqual(test_entry.get_data_types(), {'tempo'})

        test_entry = read_ob_and_taf_json.WeatherEntry()
        test_entry.is_tempo = True
        test_entry.is_ob = True
        self.assertEqual(test_entry.get_data_types(), {'tempo', 'ob'})

        test_entry = read_ob_and_taf_json.WeatherEntry()
        test_entry.is_tempo = True
        test_entry.is_ob = True
        test_entry.is_predom = True
        self.assertEqual(test_entry.get_data_types(), {'tempo', 'ob', 'predom'})

        test_entry = read_ob_and_taf_json.WeatherEntry()
        self.assertEqual(test_entry.get_data_types(), set())

    def test_get_entry_time(self):
        test_entry = read_ob_and_taf_json.WeatherEntry()
        test_entry.year = 2019
        test_entry.month = 5
        test_entry.day = 29
        test_entry.time = "0500"
        correct_return = {'time': "0500", 'day': 29, 'month': 5, 'year': 2019}
        self.assertEqual(test_entry.get_entry_time(), correct_return)

    def test_get_tempo_end(self):
        test_entry = read_ob_and_taf_json.WeatherEntry()
        test_entry.tempo_end_time = "1500"
        test_entry.tempo_end_day = 23
        test_entry.tempo_end_month = 1
        test_entry.tempo_end_year = 2020
        correct_return = {'time': "1500", 'day': 23, 'month': 1, 'year': 2020}
        self.assertEqual(test_entry.get_tempo_end(), correct_return)

    def test_sort_entires(self):
        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()


        entry_0.year = 2019
        entry_0.month = 12
        entry_0.day = 18
        entry_0.time = "0200"

        entry_1.year = 2019
        entry_1.month = 12
        entry_1.day = 18
        entry_1.time = "0300"

        entry_2.is_tempo = True
        entry_2.year = 2019
        entry_2.month = 12
        entry_2.day = 18
        entry_2.time = "230000"

        entry_3.year = 2019
        entry_3.month = 12
        entry_3.day = 18
        entry_3.time = "2001"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.sort_entries()

        all_checks = []
        all_checks.append(a.weather_entries[0] == entry_0)
        all_checks.append(a.weather_entries[1] == entry_1)
        all_checks.append(a.weather_entries[2] == entry_3)
        all_checks.append(a.weather_entries[3] == entry_2)
        self.assertEqual(all(all_checks), True)


    def test_add_entries_for_tempo_end(self):
        # has one tempo, needs to be sorted afterwards

        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()

        entry_0.year = 2019
        entry_0.month = 12
        entry_0.day = 18
        entry_0.time = "0200"

        entry_1.year = 2019
        entry_1.month = 12
        entry_1.day = 18
        entry_1.time = "0300"

        entry_2.is_tempo = True
        entry_2.year = 2019
        entry_2.month = 12
        entry_2.day = 18
        entry_2.time = "1600"
        entry_2.tempo_end_year = 2019
        entry_2.tempo_end_month = 12
        entry_2.tempo_end_day = 18
        entry_2.tempo_end_time = "2000"

        entry_3.year = 2019
        entry_3.month = 12
        entry_3.day = 18
        entry_3.time = "1905"

        new_entry_0 = read_ob_and_taf_json.WeatherEntry()
        new_entry_0.year = 2019
        new_entry_0.month = 12
        new_entry_0.day = 18
        new_entry_0.time = "2000"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.add_entries_for_tempo_end()

        all_checks = []
        all_checks.append(len(a.weather_entries) == 5)
        all_checks.append(a.weather_entries[0] == entry_0)
        all_checks.append(a.weather_entries[1] == entry_1)
        all_checks.append(a.weather_entries[2] == entry_2)
        all_checks.append(a.weather_entries[3].time == '1905')
        all_checks.append(a.weather_entries[3] == entry_3)

        all_checks.append(a.weather_entries[4] == new_entry_0)
        self.assertEqual(all(all_checks), True)

        ###################################################

        # has one tempo,

        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()

        entry_0.year = 2019
        entry_0.month = 12
        entry_0.day = 18
        entry_0.time = "0200"

        entry_1.year = 2019
        entry_1.month = 12
        entry_1.day = 18
        entry_1.time = "0300"

        entry_2.is_tempo = True
        entry_2.year = 2019
        entry_2.month = 12
        entry_2.day = 18
        entry_2.time = "1600"
        entry_2.tempo_end_year = 2019
        entry_2.tempo_end_month = 12
        entry_2.tempo_end_day = 18
        entry_2.tempo_end_time = "2000"

        entry_3.year = 2019
        entry_3.month = 12
        entry_3.day = 18
        entry_3.time = "2001"

        new_entry_0 = read_ob_and_taf_json.WeatherEntry()
        new_entry_0.year = 2019
        new_entry_0.month = 12
        new_entry_0.day = 18
        new_entry_0.time = "2000"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.add_entries_for_tempo_end()

        all_checks = []
        all_checks.append(len(a.weather_entries) == 5)
        all_checks.append(a.weather_entries[4].time == '2001')
        all_checks.append(a.weather_entries[4] == entry_3)
        all_checks.append(a.weather_entries[0] == entry_0)
        all_checks.append(a.weather_entries[1] == entry_1)
        all_checks.append(a.weather_entries[2] == entry_2)

        all_checks.append(a.weather_entries[3] == new_entry_0)
        self.assertEqual(all(all_checks), True)


        ###################################################
        # has two tempos, entry_2 and entry_4, tempos are not back-to-back

        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()

        entry_0.year = 2019
        entry_0.month = 12
        entry_0.day = 18
        entry_0.time = "0001"

        entry_1.year = 2019
        entry_1.month = 12
        entry_1.day = 18
        entry_1.time = "0121"

        entry_2.is_tempo = True
        entry_2.year = 2019
        entry_2.month = 12
        entry_2.day = 18
        entry_2.time = "1600"
        entry_2.tempo_end_year = 2019
        entry_2.tempo_end_month = 12
        entry_2.tempo_end_day = 18
        entry_2.tempo_end_time = "2000"

        entry_3.year = 2019
        entry_3.month = 12
        entry_3.day = 18
        entry_3.time = "2001"

        entry_4.is_tempo = True
        entry_4.year = 2019
        entry_4.month = 12
        entry_4.day = 19
        entry_4.time = "0200"
        entry_4.tempo_end_year = 2019
        entry_4.tempo_end_month = 12
        entry_4.tempo_end_day = 19
        entry_4.tempo_end_time = "0500"

        new_entry_0 = read_ob_and_taf_json.WeatherEntry()
        new_entry_0.year = 2019
        new_entry_0.month = 12
        new_entry_0.day = 18
        new_entry_0.time = "2000"

        new_entry_1 = read_ob_and_taf_json.WeatherEntry()
        new_entry_1.year = 2019
        new_entry_1.month = 12
        new_entry_1.day = 19
        new_entry_1.time = "0500"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.weather_entries.append(entry_4)
        a.add_entries_for_tempo_end()

        all_checks = []
        all_checks.append(len(a.weather_entries) == 7)
        all_checks.append(a.weather_entries[4].time == '2001')
        all_checks.append(a.weather_entries[4] == entry_3)
        all_checks.append(a.weather_entries[0] == entry_0)
        all_checks.append(a.weather_entries[1] == entry_1)
        all_checks.append(a.weather_entries[2] == entry_2)
        all_checks.append(a.weather_entries[3] == new_entry_0)
        all_checks.append(a.weather_entries[5] == entry_4)
        all_checks.append(a.weather_entries[6] == new_entry_1)

        self.assertEqual(all(all_checks), True)

        ###################################################
        # has three tempos, entry_2, entry_4, tempo_5, tempos 4 and 5 are  back-to-back

        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()

        entry_0.year = 2019
        entry_0.month = 12
        entry_0.day = 18
        entry_0.time = "0100"

        entry_1.year = 2019
        entry_1.month = 12
        entry_1.day = 18
        entry_1.time = "0200"

        entry_2.is_tempo = True
        entry_2.year = 2019
        entry_2.month = 12
        entry_2.day = 18
        entry_2.time = "1600"
        entry_2.tempo_end_year = 2019
        entry_2.tempo_end_month = 12
        entry_2.tempo_end_day = 18
        entry_2.tempo_end_time = "2000"

        entry_3.year = 2019
        entry_3.month = 12
        entry_3.day = 18
        entry_3.time = "2001"

        entry_4.is_tempo = True
        entry_4.year = 2019
        entry_4.month = 12
        entry_4.day = 19
        entry_4.time = "0200"
        entry_4.tempo_end_year = 2019
        entry_4.tempo_end_month = 12
        entry_4.tempo_end_day = 19
        entry_4.tempo_end_time = "0500"

        entry_5.is_tempo = True
        entry_5.year = 2019
        entry_5.month = 12
        entry_5.day = 19
        entry_5.time = "0900"
        entry_5.tempo_end_year = 2019
        entry_5.tempo_end_month = 12
        entry_5.tempo_end_day = 19
        entry_5.tempo_end_time = "1100"

        new_entry_0 = read_ob_and_taf_json.WeatherEntry()
        new_entry_0.year = 2019
        new_entry_0.month = 12
        new_entry_0.day = 18
        new_entry_0.time = "2000"

        new_entry_1 = read_ob_and_taf_json.WeatherEntry()
        new_entry_1.year = 2019
        new_entry_1.month = 12
        new_entry_1.day = 19
        new_entry_1.time = "0500"

        new_entry_2 = read_ob_and_taf_json.WeatherEntry()
        new_entry_2.year = 2019
        new_entry_2.month = 12
        new_entry_2.day = 19
        new_entry_2.time = "1100"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.weather_entries.append(entry_4)
        a.weather_entries.append(entry_5)
        a.add_entries_for_tempo_end()

        all_checks = []
        all_checks.append(len(a.weather_entries) == 9)
        all_checks.append(a.weather_entries[4].time == '2001')
        all_checks.append(a.weather_entries[4] == entry_3)
        all_checks.append(a.weather_entries[0] == entry_0)
        all_checks.append(a.weather_entries[1] == entry_1)
        all_checks.append(a.weather_entries[2] == entry_2)
        all_checks.append(a.weather_entries[3] == new_entry_0)
        all_checks.append(a.weather_entries[5] == entry_4)
        all_checks.append(a.weather_entries[6] == new_entry_1)
        all_checks.append(a.weather_entries[7] == entry_5)
        all_checks.append(a.weather_entries[8] == new_entry_2)

        self.assertEqual(all(all_checks), True)

        ###################################################
        # no tempos

        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()

        entry_0.year = 2019
        entry_0.month = 12
        entry_0.day = 19
        entry_0.time = "0050"

        entry_1.year = 2019
        entry_1.month = 12
        entry_1.day = 19
        entry_1.time = "02000"

        entry_2.year = 2019
        entry_2.month = 12
        entry_2.day = 19
        entry_2.time = "1100"

        entry_3.year = 2019
        entry_3.month = 12
        entry_3.day = 19
        entry_3.time = "1120"

        entry_4.year = 2019
        entry_4.month = 12
        entry_4.day = 19
        entry_4.time = "1350"

        entry_5.year = 2019
        entry_5.month = 12
        entry_5.day = 19
        entry_5.time = "5350"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.weather_entries.append(entry_4)
        a.weather_entries.append(entry_5)
        a.add_entries_for_tempo_end()

        all_checks = []
        all_checks.append(len(a.weather_entries) == 6)
        all_checks.append((a.weather_entries[0]) == entry_0)
        all_checks.append((a.weather_entries[5]) == entry_5)

        self.assertEqual(all(all_checks), True)

    def test_populate_empty_required(self):

        ###########################################

        # 3 entries, last one is predom taf copying from previous ob
        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()

        entry_0.is_ob = True
        entry_0.year = 2019
        entry_0.month = 10
        entry_0.day = 28
        entry_0.time = "0100"

        entry_0.ob_wind_dir = "150"

        entry_1.is_ob = True
        entry_1.year = 2019
        entry_1.month = 10
        entry_1.day = 28
        entry_1.time = "0200"
        entry_1.ob_wind_dir = "180"

        entry_2.is_predom = True
        entry_2.year = 2019
        entry_2.month = 10
        entry_2.day = 28
        entry_2.time = "0400"
        entry_2.taf_predom_wind_dir = "200"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.taf_expire_year = 2019
        a.taf_expire_month = 10
        a.taf_expire_day = 30
        a.taf_expire_time = "0200"

        a.populate_empty_required()

        all_checks = []
        all_checks.append(a.weather_entries[2].is_ob and a.weather_entries[2].is_predom)
        all_checks.append(a.weather_entries[2].ob_wind_dir == "180")
        all_checks.append(a.weather_entries[2].taf_predom_wind_dir == "200")
        all_checks.append((a.weather_entries[0].is_ob is a.weather_entries[0].is_ob is True))
        all_checks.append((a.weather_entries[0].taf_predom_wind_dir is a.weather_entries[0].taf_predom_wind_dir is None))
        self.assertEqual(all(all_checks), True)

        ###########################################

        # 4 entries, 3rd one is predom taf copying from previous ob, last one is tempo
        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()

        entry_0.is_ob = True
        entry_0.year = 2019
        entry_0.month = 10
        entry_0.day = 28
        entry_0.time = "0100"
        entry_0.ob_wind_dir = "150"

        entry_1.is_ob = True
        entry_1.year = 2019
        entry_1.month = 10
        entry_1.day = 28
        entry_1.time = "0200"
        entry_1.ob_wind_dir = "180"

        entry_2.is_predom = True
        entry_2.year = 2019
        entry_2.month = 10
        entry_2.day = 28
        entry_2.time = "0400"
        entry_2.taf_predom_wind_dir = "200"

        entry_3.is_tempo = True
        entry_3.taf_predom_wind_dir = "250"
        entry_3.year = 2019
        entry_3.month = 10
        entry_3.day = 28
        entry_3.time = "0600"
        entry_3.tempo_end_year = 2019
        entry_3.tempo_end_month = 10
        entry_3.tempo_end_day = 28
        entry_3.tempo_end_time = "0900"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.taf_expire_year = 2019
        a.taf_expire_month = 10
        a.taf_expire_day = 30
        a.taf_expire_time = "0200"

        a.add_entries_for_tempo_end()  # should add one entry for the tempo ending
        a.populate_empty_required()

        all_checks = []
        all_checks.append(len(a.weather_entries) == 5)
        all_checks.append(a.weather_entries[2].is_ob and a.weather_entries[2].is_predom)
        all_checks.append(a.weather_entries[2].ob_wind_dir == "180")
        all_checks.append(a.weather_entries[2].taf_predom_wind_dir == "200")
        all_checks.append((a.weather_entries[0].is_ob is a.weather_entries[0].is_ob is True))
        all_checks.append(a.weather_entries[0].taf_predom_wind_dir is a.weather_entries[0].taf_predom_wind_dir is None)
        all_checks.append(a.weather_entries[4].is_ob and a.weather_entries[4].is_predom)
        all_checks.append(a.weather_entries[4].ob_wind_dir == "180")
        all_checks.append(a.weather_entries[4].taf_predom_wind_dir == "200")
        self.assertEqual(all(all_checks), True)

        ###########################################

        # 4 entries, 3rd one is predom taf copying from previous ob, 2nd to last one is tempo, last on is ob that will need predom in it
        entry_0 = read_ob_and_taf_json.WeatherEntry()
        entry_1 = read_ob_and_taf_json.WeatherEntry()
        entry_2 = read_ob_and_taf_json.WeatherEntry()
        entry_3 = read_ob_and_taf_json.WeatherEntry()
        entry_4 = read_ob_and_taf_json.WeatherEntry()
        entry_5 = read_ob_and_taf_json.WeatherEntry()

        entry_0.is_ob = True
        entry_0.year = 2019
        entry_0.month = 10
        entry_0.day = 28
        entry_0.time = "0100"
        entry_0.ob_wind_dir = "150"

        entry_1.is_ob = True
        entry_1.year = 2019
        entry_1.month = 10
        entry_1.day = 28
        entry_1.time = "0200"
        entry_1.ob_wind_dir = "180"

        entry_2.is_predom = True
        entry_2.year = 2019
        entry_2.month = 10
        entry_2.day = 28
        entry_2.time = "0400"
        entry_2.taf_predom_wind_dir = "200"

        entry_3.is_tempo = True
        entry_3.taf_predom_wind_dir = "250"
        entry_3.year = 2019
        entry_3.month = 10
        entry_3.day = 28
        entry_3.time = "0600"
        entry_3.tempo_end_year = 2019
        entry_3.tempo_end_month = 10
        entry_3.tempo_end_day = 28
        entry_3.tempo_end_time = "0900"

        entry_4.is_ob = True
        entry_4.year = 2019
        entry_4.month = 10
        entry_4.day = 28
        entry_4.time = "1000"
        entry_4.ob_wind_dir = "060"

        a = read_ob_and_taf_json.AllWeather()
        a.weather_entries.append(entry_0)
        a.weather_entries.append(entry_1)
        a.weather_entries.append(entry_2)
        a.weather_entries.append(entry_3)
        a.weather_entries.append(entry_4)
        a.taf_expire_year = 2019
        a.taf_expire_month = 10
        a.taf_expire_day = 30
        a.taf_expire_time = "0200"

        a.add_entries_for_tempo_end()  # should add one entry for the tempo ending
        a.populate_empty_required()

        all_checks = []
        all_checks.append(len(a.weather_entries) == 6)
        all_checks.append(a.weather_entries[2].is_ob and a.weather_entries[2].is_predom)
        all_checks.append(a.weather_entries[2].ob_wind_dir == "180")
        all_checks.append(a.weather_entries[2].taf_predom_wind_dir == "200")
        all_checks.append((a.weather_entries[0].is_ob is a.weather_entries[0].is_ob is True))
        all_checks.append(a.weather_entries[0].taf_predom_wind_dir is a.weather_entries[0].taf_predom_wind_dir is None)
        all_checks.append(a.weather_entries[4].is_ob and a.weather_entries[4].is_predom)
        all_checks.append(a.weather_entries[4].ob_wind_dir == "180")
        all_checks.append(a.weather_entries[4].taf_predom_wind_dir == "200")
        all_checks.append(a.weather_entries[5].is_ob and a.weather_entries[5].is_predom)
        all_checks.append(a.weather_entries[5].ob_wind_dir == "060")
        all_checks.append(a.weather_entries[5].taf_predom_wind_dir == "200")
        self.assertEqual(all(all_checks), True)

