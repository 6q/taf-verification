import unittest
import get_taf
import time
from unittest import mock

import other_functions.misc
import other_functions.time_based_functions


class MyTestCase(unittest.TestCase):

    def test_move_time(self):
        # subtract 30 minutes, within range so nothing else needs to be changed
        test_current_time = {'time': '1545', 'day': 4, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': -30, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '1515', 'day': 4, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 30 minutes, 1 hour will have to subtracted too
        test_current_time = {'time': '1505', 'day': 4, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': -30, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '1435', 'day': 4, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 1 day only within range
        test_current_time = {'time': '1505', 'day': 4, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': -1, 'month': 0, 'year': 0}
        test_correct_return = {'time': '1505', 'day': 3, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 5 day only go to last month
        test_current_time = {'time': '1505', 'day': 4, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': -5, 'month': 0, 'year': 0}
        test_correct_return = {'time': '1505', 'day': 30, 'month': 7, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 5 day  and add 9 hr
        test_current_time = {'time': '1505', 'day': 4, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': -2, 'time_minutes': 0, 'day': -2, 'month': 0, 'year': 0}
        test_correct_return = {'time': '1305', 'day': 2, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 5minutes from 0000z
        test_current_time = {'time': '0000', 'day': 6, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': -5, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '2355', 'day': 5, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 5minutes from 0000z, go back a month
        test_current_time = {'time': '0000', 'day': 1, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': -5, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '2355', 'day': 31, 'month': 7, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 15minutes from 0000z, go back a month and year
        test_current_time = {'time': '0000', 'day': 1, 'month': 1, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': -15, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '2345', 'day': 31, 'month': 12, 'year': 2018}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 2h 35minutes from 0000z, go back a month and year, is not leap year
        test_current_time = {'time': '0000', 'day': 1, 'month': 3, 'year': 2021}
        test_adjust_amount = {'time_hours': -2, 'time_minutes': -35, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '2125', 'day': 28, 'month': 2, 'year': 2021}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # add 1h bringing it to 0000Z next day
        test_current_time = {'time': '2300', 'day': 2, 'month': 7, 'year': 2019}
        test_adjust_amount = {'time_hours': 1, 'time_minutes': 0, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '0000', 'day': 3, 'month': 7, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # add 2h 7m
        test_current_time = {'time': '2300', 'day': 31, 'month': 7, 'year': 2019}
        test_adjust_amount = {'time_hours': 2, 'time_minutes': 7, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '0107', 'day': 1, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # add 2hr  and 10min when it is already 2300, add 2 days, 1 month and 1 year
        test_current_time = {'time': '2300', 'day': 31, 'month': 7, 'year': 2019}
        test_adjust_amount = {'time_hours': 2, 'time_minutes': 10, 'day': 2, 'month': 1, 'year': 1}
        test_correct_return = {'time': '0110', 'day': 3, 'month': 9, 'year': 2020}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # add
        test_current_time = {'time': '2300', 'day': 20, 'month': 7, 'year': 2019}
        test_adjust_amount = {'time_hours': 2, 'time_minutes': 0, 'day': 2, 'month': 11, 'year': 0}
        test_correct_return = {'time': '0100', 'day': 23, 'month': 6, 'year': 2020}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 2019years
        test_current_time = {'time': '0000', 'day': 1, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': 0, 'year': -2019}
        test_correct_return = {'time': '0000', 'day': 1, 'month': 8, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 1 month and 2019 years
        test_current_time = {'time': '0000', 'day': 1, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': -1, 'year': -2019}
        test_correct_return = {'time': '0000', 'day': 1, 'month': 7, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # do not move time
        test_current_time = {'time': '0000', 'day': 1, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '0000', 'day': 1, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 8 months
        test_current_time = {'time': '0000', 'day': 1, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': -8, 'year': 0}
        test_correct_return = {'time': '0000', 'day': 1, 'month': 12, 'year': 2018}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 8 months and 2018 years
        test_current_time = {'time': '0000', 'day': 1, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': -8, 'year': -2018}
        test_correct_return = {'time': '0000', 'day': 1, 'month': 12, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 31 days
        test_current_time = {'time': '0000', 'day': 31, 'month': 8, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': -31, 'month': 0, 'year': 0}
        test_correct_return = {'time': '0000', 'day': 31, 'month': 7, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 5 months
        test_current_time = {'time': '0050', 'day': 30, 'month': 4, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': -5, 'year': 0}
        test_correct_return = {'time': '0050', 'day': 30, 'month': 11, 'year': 2018}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 1 month. last day of month. original month had 31 days new one has 30 days
        test_current_time = {'time': '0050', 'day': 31, 'month': 5, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': -1, 'year': 0}
        test_correct_return = {'time': '0050', 'day': 30, 'month': 4, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # add 1 month
        test_current_time = {'time': '0050', 'day': 31, 'month': 5, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 0, 'day': 0, 'month': 1, 'year': 0}
        test_correct_return = {'time': '0050', 'day': 30, 'month': 6, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # subtract 10min into last day. Goes to 2359 last day
        test_current_time = {'time': '0009', 'day': 31, 'month': 5, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': -10, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '2359', 'day': 30, 'month': 5, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # Add 5 minute to 2355Z should go to 0005Z the next day which is the 1st
        test_current_time = {'time': '2355', 'day': 31, 'month': 5, 'year': 2019}
        test_adjust_amount = {'time_hours': 0, 'time_minutes': 10, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': '0005', 'day': 1, 'month': 6, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

        # 2hr over difference into new day into a new month
        test_current_time = {'time': "2300", 'day': 31, 'month': 5, 'year': 2019}
        test_adjust_amount = {'time_hours': 2, 'time_minutes': 0, 'day': 0, 'month': 0, 'year': 0}
        test_correct_return = {'time': "0100", 'day': 1, 'month': 6, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.move_time(test_current_time, test_adjust_amount), test_correct_return)

    def test_fix_taf_times(self):
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 12, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            # 2nd line has 1223/1224 in it. change to 1223/1300, day within month
            taf = ['TAF KBAD 120200Z 1202/1308 30012KT 9999 SCT020 QNH2993INS',
                   'BECMG 1223/1224 35009KT 9999 SKC QNH2994INS TX35/1221Z TN26/1311Z']
            fixed_taf = ['TAF KBAD 120200Z 1202/1308 30012KT 9999 SCT020 QNH2993INS',
                         'BECMG 1223/1300 35009KT 9999 SKC QNH2994INS TX35/1221Z TN26/1311Z']
            self.assertEqual(other_functions.time_based_functions.fix_taf_times(taf), fixed_taf)

        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 31, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            # 2nd line has 1223/1224 in it. change to 1223/1300, last day of month of august
            taf = ['TAF KBAD 310200Z 3102/0108 30012KT 9999 SCT020 QNH2993INS',
                   'BECMG 3123/3124 35009KT 9999 SKC QNH2994INS TX35/3121Z TN26/0111Z']
            fixed_taf = ['TAF KBAD 310200Z 3102/0108 30012KT 9999 SCT020 QNH2993INS',
                         'BECMG 3123/0100 35009KT 9999 SKC QNH2994INS TX35/3121Z TN26/0111Z']
            self.assertEqual(other_functions.time_based_functions.fix_taf_times(taf), fixed_taf)

        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 1, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            # multiple lines long, BECMG that needs to be fixed is the 4th line, day within range should go to 0300
            taf = ['TAF KLRF 012300Z 0123/0305 20012KT 9999 FEW050 QNH2982INS',
                   'BECMG 0201/0202 VRB06KT 9999 FEW150 QNH2983INS',
                   'BECMG 0215/0216 29012KT 9999 SCT080 BKN150 QNH2982INS',
                   'BECMG 0223/0224 31010G15KT 8000 SHRA SCT050 BKN100 QNH2982INS',
                   'TEMPO 0302/0305 VRB15G25KT 6000 TSRA SCT020CB BKN100 TX37/0222Z TN28/0211Z']

            fixed_taf = ['TAF KLRF 012300Z 0123/0305 20012KT 9999 FEW050 QNH2982INS',
                         'BECMG 0201/0202 VRB06KT 9999 FEW150 QNH2983INS',
                         'BECMG 0215/0216 29012KT 9999 SCT080 BKN150 QNH2982INS',
                         'BECMG 0223/0300 31010G15KT 8000 SHRA SCT050 BKN100 QNH2982INS',
                         'TEMPO 0302/0305 VRB15G25KT 6000 TSRA SCT020CB BKN100 TX37/0222Z TN28/0211Z']
            self.assertEqual(other_functions.time_based_functions.fix_taf_times(taf), fixed_taf)

        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 9, 21, 5, 59, 55, 5, 215, 0, 'UTC', 0])):
            # multiple lines long, BECMG that needs to be fixed is the 4th line, day within range should go to 0300
            taf = ['KTUL 210520Z 2106/2206 17013KT P6SM BKN012 ',
                   'FM210800 16013KT P6SM OVC012 ',
                   'TEMPO 2108/2112 3SM -SHRA BR OVC007 ',
                   'FM211800 18015G23KT P6SM SCT015 BKN050 ',
                   'PROB30 2118/2124 3SM -TSRA BKN015CB ',
                   'FM220000 17014KT P6SM BKN050']

            fixed_taf = ['KTUL 210520Z 2106/2206 17013KT P6SM BKN012 ',
                         'FM210800 16013KT P6SM OVC012 ',
                         'TEMPO 2108/2112 3SM -SHRA BR OVC007 ',
                         'FM211800 18015G23KT P6SM SCT015 BKN050 ',
                         'PROB30 2118/2200 3SM -TSRA BKN015CB ',
                         'FM220000 17014KT P6SM BKN050']
            self.assertEqual(other_functions.time_based_functions.fix_taf_times(taf), fixed_taf)

        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 15, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            # multiple lines long, TEMPO line with 1517/1527 that should be converted to 1517/1600
            taf = ['TAF KHST 150000Z 1500/1606 13005KT 9999 SCT020 BKN030 QNH2994INS ', 'TEMPO 1500/1502 VRB12G20KT 8000 -TSRA BKN030CB ', 'BECMG 1510/1511 21005KT 9999 FEW050 SCT100 QNH2995INS ', 'TEMPO 1517/1524 VRB10G15KT 8000 -TSRA SCT020CB BKN100 ', 'BECMG 1601/1602 22005KT 9999 FEW050 SCT070 QNH3000INS TX33/1515Z TN27/1509Z']

            fixed_taf = ['TAF KHST 150000Z 1500/1606 13005KT 9999 SCT020 BKN030 QNH2994INS ', 'TEMPO 1500/1502 VRB12G20KT 8000 -TSRA BKN030CB ', 'BECMG 1510/1511 21005KT 9999 FEW050 SCT100 QNH2995INS ', 'TEMPO 1517/1600 VRB10G15KT 8000 -TSRA SCT020CB BKN100 ', 'BECMG 1601/1602 22005KT 9999 FEW050 SCT070 QNH3000INS TX33/1515Z TN27/1509Z']
            self.assertEqual(other_functions.time_based_functions.fix_taf_times(taf), fixed_taf)


    def test_replace_chars_in_str_over_range(self):
        test_str = 'Hello World. My name is python'
        corrent_return = 'Hello PLURD. My name is python'
        self.assertEqual(other_functions.misc.replace_chars_in_str_over_range(test_str, 6, 'PLURD'), corrent_return)

        test_str = 'BECMG 0215/0216 29012KT 9999 SCT080 BKN150 QNH2982INS'
        corrent_return = 'BECMG 0215/0299 29012KT 9999 SCT080 BKN150 QNH2982INS'
        self.assertEqual(other_functions.misc.replace_chars_in_str_over_range(test_str, 13, '99'), corrent_return)


    def test_pretty_number(self):
        self.assertEqual(other_functions.misc.pretty_number(5000), '5,000')
        self.assertEqual(other_functions.misc.pretty_number(500), '500')
        self.assertEqual(other_functions.misc.pretty_number(-580), '-580')
        self.assertEqual(other_functions.misc.pretty_number(-8403), '-8,403')
        self.assertEqual(other_functions.misc.pretty_number(-56003), '-56,003')
        self.assertEqual(other_functions.misc.pretty_number(-800456003), '-800,456,003')
        self.assertEqual(other_functions.misc.pretty_number(1000000), '1,000,000')


    def test_convert_height(self):

        self.assertEqual(get_taf.convert_coded_height('001'), 100)
        self.assertEqual(get_taf.convert_coded_height('020'), 2000)
        self.assertEqual(get_taf.convert_coded_height('160'), 16000)
        self.assertEqual(get_taf.convert_coded_height('001', return_format='str'), '100')
        self.assertEqual(get_taf.convert_coded_height('020', return_format='str'), '2,000')
        self.assertEqual(get_taf.convert_coded_height('160', return_format='str'), '16,000')
        self.assertEqual(get_taf.convert_coded_height('250', return_format='str'), '25,000')

    def test_get_wind_shear(self):

        # has LLWS
        test_str = 'TAF AMD KFRI 152345Z 1523/1703 18012G20KT 9999 BKN050 WS020/15040KT QNH2980INS'
        correct_return = {'height': 2000, 'wind_direction': '150', 'wind_speed': '40'}
        self.assertEqual(get_taf.get_wind_shear(test_str), correct_return)

        # has LLWS
        test_str = 'TAF AMD KFRI 152345Z 1523/1703 18012G20KT 9999 BKN050 WS011/01030KT QNH2980INS'
        correct_return = {'height': 1100, 'wind_direction': '010', 'wind_speed': '30'}
        self.assertEqual(get_taf.get_wind_shear(test_str), correct_return)

        # no  LLWS
        test_str = 'TAF AMD KFRI 152345Z 1523/1703 18012G20KT 9999 BKN050 QNH2980INS'
        correct_return = None
        self.assertEqual(get_taf.get_wind_shear(test_str), correct_return)

    def test_is_prob_line(self):
        self.assertEqual(other_functions.misc.is_prob_line('PROB30 1719/1722 4SM -TSRA BR BKN030CB '), True)
        self.assertEqual(other_functions.misc.is_prob_line('TEMPO 1600/1601 8000 -TSRA BKN050CB'), False)
        self.assertEqual(other_functions.misc.is_prob_line('KEWR 152322Z 1600/1706 12008KT P6SM FEW015 SCT130 BKN250'), False)
        self.assertEqual(other_functions.misc.is_prob_line('PROB30 2118/2124 3SM -TSRA BKN015CB '), True)

    def test_get_prob(self):
        self.assertEqual(get_taf.get_prob_chance('PROB30 1719/1722 4SM -TSRA BR BKN030CB '), 30)
        self.assertEqual(get_taf.get_prob_chance('PROB90 1719/1722 4SM -TSRA BR BKN030CB '), 90)
        self.assertEqual(get_taf.get_prob_chance('TAF AMD KFRI 152345Z 1523/1703 18012G20KT 9999 BKN050 WS020/15040KT QNH2980INS'), None)

    def test_get_line_type(self):
        self.assertEqual(get_taf.get_line_type('TAF AMD KFRI 152345Z 1523/1703 18012G20KT 9999 BKN050 WS020/15040KT QNH2980INS'), 'predominant')
        self.assertEqual(get_taf.get_line_type('FM160000 12004KT P6SM SKC'), 'predominant')
        self.assertEqual(get_taf.get_line_type('BECMG 1614/1615 19009KT 9999 NSW SCT080 QNH2985INS'), 'predominant')
        self.assertEqual(get_taf.get_line_type('BECMG 1607/1608 21003KT 8000 BR BKN010 QNH2987INS'), 'predominant')
        self.assertEqual(get_taf.get_line_type('PROB30 1719/1722 4SM -TSRA BR BKN030CB '), 'temporary')
        self.assertEqual(get_taf.get_line_type('TEMPO 1609/1613 32015G25KT 3200 -TSRA BKN040CB '), 'temporary')

    def test_which_date_came_first(self):

        # same time
        date_1 = {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
        date_2 = {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.which_date_came_first(date_1, date_2), None)

        # 1 has lower time
        date_1 = {'time': '1359', 'day': 5, 'month': 12, 'year': 2019}
        date_2 = {'time': '1405', 'day': 5, 'month': 12, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.which_date_came_first(date_1, date_2), 1)

        # 1 has lower day
        date_1 = {'time': '1405', 'day': 2, 'month': 12, 'year': 2019}
        date_2 = {'time': '1405', 'day': 31, 'month': 12, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.which_date_came_first(date_1, date_2), 1)

        # 2 has higher day but lower month
        date_1 = {'time': '1405', 'day': 2, 'month': 12, 'year': 2019}
        date_2 = {'time': '1405', 'day': 31, 'month': 10, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.which_date_came_first(date_1, date_2), 2)

        # 2 has lower year but higher time
        date_1 = {'time': '1405', 'day': 2, 'month': 12, 'year': 2019}
        date_2 = {'time': '1955', 'day': 2, 'month': 12, 'year': 2005}
        self.assertEqual(other_functions.time_based_functions.which_date_came_first(date_1, date_2), 2)

        # 2 has lower month but higher day and time
        date_1 = {'time': '1405', 'day': 2, 'month': 12, 'year': 2019}
        date_2 = {'time': '1955', 'day': 3, 'month': 11, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.which_date_came_first(date_1, date_2), 2)

        # 2 is lower
        date_1 = {'time': '0000', 'day': 25, 'month': 8, 'year': 2019}
        date_2 = {'time': '0141', 'day': 24, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.which_date_came_first(date_1, date_2), 2)


    def test_calculate_date_differene(self):

        # 1hr apart, in range
        date_1 = {'time': "0600", 'day': 17, 'month': 5, 'year': 2019}
        date_2 = {'time': "0700", 'day': 17, 'month': 5, 'year': 2019}
        correct_return = {'hour': 1, 'minute': 0, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 29 minutes apart, in range
        date_1 = {'time': "0600", 'day': 17, 'month': 5, 'year': 2019}
        date_2 = {'time': "0629", 'day': 17, 'month': 5, 'year': 2019}
        correct_return = {'hour': 0, 'minute': 29, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 29 minutes apart, input order reversed, in range
        date_1 = {'time': "0629", 'day': 17, 'month': 5, 'year': 2019}
        date_2 = {'time': "0600", 'day': 17, 'month': 5, 'year': 2019}
        correct_return = {'hour': 0, 'minute': 29, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 2 months, in range
        date_1 = {'time': "0600", 'day': 17, 'month': 7, 'year': 2019}
        date_2 = {'time': "0600", 'day': 17, 'month': 5, 'year': 2019}
        correct_return = {'hour': 0, 'minute': 0, 'day': 0, 'month': 2, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 12 year difference
        date_1 = {'time': "0600", 'day': 17, 'month': 5, 'year': 2019}
        date_2 = {'time': "0600", 'day': 17, 'month': 5, 'year': 2007}
        correct_return = {'hour': 0, 'minute': 0, 'day': 0, 'month': 0, 'year': 12}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 7 min over two different hours
        date_1 = {'time': "0600", 'day': 17, 'month': 5, 'year': 2019}
        date_2 = {'time': "0553", 'day': 17, 'month': 5, 'year': 2019}
        correct_return = {'hour': 0, 'minute': 7, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 2 months different from nov 2018 to jan 2019
        date_1 = {'time': "0600", 'day': 17, 'month': 11, 'year': 2018}
        date_2 = {'time': "0600", 'day': 17, 'month': 1, 'year': 2019}
        correct_return = {'hour': 0, 'minute': 0, 'day': 0, 'month': 2, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 2hr over different days
        date_1 = {'time': "2300", 'day': 16, 'month': 5, 'year': 2019}
        date_2 = {'time': "0100", 'day': 17, 'month': 5, 'year': 2019}
        correct_return = {'hour': 2, 'minute': 0, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 2hr over different days into a new month
        date_1 = {'time': "2300", 'day': 31, 'month': 5, 'year': 2019}
        date_2 = {'time': "0100", 'day': 1, 'month': 6, 'year': 2019}
        correct_return = {'hour': 2, 'minute': 0, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 2hr over different days into a new month and new year
        date_1 = {'time': "2300", 'day': 31, 'month': 12, 'year': 2020}
        date_2 = {'time': "0100", 'day': 1, 'month': 1, 'year': 2021}
        correct_return = {'hour': 2, 'minute': 0, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 1 day
        date_1 = {'time': "2300", 'day': 31, 'month': 12, 'year': 2020}
        date_2 = {'time': "2300", 'day': 30, 'month': 12, 'year': 2020}
        correct_return = {'hour': 0, 'minute': 0, 'day': 1, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 1 month
        date_1 = {'time': "2300", 'day': 31, 'month': 7, 'year': 2020}
        date_2 = {'time': "2300", 'day': 31, 'month': 8, 'year': 2020}
        correct_return = {'hour': 0, 'minute': 0, 'day': 0, 'month': 1, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # no difference
        date_1 = {'time': "2300", 'day': 31, 'month': 7, 'year': 2020}
        date_2 = {'time': "2300", 'day': 31, 'month': 7, 'year': 2020}
        correct_return = {'hour': 0, 'minute': 0, 'day': 0, 'month': 0, 'year': 0}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)

        # 1 year
        date_1 = {'time': "2300", 'day': 31, 'month': 7, 'year': 2020}
        date_2 = {'time': "2300", 'day': 31, 'month': 7, 'year': 2021}
        correct_return = {'hour': 0, 'minute': 0, 'day': 0, 'month': 0, 'year': 1}
        self.assertEqual(other_functions.time_based_functions.calculate_date_difference(date_1, date_2), correct_return)


    def test_get_current_time(self):

        # with time_strcut not given
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 12, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            correct_return = {'time': "1959", 'day': 12, 'month': 8, 'year': 2019}
            self.assertEqual(other_functions.time_based_functions.get_current_time(), correct_return)

        # with timestruct
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 12, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            correct_return = {'time': "0053", 'day': 11, 'month': 2, 'year': 2014}
            self.assertEqual(other_functions.time_based_functions.get_current_time(time.struct_time([2014, 2, 11, 0, 53, 55, 5, 215, 0, 'UTC', 0])), correct_return)


    def test_is_taf_completed(self):
        time_struct_current = time.struct_time([2019, 8, 24, 1, 12, 55, 5, 215, 0, 'UTC', 0])  # august 24th 2019 at 0112z
        taf = ['TAF AMD KBAD 232220Z 2322/2500 14010G15KT 9999 VCTS BKN050CB QNH2991INS ', 'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        self.assertEqual(get_taf.is_taf_completed(taf, time_struct_current), False)

        time_struct_current = time.struct_time([2019, 8, 24, 1, 12, 55, 5, 215, 0, 'UTC', 0])  # august 24th 2019 at 0112z
        taf = ['TAF AMD KBAD 232220Z 2322/2500 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        self.assertEqual(get_taf.is_taf_completed(taf, time_struct_current), False)


    def test_get_line_time_range(self):

        # tests a FM line where the next and last line is a tempo line. Should get the line end from the TAF end
        taf = ['KTUL 230255Z 2303/2400 05008KT P6SM VCTS FEW040CB SCT110 OVC200 ', 'TEMPO 2303/2304 VRB30G40KT 1SM +TSRA BKN030CB ', 'FM230900 13002KT P6SM VCTS OVC015CB ', 'PROB30 2312/2315 3SM -TSRA BKN030CB ', 'FM231500 11004KT P6SM OVC025 ', 'PROB30 2315/2321 3SM -TSRA BKN050CB']
        index = 4
        time_struct = time.struct_time([2019, 8, 24, 1, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = date_time_dict = {
        'soft_start_time': None,
        'soft_start_day': None,
        'soft_start_month': None,
        'soft_start_year': None,
        'hard_start_time': '1500',
        'hard_start_day': 23,
        'hard_start_month': 8,
        'hard_start_year': 2019,
        'soft_end_time': None,
        'soft_end_day': None,
        'soft_end_month': None,
        'soft_end_year': None,
        'hard_end_time': "0000",
        'hard_end_day': 24,
        'hard_end_month': 8,
        'hard_end_year': 2019
        }

        self.assertEqual(other_functions.time_based_functions.get_line_time_range(taf, 4, time_struct), correct_return)


    def test_get_taf_time_frame(self):
        taf = ['TAF AMD KBAD 232220Z 2322/2500 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        time_struct = time_struct = time.struct_time([2019, 8, 28, 1, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'start': {'day': 23, 'month': 8, 'time': '2200', 'year': 2019},
                          'end': {'day': 25, 'month': 8, 'time': '0000', 'year': 2019}}
        self.assertEqual(other_functions.time_based_functions.get_taf_time_frame(taf, time_struct), correct_return)

        taf = ['TAF AMD KBAD 232220Z 3005/0120 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        time_struct = time.struct_time([2019, 8, 25, 1, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'start': {'day': 30, 'month': 7, 'time': '0500', 'year': 2019},
                          'end': {'day': 1, 'month': 8, 'time': '2000', 'year': 2019}}
        self.assertEqual(other_functions.time_based_functions.get_taf_time_frame(taf, time_struct), correct_return)


    def test_get_taf_time_length(self):
        taf = ['TAF AMD KBAD 232220Z 2305/2405 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        time_struct = time.struct_time([2019, 8, 25, 1, 12, 55, 5, 215, 0, 'UTC', 0])
        self.assertEqual(other_functions.time_based_functions.get_taf_time_length(taf, time_struct), 24)

        taf = ['TAF AMD KBAD 232220Z 2300/2400 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        time_struct = time.struct_time([2019, 8, 25, 1, 12, 55, 5, 215, 0, 'UTC', 0])
        self.assertEqual(other_functions.time_based_functions.get_taf_time_length(taf, time_struct), 24)

        taf = ['TAF KPOE 301600Z 3016/3122 VRB06KT 9999 SCT030 QNH2996INS  ',
               'BECMG 3020/3021 16006KT 9999 VCSH SCT035 QNH2998INS',
               'BECMG 3100/3101 VRB06KT 9999 NSW SCT025 QNH2996INS TX33/3020Z TN23/3110',]
        time_struct = time.struct_time([2019, 8, 30, 16, 53, 55, 5, 215, 0, 'UTC', 0])
        self.assertEqual(other_functions.time_based_functions.get_taf_time_length(taf, time_struct), 30)


    def test_get_taf_end_time_and_date(self):
        taf = ['TAF AMD KBAD 232220Z 2322/2500 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        time_struct = time.struct_time([2019, 8, 25, 1, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '0000', 'day': 25, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.get_taf_end_time_and_date(taf, time_struct), correct_return)

        # special case. TAF start late December and ends January next year
        taf = ['TAF AMD KBAD 300800Z 3008/0105 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        time_struct = time.struct_time([2019, 12, 30, 10, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '0500', 'day': 1, 'month': 1, 'year': 2020}
        self.assertEqual(other_functions.time_based_functions.get_taf_end_time_and_date(taf, time_struct), correct_return)

        # taf_test ends 1st of next month
        taf = ['TAF AMD KBAD 310800Z 3108/0123 14010G15KT 9999 VCTS BKN050CB QNH2991INS ',
               'TEMPO 2323/2401 VRB15G28KT ',
               'BECMG 2401/2402 15008KT 9999 NSW SCT120 QNH2992INS ', 'BECMG 2409/2410 14010KT 9999 BKN015 QNH2995INS ',
               'BECMG 2417/2418 13010KT 9999 VCTS BKN050CB QNH2990INS TX34/2322Z TN23/2411Z']
        time_struct = time.struct_time([2019, 7, 31, 10, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '2300', 'day': 1, 'month': 8, 'year': 2019}
        self.assertEqual(other_functions.time_based_functions.get_taf_end_time_and_date(taf, time_struct), correct_return)


    def test_get_taf_issue_or_start_time_and_date(self):
        time_struct = time.struct_time([2019, 8, 30, 14, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '0759', 'day': 31, 'month': 7, 'year': 2019}
        taf_first_line = 'TAF AMD KSHV 310759Z 3108/0123 14010G15KT 9999 VCTS BKN050CB QNH2991INS '
        self.assertEqual(other_functions.time_based_functions.get_taf_issue_or_start_time_and_date(taf_first_line, time_struct, issue_or_start='issue'), correct_return)

        time_struct = time.struct_time([2019, 8, 30, 14, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '0800', 'day': 31, 'month': 7, 'year': 2019}
        taf_first_line = 'TAF AMD KSHV 310759Z 3108/0123 14010G15KT 9999 VCTS BKN050CB QNH2991INS '
        self.assertEqual(other_functions.time_based_functions.get_taf_issue_or_start_time_and_date(taf_first_line, time_struct, issue_or_start='start'), correct_return)

        time_struct = time.struct_time([2019, 8, 30, 14, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '1422', 'day': 21, 'month': 8, 'year': 2019}
        taf_first_line = 'TAF AMD KSHV 211422Z 2114/0123 14010G15KT 9999 VCTS BKN050CB QNH2991INS '
        self.assertEqual(other_functions.time_based_functions.get_taf_issue_or_start_time_and_date(taf_first_line, time_struct, issue_or_start='issue'), correct_return)

        time_struct = time.struct_time([2019, 8, 30, 14, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '1400', 'day': 21, 'month': 8, 'year': 2019}
        taf_first_line = 'TAF AMD KSHV 211422Z 2114/0123 14010G15KT 9999 VCTS BKN050CB QNH2991INS '
        self.assertEqual(other_functions.time_based_functions.get_taf_issue_or_start_time_and_date(taf_first_line, time_struct, issue_or_start='start'), correct_return)

        time_struct = time.struct_time([2019, 8, 30, 14, 12, 55, 5, 215, 0, 'UTC', 0])
        correct_return = {'time': '1400', 'day': 3, 'month': 8, 'year': 2019}
        taf_first_line = 'TAF AMD KSHV 031422Z 0314/0123 14010G15KT 9999 VCTS BKN050CB QNH2991INS '
        self.assertEqual(other_functions.time_based_functions.get_taf_issue_or_start_time_and_date(taf_first_line, time_struct, issue_or_start='start'), correct_return)


if __name__ == '__main__':
    unittest.main()
