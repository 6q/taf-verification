import unittest
import get_obs
from unittest import mock
import time

import other_functions.common_metar_taf_elements
import other_functions.time_based_functions
import other_functions.misc


class MyTestCase(unittest.TestCase):

    def test_get_temp_and_dew_point(self):

        test_ob = 'KBAD 041656Z AUTO 26003KT 10SM SCT110 32/19 A2998 RMK AO2 SLP153 T03200192'
        correct_return = {'temp': 32, 'dew_point': 19}
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob), correct_return)

        test_ob = 'KSAV 041653Z VRB06KT 10SM SCT036 SCT095 SCT150 32/23 A2995 RMK AO2 LTG DSNT N AND SW SLP142 CB DSNT NW T03220228'
        correct_return = {'temp': 32.2, 'dew_point': 22.8}
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob, get_exact=True), correct_return)

        test_ob = 'KSAV 041653Z VRB06KT 10SM SCT036 SCT095 SCT150 32/23 A2995 RMK AO2 LTG DSNT N AND SW SLP142 CB DSNT NW T03200228'
        correct_return = {'temp': 32.0, 'dew_point': 22.8}
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob, get_exact=True), correct_return)

        # temp not encoded into ob
        test_ob = 'KSAV 041653Z VRB06KT 10SM SCT036 SCT095 SCT150 A2995 RMK AO2 LTG DSNT N AND SW SLP142 CB DSNT NW T03200228'
        correct_return = None
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob, get_exact=False), correct_return)

        # T group will fail and fall back on main ob parts
        test_ob = 'KSAV 041653Z VRB06KT 10SM SCT036 SCT095 SCT150 M03/M09 A2995 RMK AO2 LTG DSNT N AND SW SLP142 CB DSNT NW'
        correct_return = {'temp': -3, 'dew_point': -9}
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob, get_exact=True), correct_return)

        test_ob = 'KSAV 041653Z VRB06KT 10SM SCT036 SCT095 SCT150 M05/M07 A2995 RMK AO2 LTG DSNT N AND SW SLP142 CB DSNT NW T03200228'
        correct_return = {'temp': -5, 'dew_point': -7}
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob, get_exact=False), correct_return)

        test_ob = 'KSAV 041653Z VRB06KT 10SM SCT036 SCT095 SCT150 25/M17 A2995 RMK AO2 LTG DSNT N AND SW SLP142 CB DSNT NW T03200228'
        correct_return = {'temp': 25, 'dew_point': -17}
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob, get_exact=False), correct_return)

        test_ob = 'KBNA 041653Z 00000KT 10SM FEW030 FEW085 SCT160 BKN250 29/21 A3002 RMK AO2 SLP158 T12241276'
        correct_return = {'temp': -22.4, 'dew_point': -27.6}
        self.assertEqual(other_functions.common_metar_taf_elements.get_temp_and_dew_point(test_ob, get_exact=True), correct_return)


    def test_get_icao(self):

        test_str = 'KACB 270735Z AUTO 00000KT 3SM +RA SCT022 BKN050 OVC110 20/19 A3001 RMK AO2 T02040186'
        self.assertEqual(get_obs.get_icao(test_str), 'KACB')

        test_str = 'OPLA 270755Z 09010KT 1 1/2SM FU SCT040 BKN100 31/25 Q1000 TEMPO 32015G30KT 2000 TSRA FEW030CB RMK QFE974 RMK A29.52'
        self.assertEqual(get_obs.get_icao(test_str), 'OPLA')

        test_str = 'K49A 041635Z AUTO 24004KT 10SM SCT035 SCT041 SCT050 26/19 A3010 RMK AO2 T02610191'
        self.assertEqual(get_obs.get_icao(test_str), 'K49A')

        test_str = 'A7 041635Z AUTO 24004KT 10SM SCT035 SCT041 SCT050 26/19 A3010 RMK AO2 T02610191'
        self.assertEqual(get_obs.get_icao(test_str), 'A7')

        test_str = 'ATRT7 041635Z AUTO 24004KT 10SM SCT035 SCT041 SCT050 26/19 A3010 RMK AO2 T02610191'
        self.assertEqual(get_obs.get_icao(test_str), 'ATRT7')

    def test_which_date_came_first(self):

        # both happened at the same time
        date_1 = {'time': '1050', 'day': 25, 'month': 5, 'year': 2019}
        date_2 = {'time': '1050', 'day': 25, 'month': 5, 'year': 2019}
        self.assertEqual(get_obs.which_date_came_first(date_1, date_2), None)

        # date 2 has a earlier time only
        date_1 = {'time': '1050', 'day': 25, 'month': 5, 'year': 2019}
        date_2 = {'time': '1014', 'day': 25, 'month': 5, 'year': 2019}
        self.assertEqual(get_obs.which_date_came_first(date_1, date_2), 2)

        # date 1 has a earlier day and later time
        date_1 = {'time': '2351', 'day': 1, 'month': 5, 'year': 2019}
        date_2 = {'time': '1014', 'day': 25, 'month': 5, 'year': 2019}
        self.assertEqual(get_obs.which_date_came_first(date_1, date_2), 1)

        # date 1 has a earlier month only
        date_1 = {'time': '1500', 'day': 25, 'month': 4, 'year': 2019}
        date_2 = {'time': '1500', 'day': 25, 'month': 5, 'year': 2019}
        self.assertEqual(get_obs.which_date_came_first(date_1, date_2), 1)

        # date 2 has a earlier day only
        date_1 = {'time': '1500', 'day': 25, 'month': 5, 'year': 2019}
        date_2 = {'time': '1500', 'day': 24, 'month': 5, 'year': 2019}
        self.assertEqual(get_obs.which_date_came_first(date_1, date_2), 2)

        # date 2 has a earlier time, and year only
        date_1 = {'time': '1500', 'day': 25, 'month': 5, 'year': 2019}
        date_2 = {'time': '1000', 'day': 25, 'month': 5, 'year': 2015}
        self.assertEqual(get_obs.which_date_came_first(date_1, date_2), 2)

        # date_2 came first
        date_1 = {'time': '0000', 'day': 25, 'month': 8, 'year': 2019}
        date_2 = {'time': '0118', 'day': 24, 'month': 8, 'year': 2019}
        self.assertEqual(get_obs.which_date_came_first(date_1, date_2), 2)

    def test_get_wind(self):
        self.assertEqual(other_functions.common_metar_taf_elements.get_wind(
            'CYAM 270741Z 18008G17KT 15SM -SHRA BKN058TCU OVC069 19/18 A2996 RMK TCU6ACC2 DIST LTNG VISBL S SLP146 DENSITY ALT 1300FT'),
            {'wind_dir': '180',
             'wind_speed_sustained': '08',
             'wind_speed_gust': '17'})

        self.assertEqual(other_functions.common_metar_taf_elements.get_wind(
            'KACB 270735Z AUTO 00000KT 3SM +RA SCT022 BKN050 OVC110 20/19 A3001 RMK AO2 T02040186'),
            {'wind_dir': '000',
             'wind_speed_sustained': '00',
             'wind_speed_gust': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_wind(
            'KACB 270735Z AUTO VRB04KT 10SM -DZ SCT022 BKN050 OVC110 20/19 A3001 RMK AO2 T02040186'),
            {'wind_dir': 'VRB',
             'wind_speed_sustained': '04',
             'wind_speed_gust': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_wind(
            'OPLA 270755Z 09010KT 1 1/2SM FU SCT040 BKN100 31/25 Q1000 TEMPO 32015G30KT 2000 TSRA FEW030CB RMK QFE974 RMK A29.52'),
            {'wind_dir': '090',
             'wind_speed_sustained': '10',
             'wind_speed_gust': None})

    def test_convert_visibility(self):
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('7'), '9999')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('6'), '9000')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('10'), '9999')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('15'), '9999')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('2'), '3200')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('3'), '4800')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('P6SM'), '9999')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('3/8'), '0600')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('1/2'), '0800')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('1 5/8'), '2600')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('2 1/4'), '3600')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('3/4'), '1200')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('0100'), '1/16')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('0500'), '5/16')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('1400'), '7/8')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('2000'), '1 1/4')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('2800'), '1 3/4')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('3600'), '2 1/4')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('4800'), '3')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('6000'), '4')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('9000'), '6')
        self.assertEqual(other_functions.common_metar_taf_elements.convert_visibility('9999'), '7')

    def test_get_visibility(self):
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KSLC 270254Z 17013KT 8SM RA FEW022 BKN080CB BKN160 21/17 A3016 RMK AO2 PK WND 18036/0155 RAB21 SLP162 CB S MOV E AND DSNT E-S P0021 60021 T02060172 51019'),
                         '8')
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'OPLA 270755Z 09010KT 1 1/2SM FU SCT040 BKN100 31/25 Q1000 TEMPO 32015G30KT 2000 TSRA FEW030CB RMK QFE974 RMK A29.52'),
                         '1 1/2')
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'OPLA 270755Z 09010KT 1/2SM FU SCT040 BKN100 31/25 Q1000 TEMPO 32015G30KT 2000 TSRA FEW030CB RMK QFE974 RMK A29.52'),
                         '1/2')
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KABQ 310452Z 15010KT 7/8SM BR SCT100 BKN260 28/10 A3020 RMK AO2 LTG DSNT S SLP127 CB DSNT SE-S T02780100 $'),
                         '7/8')
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KLAS 312256Z 23010G19KT 10SM TS FEW110CB SCT150 BKN200 27/18 A2996 RMK AO2 LTG DSNT SE-SW RAB07E40 TSE39B40 SLP118 OCNL LTGICCG SE-S TS SE-S MOV NE P0001 T02720183'),
                         '10')
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KLAS 312256Z 23010G19KT 5SM TS FEW110CB SCT150 BKN200 27/18 A2996 RMK AO2 LTG DSNT SE-SW RAB07E40 TSE39B40 SLP118 OCNL LTGICCG SE-S TS SE-S MOV NE P0001 T02720183'),
                         '5')
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KACB 270735Z AUTO 00000KT 3SM +FZRA SCT022 BKN050 OVC110 20/19 A3001 RMK AO2 T02040186'), '3')
        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KACB 270735Z AUTO 00000KT 7SM +FZRA SCT022 BKN050 OVC110 20/19 A3001 RMK AO2 T02040186'), '7')

        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KACB 270735Z AUTO 00000KT 6SM +FZRA SCT022 BKN050 OVC110 20/19 A3001 RMK AO2 T02040186'), '6')

        self.assertEqual(other_functions.common_metar_taf_elements.get_visibility(
            'KFSM 231622Z 2316/2412 09006KT 6SM -RA BKN035 OVC110'), '6')

    def test_get_weather(self):
        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KSLC 270254Z 17013KT 8SM RA FEW022 BKN080CB BKN160 21/17 A3016 RMK AO2 PK WND 18036/0155 RAB21 SLP162 CB S MOV E AND DSNT E-S P0021 60021 T02060172 51019'),
                         {
                             'wx_intensity': None,
                             'vicinity_or_not': None,
                             'description': None,
                             'precipitation': 'RA',
                             'obscuration': None,
                             'other': None,
                             'rotation': None,
                             'precip_liquid_or_solid': 'liquid'})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KMCI 010720Z 09010KT 10SM VCTS FEW055CB SCT140 20/18 A3009 RMK AO2 LTG DSNT W PRESFR TS VC W-NW T02000183'),
                         {
                             'wx_intensity': None,
                             'vicinity_or_not': 'VC',
                             'description': 'TS',
                             'precipitation': None,
                             'obscuration': None,
                             'other': None,
                             'rotation': None,
                             'precip_liquid_or_solid': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KMCI 010720Z 09010KT 10SM +FC RA FEW055CB SCT140 20/18 A3009 RMK AO2 LTG DSNT W PRESFR TS VC W-NW T02000183'),
                         {
                             'wx_intensity': None,
                             'vicinity_or_not': None,
                             'description': None,
                             'precipitation': 'RA',
                             'obscuration': None,
                             'other': None,
                             'rotation': 'tornado',
                             'precip_liquid_or_solid': 'liquid'})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KBOS 011500Z AUTO 18013KT 4SM 11/11 RMK SLP007 T01110111 IEM_DS3505'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': None,
                'obscuration': None,
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KMCI 010720Z 09010KT 10SM +FC RA FEW055CB SCT140 20/18 A3009 RMK AO2 LTG DSNT W PRESFR TS VC W-NW T02000183'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': 'RA',
                'obscuration': None,
                'other': None,
                'rotation': 'tornado',
                'precip_liquid_or_solid': 'liquid'})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KACB 270735Z AUTO 00000KT 3SM +FZRA SCT022 BKN050 OVC110 20/19 A3001 RMK AO2 T02040186'),
            {
                'wx_intensity': '+',
                'vicinity_or_not': None,
                'description': 'FZ',
                'precipitation': 'RA',
                'obscuration': None,
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': 'both'})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KJAX 271856Z 03009KT 1M TS FEW010 SCT032CB BKN050 BKN200 27/25 A3017 RMK AO2 LTG DSNT NW RAB22E48 TSB21 SLP218 OCNL LTGICCG OHD TS OHD MOV SW P0001 T02720250'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': 'TS',
                'precipitation': None,
                'obscuration': None,
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KABQ 310452Z 15010KT 7/8SM BR SCT100 BKN260 28/10 A3020 RMK AO2 LTG DSNT S SLP127 CB DSNT SE-S T02780100 $'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': None,
                'obscuration': 'BR',
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KACB 270735Z AUTO 00000KT 10SM +FC SCT022 BKN050CB OVC110 20/19 A3001 RMK AO2 T02040186'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': None,
                'obscuration': None,
                'other': None,
                'rotation': 'tornado',
                'precip_liquid_or_solid': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KSFO 022356Z 27019G25KT 10SM FEW004 21/13 A3001 RMK AO2 PK WND 26027/2337 SLP163 T02060133 10217 20189 58013'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': None,
                'obscuration': None,
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KBZN 030031Z 30026G42KT 10SM VCTS -RA FEW028 OVC100 19/12 A3014 RMK AO2 PK WND 30042/0025 WSHFT 0011 LTG DSNT S AND NW TSE16 P0001 T01940117'),
            {
                'wx_intensity': '-',
                'vicinity_or_not': 'VC',
                'description': 'TS',
                'precipitation': 'RA',
                'obscuration': None,
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': 'liquid'})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KPLA 270755Z 09010KT 1 1/2SM FU SCT040 BKN100 31/25 RMK A02A'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': None,
                'obscuration': 'FU',
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': None})

        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KAVP 030029Z VRB03KT 6SM -RA BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'),
            {
                'wx_intensity': '-',
                'vicinity_or_not': None,
                'description': None,
                'precipitation': 'RA',
                'obscuration': 'BR',
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': 'liquid'})

        # ice pellets
        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KAVP 030029Z VRB03KT 6SM PL BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': 'PL',
                'obscuration': 'BR',
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': 'solid'})

        # RASN
        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'KAVP 030029Z VRB03KT 6SM RASN FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'),
            {
                'wx_intensity': None,
                'vicinity_or_not': None,
                'description': None,
                'precipitation': 'RASN',
                'obscuration': None,
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': 'both'})

        # RASNPL
        self.assertEqual(other_functions.common_metar_taf_elements.get_weather(
            'METAR KBOS 080300Z 25010KT 5SM -RASNPL 04/00 A//// RMK SLP983 T00390000'),
            {
                'wx_intensity': '-',
                'vicinity_or_not': None,
                'description': None,
                'precipitation': 'RASNPL',
                'obscuration': None,
                'other': None,
                'rotation': None,
                'precip_liquid_or_solid': 'both'})


    def test_get_clouds(self):
        self.assertEqual(other_functions.common_metar_taf_elements.get_clouds(
            'KSLC 270254Z 17013KT 8SM RA FEW022 BKN080CB BKN160 21/17 A3016 RMK AO2 PK WND 18036/0155 RAB21 SLP162 CB S MOV E AND DSNT E-S P0021 60021 T02060172 51019'),
                         {'cloud_layer_1': {'amount': 'FEW', 'base': '022'},
                          'cloud_layer_2': {'amount': 'BKN', 'base': '080'},
                          'cloud_layer_3': {'amount': 'BKN', 'base': '160'}})

        self.assertEqual(other_functions.common_metar_taf_elements.get_clouds(
            'KAVP 030029Z VRB03KT 6SM -RA BR SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'),
            {'cloud_layer_1': {'amount': 'SCT', 'base': '085'},
             'cloud_layer_2': {'amount': 'OVC', 'base': '110'}})

        self.assertEqual(other_functions.common_metar_taf_elements.get_clouds(
            'KAVP 030029Z VRB03KT 6SM -RA BR CLR 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'),
            {'cloud_layer_1': {'amount': 'CLR', 'base': None}})

        self.assertEqual(other_functions.common_metar_taf_elements.get_clouds(
            'KAVP 030029Z VRB03KT 6SM -RA BR SCT060 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'),
            {'cloud_layer_1': {'amount': 'SCT', 'base': '060'}})

        self.assertEqual(other_functions.common_metar_taf_elements.get_clouds(
            'KAVP 030029Z VRB03KT 6SM -RA BR VV002 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'),
            {'cloud_layer_1': {'amount': 'VV', 'base': '002'}})

    def test_get_zulu_time(self):
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 1, 21, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.get_zulu_time(), '0121')

        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 22, 00, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.get_zulu_time(), '2200')

        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 0, 1, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.get_zulu_time(), '0001')

    # time.struct_time(tm_year=2019, tm_mon=8, tm_mday=3, tm_hour=21, tm_min=15, tm_sec=55, tm_wday=5, tm_yday=215, tm_isdst=0)
    def test_adjust_date_lower(self):
        # when ob time is earlier that day, nothing needs to be adjusted.
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 21, 15, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.adjust_date_lower('0051', 3, 8, 2019),
                             {'month': 8, 'year': 2019, 'day': 3, 'time': '0051'})

        # when the ob time '1137' is after the current time `0615` day is lowered by 1
        # it is aug 3rd 2019 at 0615z, ob time is 1137z aug 3rd 2019, shoudl adjust to
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 6, 15, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.adjust_date_lower('1137', 3, 8, 2019),
                             {'month': 7, 'year': 2019, 'day': 3, 'time': '1137'})

        # when the ob time '1137' is after the current time `0615` on January 1st 2019 it means that the ob is on December 31st 2018
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 1, 1, 6, 15, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.adjust_date_lower('1137', 1, 1, 2019),
                             {'month': 12, 'year': 2018, 'day': 1, 'time': '1137'})

        # jan 1st 2019 ob time before gmtime, should not adjust anything
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 1, 1, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.adjust_date_lower('1137', 1, 1, 2019),
                             {'month': 1, 'year': 2019, 'day': 1, 'time': '1137'})

        # testing with going back a day into last year
        with mock.patch('time.gmtime', return_value=time.struct_time([2012, 1, 1, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(other_functions.time_based_functions.adjust_date_lower('2356', 1, 1, 2012),
                             {'month': 12, 'year': 2011, 'day': 1, 'time': '2356'})

    def test_get_day_of_month_and_zulu_time_from_metar(self):
        # test with ob time before gmtime, no adjustment is needed
        test_ob = 'KAVP 030029Z VRB03KT 6SM -RA BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(get_obs.get_date_and_time_from_metar(test_ob), {'month': 8, 'year': 2019, 'day': 3, 'time': '0029'})

        # gmdate is august 3rd and ob date is 28th, it should give the ob a date of july 28
        test_ob = 'KAVP 280029Z VRB03KT 6SM -RA BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(get_obs.get_date_and_time_from_metar(test_ob), {'month': 7, 'year': 2019, 'day': 28, 'time': '0029'})

        # gmdate is jan 1st 2019 ob month_date is 31st. should figure out that date is dec 31st 2018. gmtime is after ob time
        test_ob = 'KAVP 310029Z VRB03KT 6SM -RA BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 1, 1, 19, 59, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(get_obs.get_date_and_time_from_metar(test_ob), {'month': 12, 'year': 2018, 'day': 31, 'time': '0029'})

        # gmdate is jan 1st 2019 ob month_date is 31st. should figure out that date is dec 31st 2018. ob time(2229z) is before(1556z) gmtime
        test_ob = 'KAVP 312229Z VRB03KT 6SM -RA BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 1, 1, 15, 56, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(get_obs.get_date_and_time_from_metar(test_ob), {'month': 12, 'year': 2018, 'day': 31, 'time': '2229'})

        # gmdate is jan 1st 2019 ob month_date is 31st. should figure out that date is dec 31st 2018. ob time(2229z) is before(1556z) gmtime
        test_ob = 'KAVP 292229Z VRB03KT 6SM -RA BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'
        with mock.patch('time.gmtime', return_value=time.struct_time([2016, 3, 1, 15, 56, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(get_obs.get_date_and_time_from_metar(test_ob), {'month': 2, 'year': 2016, 'day': 29, 'time': '2229'})


        # ob time is after gm_time. day is the same. should figure out that the ob is last months ob of the same day
        test_ob = 'KAVP 032229Z VRB03KT 6SM -RA BR FEW070 SCT085 OVC110 20/18 A3012 RMK AO2 LTG DSNT NE-SE TSE12 P0009 T02000183'
        with mock.patch('time.gmtime', return_value=time.struct_time([2019, 8, 3, 10, 56, 55, 5, 215, 0, 'UTC', 0])):
            self.assertEqual(get_obs.get_date_and_time_from_metar(test_ob), {'month': 7, 'year': 2019, 'day': 3, 'time': '2229'})

    def test_is_fm_line(self):
        self.assertEqual(other_functions.misc.is_fm_line('KFSM 230405Z 2304/2400 13008KT 3SM TSRA OVC035CB '), False)
        self.assertEqual(other_functions.misc.is_fm_line('FM230900 05002KT P6SM VCTS OVC015CB '), True)
        self.assertEqual(other_functions.misc.is_fm_line('PROB30 2315/2321 3SM -TSRA BKN050CB'), False)
        self.assertEqual(other_functions.misc.is_fm_line('TAF COR KBAD 241815Z 2418/2524 15005KT 9999 BKN030 OVC070 QNH2995INS'), False)

if __name__ == '__main__':
    unittest.main()
