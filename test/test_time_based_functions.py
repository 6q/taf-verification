import unittest
from other_functions.time_based_functions import *
from other_functions.csv_df_tools import set_valid_date_time_as_index
import pandas as pd

class MyTestCase(unittest.TestCase):

    def test_find_row_closest_to_time(self):
        # read the csv as pandas dataframes and set valid column as index
        df_1 = pd.read_csv('csv_files/KJFK.csv')
        df_1 = set_valid_date_time_as_index(df=df_1)
        # df_2 = pd.read_csv('csv_files/KSEA.csv')
        # df_2 = set_valid_date_time_as_index(df=df_2)

        # a time before the df. Should return 0
        time_to_match = pd.to_datetime('1841-07-01 11:00:00')
        self.assertEqual(0, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # a time after the df. Should return the index of the last row which is just len minus 1
        time_to_match = pd.to_datetime('2241-07-01 11:00:00')
        self.assertEqual(len(df_1)-1, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is the same time as the first ob
        time_to_match = pd.to_datetime('1948-07-01 11:00:00')
        self.assertEqual(0, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is the same time as the last ob
        time_to_match = pd.to_datetime('2020-01-02 00:51:00')
        self.assertEqual(len(df_1)-1, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 1 minute after first ob
        time_to_match = pd.to_datetime('1948-07-01 11:01:00')
        self.assertEqual(0, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 1 minute before last ob
        time_to_match = pd.to_datetime('2020-01-02 00:50:00')
        self.assertEqual(len(df_1)-1, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 1 minute after second ob
        time_to_match = pd.to_datetime('1948-07-01 12:01:00')
        self.assertEqual(1, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 1 minute before second to last ob
        time_to_match = pd.to_datetime('2020-01-01 23:50:00')
        self.assertEqual(len(df_1) - 2, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 5 minutes after 3rd ob
        time_to_match = pd.to_datetime('1948-07-01 13:05:00')
        self.assertEqual(2, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 5 minutes before 3rd to last ob
        time_to_match = pd.to_datetime('2020-01-01 22:46:00')
        self.assertEqual(len(df_1) - 3, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 5 minutes before 3rd ob
        time_to_match = pd.to_datetime('1948-07-01 12:55:00')
        self.assertEqual(2, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is 5 minutes after 3rd to last ob
        time_to_match = pd.to_datetime('2020-01-01 22:56:00')
        self.assertEqual(len(df_1) - 3, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # some time match is the same time as the ob. (this one is roughly in the middle of the csv)
        time_to_match = pd.to_datetime('1988-05-30 16:00:00')
        self.assertEqual(328844, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is the same time as the ob. (this one is roughly in the middle of the csv)
        time_to_match = pd.to_datetime('1988-05-17 05:34:00')
        self.assertEqual(328471, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is in between two already close obs. (this one is roughly in the middle of the csv)
        time_to_match = pd.to_datetime('1988-05-17 05:25:00')
        self.assertEqual(328470, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is exactly the same as a 2017 ob
        time_to_match = pd.to_datetime('2017-07-22 02:55:00')
        self.assertEqual(611031, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is exactly the same as a 2017 ob
        time_to_match = pd.to_datetime('2017-07-22 01:51:00')
        self.assertEqual(611017, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is in between two obs that have a time difference of a few minutes
        time_to_match = pd.to_datetime('2017-07-22 01:52:00')
        self.assertEqual(611017, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is in between two obs that have a time difference of a few minutes
        time_to_match = pd.to_datetime('2017-07-22 01:50:40')
        self.assertEqual(611017, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))

        # time to match is in between two obs that have a time difference of a few minutes
        time_to_match = pd.to_datetime('2017-07-22 01:54:00')
        self.assertEqual(611018, find_row_closest_to_time(df=df_1, time_to_match=time_to_match))
