import tensorflow as tf
if tf.test.gpu_device_name():
    print('Default GPU Device: {}'.format(tf.test.gpu_device_name()))
else:
    print("Please install GPU version of TF")

import tensorflow as tf

print('is compiled on GPU', tf.test.is_built_with_cuda())


